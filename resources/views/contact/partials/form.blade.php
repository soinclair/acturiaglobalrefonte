<div class="col-lg-8">
    <div class="section-title">
        <h2>CONTACTEZ - <span>NOUS.</span></h2>
    </div>
    <p>Envie de démarrer votre projet ? Contactez-nous et laissez-nous vous accompagner.</p>
    <form id="call-back-form" class="call-back-form needs-validation" name="call-back-form" action="{{ route('contact.store') }}" method="post">
        @csrf
        <div class="row clearfix">
          <div class="row">
                <div class="col-md-6">        
                    <div class="form-group">
                        <input type="text" name="full-name" class="form-control @error('full-name') is-invalid @enderror" value="{{ old('full-name') }}" placeholder="Name Here" required="">
                        @error('full-name')
                            <div class="invalid-feedback"> {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="email" name="email-address" class="form-control @error('email-address') is-invalid @enderror"  value="{{ old('email-address') }}" placeholder="Email Here" required="">
                        @error('email-address')
                            <div class="invalid-feedback"> {{ $message }}</div>
                        @enderror
                    </div>     
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="phone-number" class="form-control @error('phone-number') is-invalid @enderror" value="{{ old('phone-number') }}" placeholder="Phone No.">
                        @error('phone-number')
                            <div class="invalid-feedback"> {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">        
                    <div class="form-group">
                        <input type="text" name="subject" class="form-control @error('subject') is-invalid @enderror" value="{{ old('subject') }}" placeholder="Subject" required="">
                        @error('subject')
                            <div class="invalid-feedback"> {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-12 mb-40">        
                    <div class="form-group">
                        <textarea name="form-message" rows="7" class="form-control @error('form-message') is-invalid @enderror" placeholder="Text here...">{{ old('form-message') }}</textarea>
                        @error('form-message')
                            <div class="invalid-feedback"> {{ $message }}</div>
                        @enderror
                    </div>
                </div> 
            </div>                                                         
        </div>
        <div class="form-group mb-0 text-center">
            <button class="theme-btn" type="submit">Envoyer <i class="fas fa-arrow-right"></i></button>
        </div>
    </form>
</div>