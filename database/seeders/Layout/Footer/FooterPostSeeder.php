<?php

namespace Database\Seeders\Layout\Footer;

use App\Models\Layout\Footer\FooterPost;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FooterPostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        FooterPost::insert([
            [
                'title' => json_encode([
                    'en' => 'Recent Articles',
                    'fr' => 'Articles récents',
                    'ar' => 'مقالات حديثة',
                ]),
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
