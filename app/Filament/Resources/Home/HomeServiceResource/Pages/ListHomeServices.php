<?php

namespace App\Filament\Resources\Home\HomeServiceResource\Pages;

use App\Filament\Resources\Home\HomeServiceResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListHomeServices extends ListRecords
{
    use ListRecords\Concerns\Translatable;

    protected static string $resource = HomeServiceResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
