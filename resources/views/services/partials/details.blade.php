<div class="col-lg-8">
    <div class="service-details-content">
        <div class="section-title">
            <h2>{{$currentService->label}}</h2>
        </div>
        <p>{{$currentService->serviceDetail->title_paragraph}}</p>
        <div class="details-image" style="--bg-service-image-icon:url({{asset('assets/' . $currentService->icon)}})">
            <img src="{{asset('assets/' . $currentService->serviceDetail->image)}}" alt="{{ $currentService->label }}">
        </div>
        @for ($i=1; $i<4; $i++)
            <h3> {!! $currentService->serviceDetail["header$i"] !!} </h3>
            <p> {!! $currentService->serviceDetail["paragraph1_$i"] !!} </p>
            <p> {!! $i !== 3 ? $currentService->serviceDetail["paragraph2_$i"] : "" !!} </p>
        @endfor

        <div class="prev-next-area">
            <div class="row">
                <div class="col-sm-6">
                    <a class="prev-next-btn prev-btn wow fadeInUp" data-wow-duration="2s" href="{{route('services.show',['uri'  => $previousService->uri ])}}"><span>Precedent</span></a>
                </div>
                <div class="col-sm-6">
                    <a class="prev-next-btn next-btn wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s" href="{{route('services.show',['uri' => $nextService->uri ])}}"><span>Suivant</span></a>
                </div>
            </div>
        </div>
    </div>
</div>