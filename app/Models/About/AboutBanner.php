<?php

namespace App\Models\About;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class AboutBanner extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        'title',
        'image',
    ];

    public $translatable = [
        "title",
    ];
}
