<?php

namespace App\Providers\Filament;

use Illuminate\Support\ServiceProvider;

class ReOrderNavigationGroupsProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
