<?php

namespace Database\Seeders\Layout\Footer;

use App\Models\Layout\Footer\FooterMenuItem;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FooterMenuItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        FooterMenuItem::insert([
            [
                'label' => json_encode([
                    'en' => 'Support',
                    'fr' => 'Support',
                    'ar' => 'الدعم',
                ]),
                'uri' => 'contact',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
