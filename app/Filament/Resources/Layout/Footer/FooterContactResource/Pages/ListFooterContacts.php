<?php

namespace App\Filament\Resources\Layout\Footer\FooterContactResource\Pages;

use App\Filament\Resources\Layout\Footer\FooterContactResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListFooterContacts extends ListRecords
{
    use ListRecords\Concerns\Translatable;
    
    protected static string $resource = FooterContactResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
