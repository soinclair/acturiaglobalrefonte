<?php

namespace Database\Seeders\Layout\Footer;

use App\Models\Layout\Footer\FooterService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FooterServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        FooterService::insert([
            [
                'title' => json_encode([
                    'en' => 'Our Services',
                    'fr' => 'Nos Services',
                    'ar' => 'خدماتنا',
                ]),
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
