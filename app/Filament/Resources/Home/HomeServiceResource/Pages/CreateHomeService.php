<?php

namespace App\Filament\Resources\Home\HomeServiceResource\Pages;

use App\Filament\Resources\Home\HomeServiceResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateHomeService extends CreateRecord
{
    use CreateRecord\Concerns\Translatable;

    protected static string $resource = HomeServiceResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            // ...
        ];
    }
}
