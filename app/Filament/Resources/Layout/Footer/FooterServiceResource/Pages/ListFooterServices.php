<?php

namespace App\Filament\Resources\Layout\Footer\FooterServiceResource\Pages;

use App\Filament\Resources\Layout\Footer\FooterServiceResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListFooterServices extends ListRecords
{
    use ListRecords\Concerns\Translatable;
    
    protected static string $resource = FooterServiceResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
