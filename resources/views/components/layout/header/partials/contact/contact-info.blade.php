<ul class="top-left">
    <li><i class="fa fa-envelope"></i> <a href="mailto:{{ $supports[0]->email }}"> {{ $supports[0]->email }} </a></li>
    <li><i class="fas fa-map-marker-alt"></i><a href="{{ $location->url }}"> {{ $location->label }} </a></li>
    <li><i class="fa-solid fa-headphones"></i><a href="callto:{{ $supports[0]->phone }}"> {{ $supports[0]->phone }} </a>
    </li>
</ul>
