<footer class="footer-section bg-black pt-150 rpt-100">
    <div class="container">
        <div class="row">

            @include('components.layout.footer.partials.main.get-in-touch')
            @include('components.layout.footer.partials.main.recent-posts')
            @include('components.layout.footer.partials.main.our-services')
            @include('components.layout.footer.partials.main.subscribe')
            
        </div>
    </div>
    
    <!-- Copyright Area-->
    <div class="copyright-area mt-95 rmt-45">
       <div class="container">
            <div class="copyright-inner align-items-center">

                @include('components.layout.footer.partials.copyright.copyright')
                @include('components.layout.footer.partials.copyright.footer-menu')

            </div>
        </div>
    </div>
</footer>