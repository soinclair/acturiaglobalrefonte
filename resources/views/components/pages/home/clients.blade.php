<section class="partners-section pt-135 rpt-85 pb-145 rpb-130">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 clients-title-container">
               <div class="section-title wow fadeInUp" data-wow-duration="2s">
                    <h2> {{ $title }} </h2>
                </div>
            </div>
            <div class="col-xl-8">
                <div class="partner-wrap">
                    @foreach ($clientItems as $client)
                        <div class="partner-item">
                            <img src="{{ asset( $client->logo ) }}" loading="lazy" alt="{{ $client->name }}">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>