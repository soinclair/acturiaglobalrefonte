<?php

namespace App\Models\About;

use App\Models\Utility\SuccessItem;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class AboutVission extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        'tab_title',
        'tab_image',
        'title',
        'keywords',
        'paragraph',
    ];

    public $translatable = [
        'tab_title',
        'title',
        'keywords',
        'paragraph',
    ];

    public function successItem()
    {
        return $this->morphOne(SuccessItem::class, 'success_itemable');
    }
}
