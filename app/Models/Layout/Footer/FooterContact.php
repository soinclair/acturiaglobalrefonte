<?php

namespace App\Models\Layout\Footer;

use App\Models\Layout\Layout;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class FooterContact extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        'title',
        'address_title',
        'address',
        'contact_number_title',
        'contact_number',
    ];

    public $translatable = [
        'title',
        'address_title',
        'address',
        'contact_number_title',
    ];

    public function layout(){
        return $this->belongsTo(Layout::class);
    }
}
