<?php

namespace App\Http\Controllers;

use App\Mail\CallBackMail;
use App\Models\Shared\CallBackEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CallBackFormController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'full-name' => 'required',
            'email-address' => 'required|email|unique:call_back_emails,email',
            'phone-number' => 'nullable|regex:/^[0-9]{10,20}$/',
            'subject' => 'required',
            'short-text' => 'required',
        ]);

        $user = CallBackEmail::create([
            'name' => $request['full-name'],
            'email' => $request['email-address'],
            'phone_number' => $request['phone-number'],
            'subject' => $request['subject'],
            'short_text' => $request['short-text'],
        ]);
        Mail::send(new CallBackMail($user));
        return redirect()->route('home.index')->with('success', "You have successfully sent an email with <strong>".$user->email."</strong>");
    }
}
