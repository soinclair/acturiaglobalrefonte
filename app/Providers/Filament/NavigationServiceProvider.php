<?php

namespace App\Providers;

use App\Filament\Resources\Layout\Footer\FooterContactResource;
use App\Models\Layout\Header\HeaderMenuItem;
use Filament\Facades\Filament;
use Filament\Navigation\NavigationGroup;
use Illuminate\Support\ServiceProvider;

class FilamentNavigationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        // Filament::serving(function () {
        //     Filament::registerNavigationGroups([
        //         NavigationGroup::make('Layout')
        //             ->icon('heroicon-o-layout')
        //             ->collapsed()
        //             ->
        //             ->groups([
        //                 NavigationGroup::make('Header')
        //                     ->icon('heroicon-o-header')
        //                     ->items([
        //                         HeaderMenuItem::class,
        //                     ]),
        //                 NavigationGroup::make('Footer')
        //                     ->icon('heroicon-o-footer')
        //                     ->items([
        //                         FooterContactResource::class,
        //                     ]),
        //             ]),
        //     ]);
        // });
    }
}
