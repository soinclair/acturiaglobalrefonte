<!-- footer menu -->
<ul class="footer-menu order-1 order-md-2">
    @foreach($menuItems as $menuItem)
        <li><a href="{{ route($menuItem->uri.'.index') }}"> {{ $menuItem->label }}</a></li>
    @endforeach
</ul>