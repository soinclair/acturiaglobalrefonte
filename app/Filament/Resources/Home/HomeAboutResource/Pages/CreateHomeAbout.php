<?php

namespace App\Filament\Resources\Home\HomeAboutResource\Pages;

use App\Filament\Resources\Home\HomeAboutResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateHomeAbout extends CreateRecord
{

    use CreateRecord\Concerns\Translatable;

    protected static string $resource = HomeAboutResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            // ...
        ];
    }
}
