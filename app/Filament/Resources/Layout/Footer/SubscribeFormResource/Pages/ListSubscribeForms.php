<?php

namespace App\Filament\Resources\Layout\Footer\SubscribeFormResource\Pages;

use App\Filament\Resources\Layout\Footer\SubscribeFormResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListSubscribeForms extends ListRecords
{
    use ListRecords\Concerns\Translatable;
    
    protected static string $resource = SubscribeFormResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
