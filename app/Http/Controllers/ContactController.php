<?php

namespace App\Http\Controllers;

use App\Models\Contact\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index(){
        return view('contact.contact');
    }
    
    public function store(Request $request){
        $request->validate([
            'full-name' => 'required',
            'email-address' => 'required|email|unique:contacts,email',
            'phone-number' => 'nullable|regex:/^[0-9]{10,20}$/',
            'subject' => 'required',
            'form-message' => 'required',
        ]);

        $subscription = Contact::create([
            'name' => $request['full-name'],
            'email' => $request['email-address'],
            'phone_number' => $request['phone-number'],
            'subject' => $request['subject'],
            'message' => $request['form-message'],
        ]);
        return redirect()->route('home.index')->with('success', "You have successfully sent an email with <strong>".$subscription->email."</strong>");
    }
    
}
