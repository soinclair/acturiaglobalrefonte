<?php

namespace App\View\Components\Layout;

use App\Models\Layout\Header\Logo;
use App\Models\Layout\Header\Location;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class Header extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public Collection $supports,
        public Location $location,
        public Collection $socialMediaLinks,
        public Collection $menuItems,
        public Logo $logo
    )
    {   
       
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.layout.header.header');
    }
}
