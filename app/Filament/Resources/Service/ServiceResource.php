<?php

namespace App\Filament\Resources\Service;

use App\Filament\Resources\Service\ServiceResource\Pages;
use App\Filament\Resources\Service\ServiceResource\RelationManagers;
use App\Models\Service\Service;
use Filament\Forms;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;


class ServiceResource extends Resource
{
    use Translatable;

    protected static ?string $model = Service::class;

    protected static ?string $navigationIcon = 'heroicon-o-cog';

    protected static ?string $activeNavigationIcon = 'heroicon-s-cog';

    protected static ?string $navigationGroup = 'resources';

    protected static ?string $modelLabel = 'services';


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make("Title Section")->schema([
                    TextInput::make('label')
                        ->required()
                        ->maxLength(255)
                        ->label(static::getDynamicLabel("label"))
                        ->translateLabel(),

                    TextInput::make('uri')
                        ->required()
                        ->unique(Service::class, 'uri')
                        ->maxLength(255)
                        ->label(static::getDynamicLabel("uri"))
                        ->translateLabel(),
                ])->columnSpan(2),

                Section::make("Icon Image")->schema([
                    fileupload::make('icon')
                        ->acceptedfiletypes(['image/png', "image/jpeg", "image/svg+xml"])
                        ->disk('assets')
                        ->directory('images/services/icons')
                        ->hiddenLabel()
                        ->required()
                        ->extraAttributes(['class' => 'min-h-40 image-png-container'])
                        ->label(static::getDynamicLabel("icon"))
                        ->translateLabel(),

                ])->columnspan(1)
            ])->columns(3);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('label')
                    ->sortable()
                    ->searchable()
                    ->toggleable()
                    ->label(static::getDynamicLabel("label"))
                    ->translateLabel(),

                TextColumn::make('uri')
                    ->sortable()
                    ->searchable()
                    ->toggleable()
                    ->label(static::getDynamicLabel("uri"))
                    ->translateLabel(),

                // TODO: Handle dark mode image not looking good
                ImageColumn::make('icon')
                    ->disk('assets')
                    ->square()
                    ->toggleable()
                    ->label(static::getDynamicLabel("icon"))
                    ->translateLabel(),

                // TextColumn::make('created_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->searchable()
                //     ->toggleable()
                //     ->label(static::getDynamicLabel("created_at"))
                //     ->translateLabel(),
                // TextColumn::make('updated_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->searchable()
                //     ->toggleable()
                //     ->label(static::getDynamicLabel("updated_at"))
                //     ->translateLabel(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            RelationManagers\ServiceDetailRelationManager::class,
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListServices::route('/'),
            'create' => Pages\CreateService::route('/create'),
            'edit' => Pages\EditService::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch plural translation without "s"
    }

    public static function getModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch translation based on current locale
    }

    public static function getNavigationGroup(): ?string
    {
        return __("filament/custom/navigation-model-labels." . static::$navigationGroup); // Fetch translation based on current locale
    }

    protected static function getDynamicLabel(string $field): string
    {
        return __("model-properties." . static::$modelLabel . '-' . $field);
    }



    // public static function getTranslatableLocales(): array
    // {
    //     return ['en', 'fr'];
    // }
}
