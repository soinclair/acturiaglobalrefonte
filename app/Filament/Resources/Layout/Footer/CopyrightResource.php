<?php

namespace App\Filament\Resources\Layout\Footer;

use App\Filament\Resources\Layout\Footer\CopyrightResource\Pages;
use App\Filament\Resources\Layout\Footer\CopyrightResource\RelationManagers;
use App\Models\Layout\Footer\Copyright;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class CopyrightResource extends Resource
{

    use Translatable;

    protected static ?string $model = Copyright::class;

    protected static ?string $navigationIcon = 'heroicon-o-shield-check';

    protected static ?string $activeNavigationIcon = 'heroicon-s-shield-check';

    protected static ?string $modelLabel = 'copyrights';

    protected static ?string $navigationGroup = 'footer';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('paragraph')
                    ->required()
                    ->label(static::getDynamicLabel("paragraph"))
                    ->translateLabel(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('paragraph')
                    ->searchable()
                    ->sortable()
                    ->label(static::getDynamicLabel("paragraph"))
                    ->translateLabel(),
                // Tables\Columns\TextColumn::make('created_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->label(static::getDynamicLabel("created_at"))
                //     ->translateLabel(),
                // Tables\Columns\TextColumn::make('updated_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->label(static::getDynamicLabel("updated_at"))
                //     ->translateLabel(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCopyrights::route('/'),
            'create' => Pages\CreateCopyright::route('/create'),
            'edit' => Pages\EditCopyright::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch plural translation without "s"
    }

    public static function getModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch translation based on current locale
    }

    public static function getNavigationGroup(): ?string
    {
        return __("filament/custom/navigation-model-labels." . static::$navigationGroup); // Fetch translation based on current locale
    }

    protected static function getDynamicLabel(string $field): string
    {
        return __("model-properties." . static::$modelLabel . '-' . $field);
    }
}
