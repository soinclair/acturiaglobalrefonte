<?php

namespace Database\Seeders\Post;

use App\Models\Post\Post;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Post::insert([
            [
                'title' => 'Post 1',
                'uri' => 'post-1',
                'date' => now(),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'title' => 'Post 2',
                'uri' => 'post-2',
                'date' => now(),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            
        ]);
    }
}
