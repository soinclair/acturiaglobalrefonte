<?php

namespace App\Filament\Resources\Layout\Footer\CopyrightResource\Pages;

use App\Filament\Resources\Layout\Footer\CopyrightResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditCopyright extends EditRecord
{
    use EditRecord\Concerns\Translatable;

    protected static string $resource = CopyrightResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
