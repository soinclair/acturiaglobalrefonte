<?php

namespace App\View\Components\Shared;

use App\View\Components\Classes\CaseItem;
use App\View\Components\Classes\Category;
use App\View\Components\Classes\Image;
use App\View\Components\Classes\Section;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Cases extends Component
{
    /**
     * Create a new component instance.
     */
    public Section $section;
    public array $cases; // Array of CaseItem

    public function __construct(){
        $this->section = new Section(["Cases Section"],[],["This paragraph"]);
        
        $category1= new Category(1,"Category1");
        $category2= new Category(2,"Category2");
        $category3= new Category(3,"Category3");
        $category4= new Category(4,"Category4");

        $image1= new Image('case 1','assets/images/cases/case1.jpg');
        $image2= new Image('case 2','assets/images/cases/case2.jpg');
        $image3= new Image('case 3','assets/images/cases/case3.jpg');
        $image4= new Image('case 4','assets/images/cases/case3.jpg');

        $caseItem1 = new CaseItem(1,"CaseItem1",$category1,"case-item1",$image1);
        $caseItem2 = new CaseItem(2,"CaseItem2",$category2,"case-item2",$image2);
        $caseItem3 = new CaseItem(3,"CaseItem3",$category3,"case-item3",$image3);
        $caseItem4 = new CaseItem(4,"CaseItem4",$category4,"case-item4",$image4);
        
        $this->cases = [$caseItem1,$caseItem2,$caseItem3,$caseItem4];
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.shared.cases');
    }
}
