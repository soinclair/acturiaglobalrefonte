@php
    $locales = ['en' => 'English', 'fr' => 'Français'];
    $currentLocale = app()->getLocale();
@endphp

<form class="top-right " action="{{ route('switch.lang', app()->getLocale()) }}" method="GET">
    <select name="language" onchange="this.form.submit()">
        @foreach ($locales as $locale => $language)
            <option value="{{ $locale }}" {{ $currentLocale == $locale ? 'selected' : '' }}>
                {{ $language }}
            </option>
        @endforeach
    </select>
</form>
