<?php

namespace App\Filament\Resources\Layout\Footer\FooterMenuItemResource\Pages;

use App\Filament\Resources\Layout\Footer\FooterMenuItemResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListFooterMenuItems extends ListRecords
{
    use ListRecords\Concerns\Translatable;
    
    protected static string $resource = FooterMenuItemResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
