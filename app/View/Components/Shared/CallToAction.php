<?php
namespace App\View\Components\Shared;

use App\View\Components\Classes\CTA;
use App\View\Components\Classes\Section;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class CallToAction extends Component
{
    /**
     * Create a new component instance.
     */
    public CTA $cta;
    public Section $section;
    public string $theme;

    public function __construct($theme = 'orange'){
        $this->cta = new CTA("Contactez-nous","contact");
        $this->section = new Section(["Vous souhaitez collaborer avec ACTUARIA GLOBAL ?"],[],[]);
        $this->theme = $theme;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.shared.call-to-action');
    }
}
