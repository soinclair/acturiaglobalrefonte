<?php

namespace Database\Seeders\Layout\Header;

use App\Models\Layout\Header\Location;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Location::insert([
            [
                'label' => json_encode([
                    'en' => 'Casablanca, Morocco.',
                    'fr' => 'Casablanca, Maroc.',
                    'ar' => 'الدار البيضاء، المغرب.',
                ]),
                'url' => 'https://maps.app.goo.gl/mnUHLwmQWGQqH3q8A',
                'created_at' => now(),
                'updated_at' => now(),
            ],            
        ]);
    }
}
