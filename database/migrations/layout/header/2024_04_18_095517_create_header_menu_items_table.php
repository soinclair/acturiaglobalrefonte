<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    // TODO: Change "uri" to "route_name", for evey migrations.
    public function up(): void
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('header_menu_items', function (Blueprint $table) {
            $table->id();
            $table->json('label');
            $table->string('uri');
            $table->unsignedBigInteger('parent_id')->nullable(); 
            $table->foreign('parent_id')->references('id')->on('header_menu_items')->onDelete('cascade');
            $table->foreignId('layout_id')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('header_menu_items');
    }
};
