<?php

namespace Database\Seeders\Sector;

use App\Models\Sector\Sector;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // It was thought that these were Services, but they were not, They are Secotrs,
        // The data here will be use for Sectors instead
        Sector::insert([
            [
                'label' => 'Mutuelles et prevoyance sociale',
                'uri' => Str::slug('Mutuelles et prevoyance sociale'),
                'client_type' => 'entreprise',
                'icon'=> Str::slug('Mutuelles et prevoyance sociale').'.png',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'label' => 'Gouvernement, Financement & Fondations Sociales',
                'uri' => Str::slug('Gouvernement, Financement & Fondations Sociales'),
                'client_type' => 'entreprise',
                'icon' => Str::slug('Gouvernement, Financement & Fondations Sociales').'.png',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'label' => 'Caisses de retraite',
                'uri' => Str::slug('Caisses de retraite'),
                'client_type' => 'entreprise',
                'icon' => Str::slug('Caisses de retraite').'.png',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'label' => 'Assurances',
                'uri' => Str::slug('Assurances'),
                'client_type' => 'entreprise',
                'icon' => Str::slug('Assurances').'.png', 
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'label' => 'TAKAFUL & Finance Islamique',
                'uri' => Str::slug('TAKAFUL & Finance Islamique'),
                'client_type' => 'entreprise',
                'icon' => Str::slug('TAKAFUL & Finance Islamique').'.png', 
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'label' => 'Gestion de l\'assurance',
                'uri' => Str::slug('Gestion de l\'assurance'),
                'client_type' => 'institution',
                'icon' => Str::slug('Gestion de l\'assurance').'.png', 
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'label' => 'Prevoyance medicale',
                'uri' => Str::slug('Prevoyance medicale'),
                'client_type' => 'institution',
                'icon' => Str::slug('Prevoyance medicale').'.png', 
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'label' => 'Engagements sociaux: IAS 19',
                'uri' => Str::slug('Engagements sociaux: IAS 19'),
                'client_type' => 'institution',
                'icon' => Str::slug('Engagements sociaux: IAS 19').'.png', 
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'label' => 'Retraite & Epargne salariale',
                'uri' => Str::slug('Retraite & Epargne salariale'),
                'client_type' => 'institution',
                'icon' => Str::slug('Retraite & Epargne salariale').'.png', 
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'label' => 'Remuneration',
                'uri' => Str::slug('Remuneration'),
                'client_type' => 'institution',
                'icon' => Str::slug('Remuneration').'.png', 
                'created_at' => now(),
                'updated_at' => now(),
            ],            
        ]);
    }
}
