<?php

namespace App\Filament\Resources\About\AboutVissionResource\Pages;

use App\Filament\Resources\About\AboutVissionResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListAboutVissions extends ListRecords
{
    use ListRecords\Concerns\Translatable;

    protected static string $resource = AboutVissionResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
