<?php

namespace App\Models\Layout\Footer;

use App\Models\Layout\Layout;
use App\Models\Post\Post;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class FooterPost extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        'title',
    ];

    public $translatable = [
        'title',
    ];
    
    public function layout(){
        return $this->belongsTo(Layout::class);
    }
}
