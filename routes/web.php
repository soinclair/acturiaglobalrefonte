<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CallBackFormController;
use App\Http\Controllers\CareerController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\FAQController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\SectorController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SubscribeFormController;
use App\Http\Controllers\SupportController;
use Illuminate\Http\Request;

Route::redirect('/', 'home');

// <--------------------- Layout --------------------->
Route::post('/subscribe', [SubscribeFormController::class, 'store'])->name('subscribe');

// <--------------------- Home --------------------->
Route::get('/home', [HomeController::class, 'index'])->name('home.index');

// <--------------------- About us --------------------->
Route::get('/about-us', [AboutController::class, 'index'])->name('about.index');

// <--------------------- Your Sector --------------------->
Route::get('/your-sector', [SectorController::class, 'index'])->name('sector.index');
Route::get('/your-sector/{sector}', [SectorController::class, 'show'])->name('sector.show');

Route::redirect('/your-sector', 'coming-soon');

// <--------------------- Services --------------------->
Route::get('/services', [ServiceController::class, 'index'])->name('services.index');
Route::get('/services/{uri}', [ServiceController::class, 'show'])->name('services.show');


// <--------------------- News --------------------->
Route::get('/news', [NewsController::class, 'index'])->name('news.index');
Route::get('/news/{slug}', [NewsController::class, 'show'])->name('news.show');

Route::redirect('/news', 'coming-soon');

// <--------------------- Clients ---------------------
Route::get('/clients', [ClientController::class, 'index'])->name('clients.index');
Route::get('/clients/{client}', [ClientController::class, 'show'])->name('clients.show');

Route::redirect('/clients', 'coming-soon');

// <--------------------- Blogs --------------------->
Route::resource('/blogs', BlogController::class);

Route::redirect('/blogs', 'coming-soon');

// <--------------------- Contact --------------------->
Route::get('/contact', [ContactController::class, 'index'])->name('contact.index');
Route::post('/contact', [ContactController::class, 'store'])->name('contact.store');


// <--------------------- Career --------------------->
Route::get('/career', [CareerController::class, 'index'])->name('career.index');


// <--------------------- Support --------------------->
Route::get('/support', [SupportController::class, 'index'])->name('support.index');


// <--------------------- FAQ --------------------->
Route::get('/FAQ', [FAQController::class, 'index'])->name('FAQ.index');


// <--------------------- Shared --------------------->
// <<--------------------- Call Back --------------------->>
Route::post('/callback', [CallBackFormController::class, 'store'])->name('callback.store');


// <--------------------- Fallbacks --------------------->
Route::fallback(function () {
    return view('404.404');
})->name('404');

Route::view('coming-soon', 'coming-soon.coming-soon');

// <--------------------- Language Switch --------------------->

// Route::get('lang', function (Request $request) {
//     $locale = $request->get('language');
//     if (in_array($locale, ['en', 'fr'])) {
//         session(['locale' => $locale]);
//     }
//     return redirect()->back();
// })->name('switch.lang');
