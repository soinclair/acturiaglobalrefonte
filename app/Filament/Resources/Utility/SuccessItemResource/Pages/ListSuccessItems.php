<?php

namespace App\Filament\Resources\Utility\SuccessItemResource\Pages;

use App\Filament\Resources\Utility\SuccessItemResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListSuccessItems extends ListRecords
{
    use ListRecords\Concerns\Translatable;

    protected static string $resource = SuccessItemResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
