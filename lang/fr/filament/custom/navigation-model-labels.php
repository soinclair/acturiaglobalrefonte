<?php

return [
    /* <---------- Public ----------> */

    /* <----- Home -----> */
    "homes" => "Sections de la page d'accueil",
    "home_abouts" => "À propos de nous",
    "home_clients" => "Nos Clients",
    "home_heroes" => "Section Principale",
    "slides" => "Diapositives d'images principales",
    "home_services" => "Nos Services",
    "home_successes" => "Histoires de Réussite",

    /* <----- About -----> */
    "abouts" => "Sections de la page À propos",
    "about_banners" => "Bannière supérieure",
    "about_vissions" => "Notre Vision",

    /* <---------- Layout ----------> */

    /* <----- Header -----> */
    "header" => "En-tête de la mise en page",
    "header_menu_items" => "Liens de navigation",
    "locations" => "Nos Emplacements",
    "logos" => "Logo de notre marque",
    "supports" => "Nos Coordonnées",

    /* <----- Footer -----> */
    "footer" => "Pied de page",
    "footer_menu_items" => "Liens de navigation",
    "copyrights" => "Droits d'auteur",
    "footer_contacts" => "Coordonnées",
    "footer_posts" => "Section des Articles",
    "footer_services" => "Section des Services",
    "subscribe_forms" => "Formulaire d'abonnement à la newsletter",

    /* <---------- Resources ----------> */
    "resources" => "Ressources",
    "services" => "Services",
    "service_details" => "Détails du Service",
    "subscribe_emails" => "E-mails abonnés",
    "success_items" => "Éléments de réussite",
];

