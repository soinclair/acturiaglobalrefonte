<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Translatable\HasTranslations;

return new class extends Migration
{
    

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('service_details', function (Blueprint $table) {
            $table->id();
            $table->json('title_paragraph');
            $table->string('image');
            $table->string('header1')->nullable();
            $table->text('paragraph1_1')->nullable();
            $table->text('paragraph2_1')->nullable();
            $table->string('header2')->nullable();
            $table->text('paragraph1_2')->nullable();
            $table->text('paragraph2_2')->nullable();
            $table->string('header3')->nullable();
            $table->text('paragraph1_3')->nullable();
            $table->foreignId('service_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('service_details');
    }
};
