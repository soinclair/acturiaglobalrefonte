<?php

namespace App\Filament\Resources\About\AboutBannerResource\Pages;

use App\Filament\Resources\About\AboutBannerResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditAboutBanner extends EditRecord
{
    use EditRecord\Concerns\Translatable;

    protected static string $resource = AboutBannerResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
