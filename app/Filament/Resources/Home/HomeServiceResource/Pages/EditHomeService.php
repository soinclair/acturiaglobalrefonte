<?php

namespace App\Filament\Resources\Home\HomeServiceResource\Pages;

use App\Filament\Resources\Home\HomeServiceResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditHomeService extends EditRecord
{

    use EditRecord\Concerns\Translatable;

    protected static string $resource = HomeServiceResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
