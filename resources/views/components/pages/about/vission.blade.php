<section class="vission-mission py-150 rpy-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <ul class="vission-tabs">
                    {{-- TODO: Make the image take the full size of the banner --}}
                    @foreach ($vissionItems as $vission )
                        <li @class(['wow', 'fadeInUp', 'active' => $loop->iteration == 2]) 
                            data-tab="tab_{{ $loop->iteration }}" 
                            data-wow-duration="1.5s"
                            style="background:url({{ asset( $vission->tabImage ) }});"
                        >
                            <h3> {{ $vission->tabTitle }} </h3>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-lg-6">
                <div class="vission-content-wrap">

                    @foreach ($vissionItems as $vission)
                        <div id="tab_{{ $loop->iteration }}" class="vission-tab-content @if($loop->iteration == 2) active @endif">
                            <div class="section-title">
                                <h2> {!! $vission->title !!} </h2>
                            </div>

                            <p>{!! nl2br( $vission->paragraph) !!}</p>
                            
                            {{-- TODO: Fix success item icon not showing, prob color --}}
                            <div class="success-item bg-snow">
                                <div class="success-icon">
                                    <i class="fa-{{ $vission->success->iconStyle }} fa-{{ $vission->success->icon }}"></i>
                                </div>
                                <div class="success-content">
                                    <span class="count-text @if( $vission->success->isPercentage) percentage @endif" data-speed="2500" data-stop="{{ $vission->success->number }}">0</span>
                                    <p> {{ $vission->success->label }} </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    

                    
                </div>
            </div>
        </div>
    </div>
</section>