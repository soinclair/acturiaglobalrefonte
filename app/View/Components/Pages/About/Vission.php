<?php

namespace App\View\Components\Pages\About;

use App\Models\About\AboutVission;
use App\Services\TextTransformer\TextTransformer;
use App\View\Components\Classes\SuccessItem;
use App\View\Components\Classes\VissionItem;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class Vission extends Component
{
    
   
    public array $vissionItems;

    public function __construct(Collection $aboutVissions, TextTransformer $textTransformer)
    {
        $this->vissionItems = $aboutVissions->map(function($item, $index) use($textTransformer){
            $item->title = $textTransformer->transformTextWithKeywords($item->title, $item->keywords)->getText();
            $item->paragraph = $textTransformer->transformTextWithLists($item->paragraph)->getText();

            return new VissionItem(
                $item->tab_title,
                'assets/'. $item->tab_image,
                $item->title,
                $item->paragraph,
                new SuccessItem(
                    $item->successItem->number ?? 99,
                    $item->successItem->icon ?? 'exclamation-triangle',
                    $item->successItem->icon_style ?? 'solid',
                    $item->successItem->label ?? 'error',
                    $item->successItem->is_percentage ?? false,
                )
            );
        })->toArray();
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.pages.about.vission');
    }
}
