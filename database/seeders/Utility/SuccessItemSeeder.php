<?php

namespace Database\Seeders\Utility;

use App\Models\About\AboutVission;
use App\Models\Home\HomeSuccess;
use App\Models\Utility\SuccessItem;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SuccessItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $successItemsForHome = [
            [
                'label' => [
                    'en' => "Years of Experience",
                    'fr' => "Années d'expérience",
                    'ar' => "سنوات الخبرة",
                ],
                'number' => 20,
                'icon' => 'calendar-days',
                'icon_style' => 'solid',
                'is_percentage' => false,
            ],
            [
                'label' => [
                    'en' => "Satisfied Clients",
                    'fr' => "Clients satisfaits",
                    'ar' => "عملاء راضون",
                ],
                'number' => 200,
                'icon' => 'smile',
                'icon_style' => 'solid',
                'is_percentage' => false,
            ],
            [
                'label' => [
                    'en' => "Countries of Intervention",
                    'fr' => "Pays d'intervention",
                    'ar' => "دول التدخل",
                ],
                'number' => 10,
                'icon' => 'globe',
                'icon_style' => 'solid',
                'is_percentage' => false,
            ],
            [
                'label' => [
                    'en' => "Retention Rate",
                    'fr' => "Taux de fidélisation",
                    'ar' => "معدل الاحتفاظ",
                ],
                'number' => 90,
                'icon' => 'thumbs-up',
                'icon_style' => 'solid',
                'is_percentage' => true,
            ]
        ];

        $successItemsForAbout = [
            [
                'label' => [
                    'en' => "Retention Rate",
                    'fr' => "Taux de fidélisation",
                    'ar' => "معدل الاحتفاظ",
                ],
                'number' => 90,
                'icon' => 'thumbs-up',
                'icon_style' => 'solid',
                'is_percentage' => true,
            ],
            [
                'label' => [
                    'en' => "Retention Rate",
                    'fr' => "Taux de fidélisation",
                    'ar' => "معدل الاحتفاظ",
                ],
                'number' => 90,
                'icon' => 'thumbs-up',
                'icon_style' => 'solid',
                'is_percentage' => true,
            ],
            [
                'label' => [
                    'en' => "Retention Rate",
                    'fr' => "Taux de fidélisation",
                    'ar' => "معدل الاحتفاظ",
                ],
                'number' => 90,
                'icon' => 'thumbs-up',
                'icon_style' => 'solid',
                'is_percentage' => true,
            ],
        ];

        $homeSuccess = HomeSuccess::find(1);
        $aboutVission = AboutVission::limit(count($successItemsForAbout))->get();

        foreach ($successItemsForHome as $attributes) {
            $item = SuccessItem::create($attributes);
            $homeSuccess->successItems()->save($item);
        }

        foreach ($successItemsForAbout as $index => $attributes) {
            $item = SuccessItem::create($attributes);
            $aboutVission[$index]->successItem()->save($item);
        }
    }
}
