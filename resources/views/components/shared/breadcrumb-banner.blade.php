<section class="page-banner overlay" style="--bg-banner-image:url({{ asset($image) }})">
    <div class="container">
        <div class="banner-inner">
            <h2 class="wow fadeInUp" data-wow-duration="1.5s"> {{ $title }} </h2>
            <nav aria-label="breadcrumb" class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.3s">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Accueil</a></li>
                    @foreach ($breadcrumbs as $page)
                        @if (Route::has($page . '.index'))
                            @if ($loop->last)
                                <li class="breadcrumb-item active" aria-current="page">{{ ucfirst($page) }}</li>
                            @else
                                <li class="breadcrumb-item"><a
                                        href="{{ route($page . '.index') }}">{{ ucfirst($page) }}</a></li>
                            @endif
                        @else
                            <li class="breadcrumb-item">{{ ucfirst($page) }}</li>
                        @endif
                    @endforeach
                </ol>
            </nav>
        </div>
    </div>
</section>
