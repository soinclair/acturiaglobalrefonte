<?php

namespace Database\Seeders;

use App\Models\User;
use Database\Seeders\About\AboutSeeder;
use Database\Seeders\Client\ClientSeeder;
use Database\Seeders\Home\HomeSeeder;
use Database\Seeders\Layout\LayoutSeeder;
use Database\Seeders\Post\PostSeeder;
use Database\Seeders\Service\ServiceSeeder;
use Database\Seeders\Utility\UtilitySeeder;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        $this->call([
            ClientSeeder::class,
            ServiceSeeder::class,
            LayoutSeeder::class,
            PostSeeder::class,
            HomeSeeder::class,
            AboutSeeder::class,
            UtilitySeeder::class,
        ]);
    }
}
