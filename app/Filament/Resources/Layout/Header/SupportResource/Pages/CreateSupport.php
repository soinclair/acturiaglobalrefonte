<?php

namespace App\Filament\Resources\Layout\Header\SupportResource\Pages;

use App\Filament\Resources\Layout\Header\SupportResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateSupport extends CreateRecord
{
    protected static string $resource = SupportResource::class;
}
