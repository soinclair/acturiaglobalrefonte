<?php

namespace App\Models\Layout\Header;

use App\Models\Layout\Layout;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Location extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        'label',
        'url',
    ];
    
    public $translatable = [
        "label",
    ];

    public function layout(){
        return $this->belongsTo(Layout::class);
    }
}
