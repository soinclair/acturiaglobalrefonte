<div class="nav-outer clearfix ml-lg-auto mr-lg-auto">
    {{-- ------------ Main Menu ------------ --}}
    <nav class="main-menu navbar-expand-lg">

        {{-- ------------ Responsive Hamburger Toggle Button ------------ --}}
        <div class="navbar-header clearfix">
            <button
                class="navbar-toggle"
                data-toggle="collapse"
                data-target=".navbar-collapse"
                type="button"
            >
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        {{-- ------------ Menu Items ------------ --}}
        <div class="navbar-collapse clearfix collapse">
            <ul class="navigation clearfix">
                @foreach ($menuItems as $menuItem)
                    @if ($menuItem->children->isNotEmpty())
                        <li
                            class="dropdown {{ Str::contains(url()->current(), strtolower($menuItem->label)) ? 'current-menu-item' : '' }}">

                            <a href="{{ route($menuItem->uri . '.index') }}">{{ $menuItem->label }}</a>
                            <ul>
                                    @foreach ($menuItem->children as $subPage)
                                        @if($loop->index % 2 === 0 && !($loop->last))
                                            <span class="d-flex justify-content-between">
                                                <li><a
                                                        href="{{ route($menuItem->uri . '.show', ['uri' => $subPage->uri]) }}">{{ $subPage->label }}</a>
                                                </li>
                                        @else
                                                <li><a
                                                        href="{{ route($menuItem->uri . '.show', ['uri' => $subPage->uri]) }}">{{ $subPage->label }}</a>
                                                </li>
                                            </span>
                                        @endif

                                    @endforeach
                            </ul>
                        </li>
                    @elseif($menuItem->parent === null)
                        <li
                            class="{{ Str::contains(url()->current(), strtolower($menuItem->label)) ? 'current-menu-item' : '' }}">
                            <a href="{{ route($menuItem->uri . '.index') }}"> {{ $menuItem->label }} </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>

    </nav>
    {{-- ------------ Main Menu End ------------ --}}
</div>
