<?php

namespace App\Filament\Resources\Layout\Header\LogoResource\Pages;

use App\Filament\Resources\Layout\Header\LogoResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListLogos extends ListRecords
{
    protected static string $resource = LogoResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
