<?php

return [
    /* <---------- Public ----------> */

    /* <----- Home -----> */
    "homes" => "أقسام الصفحة الرئيسية",
    "home_abouts" => "من نحن",
    "home_clients" => "عملاؤنا",
    "home_heroes" => "القسم الرئيسي",
    "slides" => "شرائح الصور الرئيسية",
    "home_services" => "خدماتنا",
    "home_successes" => "قصص النجاح",

    /* <----- About -----> */
    "abouts" => "أقسام صفحة من نحن",
    "about_banners" => "البانر العلوي",
    "about_vissions" => "رؤيتنا",

    /* <---------- Layout ----------> */

    /* <----- Header -----> */
    "header" => "رأس الصفحة",
    "header_menu_items" => "روابط التنقل",
    "locations" => "مواقعنا",
    "logos" => "شعار علامتنا التجارية",
    "supports" => "معلومات التواصل",

    /* <----- Footer -----> */
    "footer" => "تذييل الصفحة",
    "footer_menu_items" => "روابط التنقل",
    "copyrights" => "حقوق النشر",
    "footer_contacts" => "معلومات الاتصال",
    "footer_posts" => "قسم المنشورات",
    "footer_services" => "قسم الخدمات",
    "subscribe_forms" => "نموذج الاشتراك في النشرة الإخبارية",

    /* <---------- Resources ----------> */
    "resources" => "الموارد",
    "services" => "الخدمات",
    "service_details" => "تفاصيل الخدمة",
    "subscribe_emails" => "البريد الإلكتروني للمشتركين",
    "success_items" => "عناصر النجاح",
];
