<section class="error-section pt-200 rpt-100 pb-185 rpb-85">
    <div class="container">
        <div class="error-image text-center">
            <img src="{{asset('assets/images/error-page/404-error.png')}}" alt="Error Image">
        </div>
        <div class="error-content">
            <div class="content-left wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.3s">
                <h2>Pardon! </h2>
                <h3>La page que vous recherchez est introuvable.</h3>
            </div>
            <div class="content-right wow fadeInUp" data-wow-duration="1.5s">
                <a href="{{route('home.index')}}" class="theme-btn"><i class="fas fa-arrow-left"></i> Accueil </a>
            </div>
        </div>
    </div>
</section>