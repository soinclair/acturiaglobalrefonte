<?php

namespace App\View\Components\Pages\Home;

use App\Models\Home\HomeSuccess;
use App\View\Components\Classes\SuccessItem;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Success extends Component
{
    /**
     * Create a new component instance.
     */
    public array $successItems;

    public function __construct(
        HomeSuccess $homeSuccess
    )
    {   
        $this->successItems = $homeSuccess->successItems->map(function ($item) {
            return new SuccessItem(
                $item->number, 
                $item->icon,
                $item->icon_style, 
                $item->label,
                $item->is_percentage
            );
        })->toArray();
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.pages.home.success');
    }
}
