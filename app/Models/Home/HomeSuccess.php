<?php

namespace App\Models\Home;

use App\Models\Utility\SuccessItem;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeSuccess extends Model
{
    use HasFactory;

    public function home(){
        return $this->belongsTo(Home::class);
    }

    // TODO: Add HomeSuccess as filament resources and list these instead of global successItems Resource
    public function successItems()
    {
        return $this->morphMany(SuccessItem::class, 'success_itemable');
    }
}
