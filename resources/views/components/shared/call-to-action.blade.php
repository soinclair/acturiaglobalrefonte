<section class="cta-section bg-{{ $theme }} pt-130 rpt-80 pb-135 rpb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8">
                <div class="cta-text wow fadeInUp rmb-25" data-wow-duration="2s">
                    <h3> {{ $section->headers[0] }} </h3>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="cta-btn wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s">
                    <a href="{{ route($cta->path.'.index') }}" class="theme-btn"> {{ $cta->label }} <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

