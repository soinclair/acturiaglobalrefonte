<?php

namespace App\Filament\Resources\Layout\Footer\FooterPostResource\Pages;

use App\Filament\Resources\Layout\Footer\FooterPostResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListFooterPosts extends ListRecords
{
    use ListRecords\Concerns\Translatable;
    
    protected static string $resource = FooterPostResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
