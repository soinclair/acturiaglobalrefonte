<?php

namespace App\View\Components\Classes;

class Placeholder{

    public function __construct(
        
        public string $name,
        public string $email,
        public string $phoneNumber,
        public string $subject,
        public string $shortText,
    
    ){

    }

}