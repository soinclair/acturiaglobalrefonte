<?php

namespace App\View\Components\Classes;

class SuccessItem{

    public function __construct(
        
        public int $number,
        public string $icon,
        public string $iconStyle,
        public string $label,
        public bool $isPercentage = false
    ){
        
    }
}