<?php

namespace App\View\Components\Pages\About;

use App\Models\About\AboutBanner;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Banner extends Component
{
    public string $title;
    public string $image;

    public function __construct(AboutBanner $aboutBanner)
    {   
        $this->title = $aboutBanner->title;
        $this->image = '/assets/' . $aboutBanner->image;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.pages.about.banner');
    }
}
