<?php

namespace App\Filament\Resources\Layout\Footer\FooterContactResource\Pages;

use App\Filament\Resources\Layout\Footer\FooterContactResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateFooterContact extends CreateRecord
{
    use CreateRecord\Concerns\Translatable;

    protected static string $resource = FooterContactResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            // ...
        ];
    }
    
}
