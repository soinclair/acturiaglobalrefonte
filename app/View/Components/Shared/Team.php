<?php

namespace App\View\Components\Shared;

use App\View\Components\Classes\Image;
use App\View\Components\Classes\Member;
use App\View\Components\Classes\Section;
use App\View\Components\Classes\SocialMediaLink;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Team extends Component
{
    /**
     * Create a new component instance.
     */
    public Section $section;
    public array $members;

    public function __construct(){

        $this->section = new Section(["Nos Experts"],[],["Decouvrez nos fondateurs, des experts chevronnes avec une experience combinee de plus de 40 ans dans le domaine de l'actuariat. Siam Barki et Hachem Bouabdi dirigent notre equipe avec leur expertise, leur vision et leur passion pour offrir des solutions actuarielles de pointe"]);
        $image1 = new Image('Image 1',"hacheem.png");
        $image2 = new Image('Image 1',"siam.jpg");
        $socialMediaLinks1 = [
            new SocialMediaLink("facebook",'facebook.com/1'),
            new SocialMediaLink("twitter",'twitter.com/1'),
        ];
        $socialMediaLinks2 = [
            new SocialMediaLink("facebook",'facebook.com/2'),
            new SocialMediaLink("twitter",'twitter.com/2'),
        ];
        $member1 = new Member(1,"Hachem Bouabdi", "Directeur Associe", $image1, $socialMediaLinks1 );
        $member2 = new Member(2,"Siam Barki", "Directeur Associe", $image2, $socialMediaLinks2 );
        $this->members = [$member1,$member2];

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.shared.team');
    }
}
