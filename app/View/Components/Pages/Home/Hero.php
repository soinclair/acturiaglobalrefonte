<?php

namespace App\View\Components\Pages\Home;

use App\Models\Home\HomeHero;
use App\View\Components\Classes\CTA;
use App\View\Components\Classes\Slide;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Hero extends Component
{
    
    public array $slides;

    public function __construct(HomeHero $homeHero)
    {
        $this->slides = $homeHero->slides->map(function ($slide) {
            return new Slide(
                $slide->title, 
                new CTA($slide->cta_label, $slide->cta_path), 
                'assets/' . $slide->image
            );
        })->toArray();
    }

    public function render(): View|Closure|string
    {
        return view('components.pages.home.hero');
    }
}
