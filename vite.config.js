import { defineConfig } from "vite";
import laravel, { refreshPaths } from "laravel-vite-plugin";
import inject from "@rollup/plugin-inject";
import tailwindcss from "tailwindcss";
import autoprefixer from "autoprefixer";
import commonjs from 'vite-plugin-commonjs'; // CommonJS support
import legacy from '@vitejs/plugin-legacy'; // Legacy module support

export default defineConfig({
    plugins: [
        // Inject jQuery into the global scope
        inject({
            $: "jquery",
            jQuery: "jquery",
            include: [/\.js$/],
        }),

        // Laravel Vite plugin for Laravel integration
        laravel({
            input: [
                "resources/css/app.css", 
                "resources/js/app.js",
                "resources/css/filament/custom/filament-custom.css"
            ],
            refresh: true,
        }),

        // CommonJS plugin to handle `require` in frontend code
        commonjs(),

        // Legacy plugin for compatibility with older modules (like AMD)
        legacy({
            targets: ['defaults', 'not IE 11'], // Can be adjusted based on your needs
            additionalLegacyPolyfills: ['regenerator-runtime/runtime'], // Optional, for async/await support
        }),
    ],
    css: {
        postcss: {
            plugins: [
                tailwindcss(), // Tailwind CSS configuration
                autoprefixer(), // Autoprefixer for cross-browser compatibility
            ],
        },
    },
    optimizeDeps: {
        include: ["jquery"], // Ensure jQuery is included in the optimization step
    },
    build: {
        target: 'esnext', // Ensure build targets modern browsers
    },
});
