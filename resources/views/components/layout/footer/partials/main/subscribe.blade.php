<div class="col-lg-3 col-6">
    <div class="widget subscribe-widget">

        <h3 class="widget-title"> {{ $subscribeForm->title }} </h3>

        <form action="{{route('subscribe')}}" method="post" name="subscribe-form" class="subscribe-form">
            @csrf
           <p> {{ $subscribeForm->paragraph }} </p>
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="{{ $subscribeForm->input_placeholder }}" required autocomplete="on" > 
                <button type="submit" class="subscribe-submit"><i class="fa fa-envelope"></i></button>
            </div>
        </form>
        
        <div class="social-style-two mt-20">
            @foreach ($socialMediaLinks as $media)
                <a href="{{ $media->url }}" target="_blank"><i class="fab fa-{{ $media->label }}"></i></a>
            @endforeach
        </div>

    </div>
</div>