<?php

namespace App\Filament\Resources\Layout\Footer\SubscribeEmailResource\Pages;

use App\Filament\Resources\Layout\Footer\SubscribeEmailResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditSubscribeEmail extends EditRecord
{
    protected static string $resource = SubscribeEmailResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
