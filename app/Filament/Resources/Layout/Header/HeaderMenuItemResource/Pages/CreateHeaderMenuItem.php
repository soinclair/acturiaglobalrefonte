<?php

namespace App\Filament\Resources\Layout\Header\HeaderMenuItemResource\Pages;

use App\Filament\Resources\Layout\Header\HeaderMenuItemResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateHeaderMenuItem extends CreateRecord
{
    use CreateRecord\Concerns\Translatable;

    protected static string $resource = HeaderMenuItemResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            // ...
        ];
    }
}
