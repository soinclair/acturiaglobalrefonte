<?php

namespace App\Filament\Resources\Layout\Footer\FooterServiceResource\Pages;

use App\Filament\Resources\Layout\Footer\FooterServiceResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateFooterService extends CreateRecord
{
    use CreateRecord\Concerns\Translatable;

    protected static string $resource = FooterServiceResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            // ...
        ];
    }

}
