<?php

namespace App\Models\Home;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class HomeAbout extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        'title',
        'keywords',
        'paragraph',
        'italic_paragraph',
        'image',
        'cta_label',
        'cta_path',
        'home_id',
    ];

    public $translatable = [
        'title',
        'keywords',
        'paragraph',
        'italic_paragraph',
        'cta_label',
    ];



    public function home()
    {
        return $this->belongsTo(Home::class);
    }
}
