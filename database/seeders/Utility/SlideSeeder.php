<?php

namespace Database\Seeders\Utility;

use App\Models\Utility\Slide;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SlideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Slide::insert([
            [
                'cta_label' => json_encode([
                    'en' => 'Learn More',
                    'fr' => 'SAVOIR PLUS',
                    'ar' => 'تعرف على المزيد',
                ]),
                'cta_path' => 'contact',
                'title' => json_encode([
                    'en' => 'Consulting in Actuarial Science for Enterprises, Insurers, Mutuals, and Pension Funds',
                    'fr' => 'Conseil en Actuariat pour Entreprises, assureurs, mutuelles et caisses de retraite',
                    'ar' => 'استشارات في علم الاكتوارية للشركات، شركات التأمين، الجمعيات، وصناديق التقاعد',
                ]),
                'image' => 'images/hero/slide1.jpeg',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'cta_label' => json_encode([
                    'en' => 'Learn More',
                    'fr' => 'SAVOIR PLUS',
                    'ar' => 'تعرف على المزيد',
                ]),
                'cta_path' => 'contact',
                'title' => json_encode([
                    'en' => 'Moroccan Leader in Management Consulting and Negotiation of Corporate Insurance Programs',
                    'fr' => "Leader marocain du conseil en gestion et négociation des programmes d'assurance en entreprises",
                    'ar' => "الرائد المغربي في استشارات الإدارة والتفاوض على برامج التأمين للشركات",
                ]),
                'image' => 'images/hero/slide2.jpeg',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'cta_label' => json_encode([
                    'en' => 'Learn More',
                    'fr' => 'SAVOIR PLUS',
                    'ar' => 'تعرف على المزيد',
                ]),
                'cta_path' => 'contact',
                'title' => json_encode([
                    'en' => 'Moroccan Leader in Health Insurance and Mutuality Consulting',
                    'fr' => "Leader marocain du conseil en assurances maladies et mutualité",
                    'ar' => "الرائد المغربي في استشارات التأمين الصحي والتعاونيات",
                ]),
                'image' => 'images/hero/slide3.jpeg',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'cta_label' => json_encode([
                    'en' => 'Learn More',
                    'fr' => 'SAVOIR PLUS',
                    'ar' => 'تعرف على المزيد',
                ]),
                'cta_path' => 'contact',
                'title' => json_encode([
                    'en' => "Experts in Optimizing Employee Benefits (Retirement and Health) and Evaluating Social Commitments IAS 19",
                    'fr' => "Experts en optimisation des avantages sociaux (retraite et maladie) et en évaluation des engagements sociaux IAS 19",
                    'ar' => "خبراء في تحسين مزايا الموظفين (التقاعد والصحة) وتقييم الالتزامات الاجتماعية IAS 19",
                ]),
                'image' => 'images/hero/slide4.jpeg',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
