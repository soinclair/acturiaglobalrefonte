<?php

return [
    App\Providers\AppServiceProvider::class,
    App\Providers\Utility\Dev\LoadMigrationsProvider::class,
    App\Providers\Filament\AdminPanelProvider::class,
    App\Providers\Filament\ViteThemeRegisterProvider::class,
    App\Providers\FilamentNavigationServiceProvider::class,
    App\Providers\Filament\LanguageSwitcherProvider::class,
    App\Providers\Filament\AutoTranslateLabelsProvider::class,
];
