<?php

namespace App\View\Components\Classes;

class Image{

    public function __construct(
        
        public string $label,
        public string $path
       
    ){

    }

}