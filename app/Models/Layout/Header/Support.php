<?php

namespace App\Models\Layout\Header;

use App\Models\Layout\Layout;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    use HasFactory;

    protected $fillable = [
        'email',
        'phone'
    ];

    public function layout(){
        return $this->belongsTo(Layout::class);
    }
}
