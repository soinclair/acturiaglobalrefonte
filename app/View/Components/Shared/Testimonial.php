<?php

namespace App\View\Components\Shared;

use App\View\Components\Classes\Image;
use App\View\Components\Classes\Review;
use App\View\Components\Classes\ReviewButton;
use App\View\Components\Classes\Section;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Testimonial extends Component
{
    /**
     * Create a new component instance.
     */

    public Section $section;
    public array $reviews;
    public array $reviewButtons;

    public function __construct(){
        $this->section = new Section(["Header1","Header2"],[],["paragraph"]);
        $imgRevBtn1 = new Image('image Review1','assets/images/testimonials/testi-small1.jpg');
        $imgRevBtn2 = new Image('image Review2','assets/images/testimonials/testi-small2.jpg');
        $revBtn1 = new ReviewButton(1,$imgRevBtn1,'testimonial1');
        $revBtn2 = new ReviewButton(2,$imgRevBtn2,'testimonial2');
        $this->reviewButtons = [$revBtn1,$revBtn2];

        $imgRev1 = new Image('image Review1','assets/images/testimonials/testi-big1.jpg');
        $imgRev2 = new Image('image Review2','assets/images/testimonials/testi-big2.jpg');
        $rev1= new Review(1,'reviewer 1','his postion 1','Lorem ipsum dolor sit, amet consectetur adipisicing elit. Placeat quas similique beatae doloremque voluptatem facere sit omnis alias repellendus rerum ipsa et, temporibus aliquid id? Culpa voluptatibus perferendis provident aut.'
        ,$imgRev1);
        $rev2= new Review(2,'reviewer 2','his postion 2','Lorem ipsum dolor sit, amet consectetur adipisicing elit. Placeat quas similique beatae doloremque voluptatem facere sit omnis alias repellendus rerum ipsa et, temporibus aliquid id? Culpa voluptatibus perferendis provident aut.'
        ,$imgRev2);
        $this->reviews = [$rev1,$rev2];
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.shared.testimonial');
    }
}
