<?php

namespace App\Services\TextTransformer;

class TextTransformer
{
    public function transformTextWithLists($text)
    {
        $transformResult = new TextTransformerResult($text);
        return $transformResult->transformTextWithLists();
    }

    
    public function removeAccents($text)
    {  
        $transformResult = new TextTransformerResult($text);
        return $transformResult->removeAccents();

    }

    function transformTextWithKeywords($text, $keywords) {
        $transformResult = new TextTransformerResult($text);
        return $transformResult->transformTextWithKeywords($keywords);
    }
}

