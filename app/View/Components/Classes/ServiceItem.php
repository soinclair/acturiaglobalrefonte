<?php

namespace App\View\Components\Classes;

class ServiceItem{

    public function __construct(
        
        public string $label,
        public string $paragraph,
        public string $icon,
        public string $uri
        
    ){

    }
}