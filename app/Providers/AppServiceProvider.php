<?php

namespace App\Providers;

use App\Models\Layout\Layout;
use App\Models\Post\Post;
use App\Models\Service\Service;
use App\Models\User;
use App\Providers\Filament\LanguageSwitcherProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        
        /* Share data on all views */
        View::composer('layouts.app', function ($view) {
            $layout = Layout::first();
            $recentPosts = Post::orderBy('created_at','desc')->limit(2)->get();
            $services = Service::limit(5)->get();

            $view->with('layout', $layout);
            $view->with('recentPosts', $recentPosts);
            $view->with('services', $services);
        });
  
    }
}
