<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: Add order property, consider adding it in other models
        Schema::create('success_items', function (Blueprint $table) {
            $table->id();
            $table->json('label');
            $table->integer('number');
            $table->string('icon');
            $table->enum('icon_style', ['solid', 'regular', 'light', 'thin']);
            $table->boolean('is_percentage')->default(false);
            $table->unsignedBigInteger('success_itemable_id')->nullable();
            $table->string('success_itemable_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('success_items');
    }
};
