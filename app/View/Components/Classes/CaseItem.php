<?php

namespace App\View\Components\Classes;

class CaseItem{

    public function __construct(
        
        public int $id,
        public string $label,
        public Category $category,
        public string $uri,
        public Image $image
        
    ){

    }
}