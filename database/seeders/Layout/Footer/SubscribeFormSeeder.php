<?php

namespace Database\Seeders\Layout\Footer;

use App\Models\Layout\Footer\SubscribeForm;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SubscribeFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        SubscribeForm::insert([
            [
                'title' => json_encode([
                    'en' => 'Subscribe Now!',
                    'fr' => 'Abonnez-vous maintenant !',
                    'ar' => 'اشترك الآن!',
                ]),
                'paragraph' => json_encode([
                    'en' => 'Subscribe to our newsletter for the latest updates.',
                    'fr' => 'Abonnez-vous à notre newsletter pour les dernières mises à jour.',
                    'ar' => 'اشترك في نشرتنا الإخبارية للحصول على آخر التحديثات.',
                ]),
                'input_placeholder' => json_encode([
                    'en' => 'Email address',
                    'fr' => 'Adresse e-mail',
                    'ar' => 'عنوان بريدك الإلكتروني',
                ]),
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
