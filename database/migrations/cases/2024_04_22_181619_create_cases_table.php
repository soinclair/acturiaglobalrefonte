<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('cases', function (Blueprint $table) {
            $table->id();
            $table->string('label');
            $table->string('uri');
            $table->string('image_label');
            $table->string('image_path');
            $table->foreignId('case_category_id');
            $table->foreignId('home_case_id')->default(1);
            $table->timestamps();
        });
    }      //belongsTo Case //belongsTo CaseCategory 
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cases');
    }
};
