<?php

namespace App\Filament\Resources\Home\HomeClientResource\Pages;

use App\Filament\Resources\Home\HomeClientResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListHomeClients extends ListRecords
{
    use ListRecords\Concerns\Translatable;
    protected static string $resource = HomeClientResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
