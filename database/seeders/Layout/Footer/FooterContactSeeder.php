<?php

namespace Database\Seeders\Layout\Footer;

use App\Models\Layout\Footer\FooterContact;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FooterContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        FooterContact::insert([
            [
                'title' => json_encode([
                    'en' => 'Contact Us',
                    'fr' => 'Contactez Nous',
                    'ar' => 'اتصل بنا',
                ]),
                'address_title' => json_encode([
                    'en' => 'Our Address',
                    'fr' => 'Notre Address',
                    'ar' => 'عنواننا',
                ]),
                'address' => json_encode([
                    'en' => '445 A, Bd Abdelmoumen, Casablanca, Morocco.',
                    'fr' => '445 A, Bd Abdelmoumen, Casablanca, Maroc.',
                    'ar' => '445 أ، شارع عبد المومن، الدار البيضاء، المغرب.',
                ]),
                'contact_number_title' => json_encode([
                    'en' => 'Contact Number',
                    'fr' => 'Numero de contact',
                    'ar' => 'رقم الاتصال',
                ]),
                'contact_number' => '+212 (0) 5 22 86 28 88',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'title' => json_encode([
                    'en' => 'N/A',
                    'fr' => 'N/A',
                    'ar' => 'N/A',
                ]),
                'address_title' => json_encode([
                    'en' => 'N/A',
                    'fr' => 'N/A',
                    'ar' => 'N/A',
                ]),
                'address' => json_encode([
                    'en' => 'N/A',
                    'fr' => 'N/A',
                    'ar' => 'N/A',
                ]),
                'contact_number_title' => json_encode([
                    'en' => 'N/A',
                    'fr' => 'N/A',
                    'ar' => 'N/A',
                ]),
                'contact_number' => '+212 (0) 5 22 86 28 86',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
