<section class="hero-section overlay">
    <div class="container-fluid px-0">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">

                @foreach ($slides as $slide)
                    <div class="carousel-item @if($loop->first) active @endif">
                        <img src="{{ asset( $slide->image )  }}"  alt="A carousel image"/>
                        <div class="container">
                            <div class="carousel-item-title">
                                <h3> {!! nl2br( e( $slide->title )) !!} </h3>

                                <a href="{{ route( $slide->cta->path . '.index' )}}" class="theme-btn wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                                    {{ $slide->cta->label }}
                                    <i class="fas fa-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <div class="container carousel-control-buttons">
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-next-icon bg-white rounded shadow d-inline-block" aria-hidden="true">
                        <i class="fas fa-angle-double-left text-black"></i>
                    </span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon bg-white rounded shadow d-inline-block" aria-hidden="true">
                        <i class="fas fa-angle-double-right text-black"></i>
                    </span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
  </div>
</section>
