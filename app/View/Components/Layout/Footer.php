<?php

namespace App\View\Components\Layout;

use App\Models\Layout\Footer\Copyright;
use App\Models\Layout\Footer\FooterPost;
use App\Models\Layout\Footer\FooterService;
use App\Models\Layout\Footer\SubscribeForm;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class Footer extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public Collection $contacts,
        public FooterService $footerService,
        public Collection $services,
        public Collection $recentPosts,
        public FooterPost $footerPost,
        public Collection $socialMediaLinks,
        public SubscribeForm $subscribeForm,
        public Collection $menuItems,
        public Copyright $copyright
    ){
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.layout.footer.footer');
    }
}
