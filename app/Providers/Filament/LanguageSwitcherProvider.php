<?php

namespace App\Providers\Filament;

use BezhanSalleh\FilamentLanguageSwitch\LanguageSwitch;
use Illuminate\Support\ServiceProvider;

class LanguageSwitcherProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        LanguageSwitch::configureUsing(function (LanguageSwitch $switch) {
            $switch
                ->locales(['ar','en','fr'])
                ->visible(outsidePanels: true)
                ->flags([
                    'ar' => asset('assets/svg/flags/sa.svg'),
                    'fr' => asset('assets/svg/flags/fr.svg'),
                    'en' => asset('assets/svg/flags/us.svg'),
                ]);

        });
    }
}
