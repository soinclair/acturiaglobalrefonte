<?php

namespace App\Models\Layout;

use App\Models\Layout\Footer\Copyright;
use App\Models\Layout\Footer\FooterContact;
use App\Models\Layout\Footer\FooterMenuItem;
use App\Models\Layout\Footer\FooterPost;
use App\Models\Layout\Footer\FooterService;
use App\Models\Layout\Footer\SubscribeForm;

use App\Models\Layout\Header\HeaderMenuItem;
use App\Models\Layout\Header\Logo;
use App\Models\Layout\Header\Location;
use App\Models\Layout\Header\Support;

use App\Models\Layout\Shared\SocialMediaLink;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Layout extends Model
{
    use HasFactory;
    /* --------------- Header Models -------------- */

    public function location(){
        return $this->hasOne(Location::class);
    }

    public function headerMenuItems(){
        return $this->hasMany(HeaderMenuItem::class);
    }

    public function socialMediaLinks(){
        return $this->hasMany(SocialMediaLink::class);
    }

    public function supports(){
        return $this->hasMany(Support::class);
    }

    /* --------------- Footer Models -------------- */
    public function subscribeForm(){
        return $this->hasOne(SubscribeForm::class);
    }
    
    public function copyright(){
        return $this->hasOne(Copyright::class);
    }

    public function logo(){
        return $this->hasOne(Logo::class);
    }

    public function footerContacts(){
        return $this->hasMany(FooterContact::class);
    }

    public function footerPost(){
        return $this->hasOne(FooterPost::class);
    }

    public function footerService(){
        return $this->hasOne(FooterService::class);
    }

    public function footerMenuItems(){
        return $this->hasMany(FooterMenuItem::class);
    }
}
