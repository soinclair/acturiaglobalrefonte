<?php

namespace App\Filament\Resources\Layout\Footer;

use App\Filament\Resources\Layout\Footer\SubscribeFormResource\Pages;
use App\Filament\Resources\Layout\Footer\SubscribeFormResource\RelationManagers;
use App\Models\Layout\Footer\SubscribeForm;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class SubscribeFormResource extends Resource
{
    use Translatable;

    protected static ?string $model = SubscribeForm::class;

    protected static ?string $navigationIcon = 'heroicon-o-envelope';

    protected static ?string $activeNavigationIcon = 'heroicon-s-envelope';

    protected static ?string $modelLabel = 'subscribe_forms';

    protected static ?string $navigationGroup = 'footer';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('title')
                    ->required()
                    ->label(static::getDynamicLabel("title"))
                    ->translateLabel(),
                Forms\Components\Textarea::make('paragraph')
                    ->required()
                    ->label(static::getDynamicLabel("paragraph"))
                    ->translateLabel(),
                Forms\Components\TextInput::make('input_placeholder')
                    ->required()
                    ->label(static::getDynamicLabel("input_placeholder"))
                    ->translateLabel(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->searchable()
                    ->sortable()
                    ->label(static::getDynamicLabel("title"))
                    ->translateLabel(),
                Tables\Columns\TextColumn::make('paragraph')
                    ->searchable()
                    ->limit(50)
                    ->label(static::getDynamicLabel("paragraph"))
                    ->translateLabel(),
                Tables\Columns\TextColumn::make('input_placeholder')
                    ->searchable()
                    ->sortable()
                    ->label(static::getDynamicLabel("input_placeholder"))
                    ->translateLabel(),
                // Tables\Columns\TextColumn::make('created_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->label(static::getDynamicLabel("created_at"))
                //     ->translateLabel(),
                // Tables\Columns\TextColumn::make('updated_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->label(static::getDynamicLabel("updated_at"))
                //     ->translateLabel(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSubscribeForms::route('/'),
            'create' => Pages\CreateSubscribeForm::route('/create'),
            'edit' => Pages\EditSubscribeForm::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch plural translation without "s"
    }

    public static function getModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch translation based on current locale
    }

    public static function getNavigationGroup(): ?string
    {
        return __("filament/custom/navigation-model-labels." . static::$navigationGroup); // Fetch translation based on current locale
    }

    protected static function getDynamicLabel(string $field): string
    {
        return __("model-properties." . static::$modelLabel . '-' . $field);
    }
}
