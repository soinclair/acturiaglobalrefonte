<?php

namespace App\Filament\Resources\Home\HomeAboutResource\Pages;

use App\Filament\Resources\Home\HomeAboutResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditHomeAbout extends EditRecord
{
    use EditRecord\Concerns\Translatable;

    protected static string $resource = HomeAboutResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
