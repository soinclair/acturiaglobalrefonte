@extends('layouts.app')
@section('about.index')
        <x-pages.about.banner 
            :about-banner="$about->aboutBanner"
        />

        <x-pages.about.vission
            :about-vissions="$about->aboutVissions"
        />

        <x-shared.call-back 
        {{-- 
            :image="" 
            :section="" 
            :placeholder="" 
            :cta=""  
        --}}
        />   

        {{-- <x-shared.team

            :section="" 
            :members=""  

        /> --}}

        <x-shared.call-to-action 
        {{-- 
            :section=""
            :cta=""
        --}}
            :theme="'snow'"
        />

        {{-- <x-shared.testimonial 
         
            :section="" 
            :reviews="" 
            :reviewButtons=""  
        
        /> --}}

@endsection

       
      
