<?php

namespace Database\Seeders\Home;

use App\Models\Home\HomeService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        HomeService::insert([
            [
                'title' => json_encode([
                    'en' => 'Our Services',
                    'fr' => 'Nos Services.',
                    'ar' => 'خدماتنا.',
                ]),
                'paragraph' => json_encode([
                    'en' => 'We offer a diverse range of services designed to meet your specific needs. Whether you are a small business looking to understand and manage your risks, or a large corporation requiring in-depth analysis to optimize your financial strategies.',
                    'fr' => 'Nous offrons une palette diversifiée de services conçus pour répondre à vos besoins spécifiques. Que vous soyez une petite entreprise cherchant à comprendre et à gérer ses risques, ou une grande entreprise nécessitant une analyse approfondie pour optimiser ses stratégies financières.',
                    'ar' => 'نحن نقدم مجموعة متنوعة من الخدمات المصممة لتلبية احتياجاتك المحددة. سواء كنت شركة صغيرة تسعى لفهم وإدارة مخاطرها، أو شركة كبيرة تتطلب تحليلاً عميقاً لتحسين استراتيجياتها المالية.',
                ]),
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
