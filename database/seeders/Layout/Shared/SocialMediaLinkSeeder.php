<?php

namespace Database\Seeders\Layout\Shared;

use App\Models\Layout\Shared\SocialMediaLink;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SocialMediaLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        SocialMediaLink::insert([
            [
                'label' => 'facebook',
                'url' => 'https://www.facebook.com/ACTUARIA-GLOBAL-882878798456205',
                 
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'label' => 'linkedin',
                'url' => 'https://www.linkedin.com/company/actuaria-global/',
                
                'created_at' => now(),
                'updated_at' => now(),
            ],
            
        ]);
    }
}
