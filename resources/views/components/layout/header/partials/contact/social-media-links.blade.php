<div class="top-right ml-auto">
    <div class="social-style-one">
        @foreach ($socialMediaLinks as $media)
            <a
                href={{ $media->url }}
                target="_blank"
            ><i class="fab fa-{{ $media->label }}"></i></a>
        @endforeach
    </div>
</div>
