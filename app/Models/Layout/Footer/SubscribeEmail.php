<?php
namespace App\Models\Layout\Footer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscribeEmail extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'email',
    ];

    protected $casts = [
        'email' => 'string',
    ];
    
}
