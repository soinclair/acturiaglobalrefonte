<?php

namespace App\Filament\Resources\About\AboutVissionResource\Pages;

use App\Filament\Resources\About\AboutVissionResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateAboutVission extends CreateRecord
{
    use CreateRecord\Concerns\Translatable;

    protected static string $resource = AboutVissionResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            // ...
        ];
    }

}
