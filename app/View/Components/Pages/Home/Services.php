<?php

namespace App\View\Components\Pages\Home;

use App\Models\Home\HomeService;
use App\View\Components\Classes\ServiceItem;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Services extends Component
{
    /**
     * Create a new component instance.
     */
    public string $title;
    public string $paragraph;
    public array $topServices;
    public array $bottomServices;

    public function __construct(
        HomeService $homeService
    )
    {   
        $services = $homeService->services->map(function($service){
            return new ServiceItem($service->label, $service->serviceDetail->title_paragraph, 'assets/'.$service->icon, $service->uri);
        })->toArray();

        $this->title = $homeService->title;
        $this->paragraph = $homeService->paragraph;
        $this->topServices = array_slice($services, 0, 4); 
        $this->bottomServices = array_slice($services, 4, 4); 
        
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.pages.home.services');
    }
}
