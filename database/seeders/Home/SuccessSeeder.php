<?php

namespace Database\Seeders\Home;

use App\Models\Home\HomeSuccess;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SuccessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {   
        HomeSuccess::insert([
            [
                'created_at' => now(), 
                'updated_at' => now()
            ],
        ]);
    }
}
