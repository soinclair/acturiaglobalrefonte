<?php

namespace App\Filament\Resources\Layout\Footer\SubscribeEmailResource\Pages;

use App\Filament\Resources\Layout\Footer\SubscribeEmailResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListSubscribeEmails extends ListRecords
{
    protected static string $resource = SubscribeEmailResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
