<?php

return [
    /* <-------------------- Layout Models --------------------> */

    /* <----------- Header Models -----------> */

    /* <--- Support ------> */
    "supports-email" => "Email",
    "supports-phone" => "Phone",

    /* <--- Location ------> */
    "locations-label" => "Location Name",
    "locations-url" => "Map Link",

    /* <--- HeaderMenuItem ------> */
    "header_menu_items-label" => "Menu Name",
    "header_menu_items-uri" => "Link",
    "header_menu_items-parent_id" => "Parent Menu",

    /* <--- Logo ------> */
    "logos-image" => "Logo",

    /* <----------- Footer Models -----------> */

    /* <--- SubscribeForm ------> */
    "subscribe_forms-title" => "Form Title",
    "subscribe_forms-paragraph" => "Form Description",
    "subscribe_forms-input_placeholder" => "Email Placeholder",

    /* <--- SubscribeEmail ------> */
    "subscribe_emails-email" => "Subscriber Email",

    /* <--- FooterMenuItem ------> */
    "footer_menu_items-label" => "Menu Name",
    "footer_menu_items-uri" => "Link",

    /* <--- Copyright ------> */
    "copyrights-paragraph" => "Copyright Text",

    /* <--- FooterService ------> */
    "footer_services-title" => "Service Title",

    /* <--- FooterPost ------> */
    "footer_posts-title" => "Post Title",

    /* <--- FooterContact ------> */
    "footer_contacts-title" => "Section Title",
    "footer_contacts-address_title" => "Address Label",
    "footer_contacts-address" => "Address",
    "footer_contacts-contact_number_title" => "Phone Label",
    "footer_contacts-contact_number" => "Phone",

    /* <-------------------- Home Models --------------------> */

    /* <----------- HomeAbout -----------> */
    "home_abouts-title" => "Title",
    "home_abouts-keywords" => "Keywords",
    "home_abouts-paragraph" => "Description",
    "home_abouts-italic_paragraph" => "Highlighted Text",
    "home_abouts-image" => "Image",
    "home_abouts-cta_label" => "CTA Label",
    "home_abouts-cta_path" => "CTA Link",

    /* <----------- HomeClient -----------> */
    "home_clients-title" => "Clients Title",

    /* <----------- HomeServices -----------> */
    "home_services-title" => "Section Title",
    "home_services-paragraph" => "Section Description",

    /* <-------------------- About Models --------------------> */

    /* <----------- AboutBanner -----------> */
    "about_banners-title" => "Banner Title",
    "about_banners-image" => "Banner Image",

    /* <----------- AboutVission -----------> */
    "about_vissions-tab_title" => "Tab Title",
    "about_vissions-tab_image" => "Tab Image",
    "about_vissions-title" => "Title",
    "about_vissions-keywords" => "Keywords",
    "about_vissions-paragraph" => "Description",

    /* <-------------------- Contact Models --------------------> */

    /* <----------- Contact -----------> */
    "contacts-name" => "Name",
    "contacts-email" => "Email",
    "contacts-phone_number" => "Phone",
    "contacts-subject" => "Subject",
    "contacts-message" => "Message",

    /* <-------------------- Service Models --------------------> */

    /* <----------- Service -----------> */
    "services-label" => "Name",
    "services-uri" => "Link",
    "services-icon" => "Icon",

    /* <----------- ServiceDetail -----------> */
    "service_details-title_paragraph" => "Title",
    "service_details-image" => "Image",
    "service_details-header1" => "Header 1",
    "service_details-paragraph1_1" => "Paragraph 1.1",
    "service_details-paragraph2_1" => "Paragraph 2.1",
    "service_details-header2" => "Header 2",
    "service_details-paragraph1_2" => "Paragraph 1.2",
    "service_details-paragraph2_2" => "Paragraph 2.2",
    "service_details-header3" => "Header 3",
    "service_details-paragraph1_3" => "Paragraph 1.3",

    /* <-------------------- Post Models --------------------> */

    /* <----------- Post -----------> */
    "posts-title" => "Title",
    "posts-uri" => "Link",
    "posts-date" => "Date",

    /* <-------------------- Client Models --------------------> */

    /* <----------- Client -----------> */
    "clients-name" => "Name",
    "clients-logo" => "Logo",

    /* <-------------------- Utility Models --------------------> */

    /* <----------- SuccessItem  -----------> */
    "success_items-label" => "Label",
    "success_items-number" => "Number",
    "success_items-icon" => "Icon",
    "success_items-icon_style" => "Icon Style",
    "success_items-is_percentage" => "Percentage?",

    /* <----------- Slide  -----------> */
    "slides-title" => "Title",
    "slides-image" => "Image",
    "slides-cta_label" => "CTA Label",
    "slides-cta_path" => "CTA Link"
];
