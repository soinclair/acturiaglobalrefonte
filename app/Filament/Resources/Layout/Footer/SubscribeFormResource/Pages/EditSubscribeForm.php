<?php

namespace App\Filament\Resources\Layout\Footer\SubscribeFormResource\Pages;

use App\Filament\Resources\Layout\Footer\SubscribeFormResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditSubscribeForm extends EditRecord
{
    use EditRecord\Concerns\Translatable;

    protected static string $resource = SubscribeFormResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
