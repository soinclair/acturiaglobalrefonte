<section class="cases-section bg-black pt-140 rpt-90 pb-150 rpb-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title text-center mb-80 wow fadeInUp" data-wow-duration="2s">
                    <h2> {{ $section->headers[0] }} </h2>
                    <p> {{ $section->paragraphs[0] }} </p>
                </div>
            </div>
        </div>
        <div class="case-wrap">
            @foreach ($cases as $case)
                <div itemscope itemtype="http://schema.org/Service" itemid="{{ url()->current() }}/services/{{ $case->id }}" class="case-item wow fadeInUp" data-wow-duration="2s">
                    <div class="case-image">
                        <img src="{{ $case->image->path }}" alt="{{ $case->image->label }}" itemprop="image">
                        <a href="{{ $case->uri }}"><i class="fas fa-angle-double-right"></i></a>
                    </div>
                    <div class="case-content">
                        <span itemprop="category">{{ $case->category->label }}</span>
                        <h4><a href="{{ $case->uri }}" itemprop="name">{{ $case->label }}</a></h4>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>