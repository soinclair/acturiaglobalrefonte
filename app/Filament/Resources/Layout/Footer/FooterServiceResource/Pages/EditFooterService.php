<?php

namespace App\Filament\Resources\Layout\Footer\FooterServiceResource\Pages;

use App\Filament\Resources\Layout\Footer\FooterServiceResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditFooterService extends EditRecord
{
    use EditRecord\Concerns\Translatable;

    protected static string $resource = FooterServiceResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),

        ];
    }
}
