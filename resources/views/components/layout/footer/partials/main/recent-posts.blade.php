<div class="col-lg-3 col-6">
    <div class="widget recent-post-widget">
        <h3 class="widget-title"> {{ $footerPost->title }} </h3>
        <ul>
            @foreach ($recentPosts as $post)
                <li>
                    <div class="post-item">
                        <h5><a href="{{ $post->uri }}"> {{ $post->title }} </a></h5>
                        <p> {{ $post->date }} </p>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>