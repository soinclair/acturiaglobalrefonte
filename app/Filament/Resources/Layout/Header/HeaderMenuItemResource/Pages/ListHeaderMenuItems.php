<?php

namespace App\Filament\Resources\Layout\Header\HeaderMenuItemResource\Pages;

use App\Filament\Resources\Layout\Header\HeaderMenuItemResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListHeaderMenuItems extends ListRecords
{
    use ListRecords\Concerns\Translatable;
    
    protected static string $resource = HeaderMenuItemResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
