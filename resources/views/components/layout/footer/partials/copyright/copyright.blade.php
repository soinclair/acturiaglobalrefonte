<div class="copyright-wrap order-2 order-md-1">
    <p> {{ $copyright->paragraph }} </p>
    <!-- Scroll Top Button -->
    <button class="scroll-top scroll-to-target wow fadeInUp" data-wow-duration="2s" data-target="html"><i class="fas fa-angle-double-up"></i></button>
</div>