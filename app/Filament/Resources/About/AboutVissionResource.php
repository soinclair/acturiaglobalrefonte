<?php

namespace App\Filament\Resources\About;

use App\Filament\Resources\About\AboutVissionResource\Pages;
use App\Filament\Resources\About\AboutVissionResource\RelationManagers;
use App\Models\About\AboutVission;
use Filament\Forms;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\TagsInput;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class AboutVissionResource extends Resource
{

    use Translatable;

    protected static ?string $model = AboutVission::class;

    protected static ?string $navigationIcon = 'heroicon-o-light-bulb';

    protected static ?string $activeNavigationIcon = 'heroicon-s-light-bulb';

    protected static ?string $modelLabel = 'about_vissions';

    protected static ?string $navigationGroup = 'abouts';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                // TODO: Internationalize the Sections title & Tabs as well in their own files
                Section::make('Title Section')->schema([
                    TextInput::make('title')
                        ->required()
                        ->maxLength(255)
                        ->label(static::getDynamicLabel("title"))
                        ->translateLabel(),

                    // TODO: Replace the Tags with filament Tags plugin
                    TextInput::make('keywords')
                        ->hint(static::getDynamicFieldProp("keywords", "hint"))
                        ->label(static::getDynamicLabel("keywords"))
                        ->translateLabel(),
                ])->columnSpan(1)
                    ->extraAttributes(['style' => 'min-height: 20rem;']),

                Section::make('Tab Section')->schema([
                    TextInput::make('tab_title')
                        ->required()
                        ->maxLength(255)
                        ->label(static::getDynamicLabel("tab_title"))
                        ->translateLabel(),
                    FileUpload::make('tab_image')
                        ->acceptedFileTypes(['image/png'])
                        ->disk('assets')
                        ->directory('images/abouts')
                        ->required()
                        ->preserveFilenames()
                        ->label(static::getDynamicLabel("tab_image"))
                        ->translateLabel(),
                ])->columnSpan(1)
                    ->extraAttributes(['style' => 'min-height: 20rem;']),

                Section::make('Paragraph Section')
                    ->description('For using lists, use • before list')
                    ->schema([
                        Textarea::make('paragraph')
                            ->required()
                            ->label(static::getDynamicLabel("paragraph"))
                            ->translateLabel(),
                    ])->columnSpan(1),
            ])->columns(1);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('tab_title')
                    ->sortable()
                    ->searchable()
                    ->toggleable()
                    ->label(static::getDynamicLabel("tab_title"))
                    ->translateLabel(),
                ImageColumn::make('tab_image')
                    ->disk('assets')
                    ->square()
                    ->toggleable()
                    ->label(static::getDynamicLabel("tab_image"))
                    ->translateLabel(),
                TextColumn::make('title')
                    ->sortable()
                    ->searchable()
                    ->toggleable()
                    ->label(static::getDynamicLabel("title"))
                    ->translateLabel(),
                TextColumn::make('keywords')
                    ->sortable()
                    ->searchable()
                    ->toggleable()
                    ->label(static::getDynamicLabel("keywords"))
                    ->translateLabel(),
                TextColumn::make('paragraph')
                    ->limit(50)
                    ->tooltip(fn($record) => $record->paragraph)
                    ->toggleable()
                    ->label(static::getDynamicLabel("paragraph"))
                    ->translateLabel(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAboutVissions::route('/'),
            'create' => Pages\CreateAboutVission::route('/create'),
            'edit' => Pages\EditAboutVission::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch plural translation without "s"
    }

    public static function getModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch translation based on current locale
    }

    public static function getNavigationGroup(): ?string
    {
        return __("filament/custom/navigation-model-labels." . static::$navigationGroup); // Fetch translation based on current locale
    }

    protected static function getDynamicLabel(string $field): string
    {
        return __("model-properties." . static::$modelLabel . '-' . $field);
    }

    public static function getModelName(): string
    {
        $fullClassName = static::$model;

        return basename(str_replace('\\', '/', $fullClassName));
    }

    public static function getDynamicFieldProp(string $field, string $prop): string
    {
        $parentDirName = basename(dirname(__FILE__));
        $path = "filament/custom/resources/$parentDirName/" . static::getModelName();
        return __($path . "."  .  $field . "-" . $prop);
    }
}
