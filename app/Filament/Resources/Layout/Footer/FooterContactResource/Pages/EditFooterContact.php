<?php

namespace App\Filament\Resources\Layout\Footer\FooterContactResource\Pages;

use App\Filament\Resources\Layout\Footer\FooterContactResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditFooterContact extends EditRecord
{
    use EditRecord\Concerns\Translatable;
    
    protected static string $resource = FooterContactResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
