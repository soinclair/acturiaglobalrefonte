<?php

namespace App\Http\Controllers;

use App\Models\Layout\Footer\SubscribeEmail;
use Illuminate\Http\Request;

class SubscribeFormController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'email'=>'required|email|unique:subscribe_emails,email'
        ]);

        $subscription = SubscribeEmail::create([
            'email'=> $request->email
        ]);
        return redirect()->back()->with('success', "Bienvenue <strong>".$subscription->email."</strong>, Vous vous êtes inscrit avec succès à notre newsletter.");
    }
}
