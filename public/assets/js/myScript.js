"use strict";

$(document).ready(function () {
    // Function to handle the resize behavior
    function handleResize() {
        const $dropdown = $(".dropdown ul"); // Select the dropdown menu

        if ($(window).width() < 991) {
            // If screen size is below 991px
            $dropdown.find("span").each(function () {
                // Move children out of the span and remove the span
                $(this).children().unwrap();
            });
        } else {
            // If screen size is above or equal to 991px
            const $listItems = $dropdown.find("li"); // Get direct <li> children of <ul>
            const itemsPerSpan = 2; // Number of <li> elements per <span>

            // Clear the dropdown and recreate spans
            $dropdown.empty();
            console.log($listItems);
            for (let i = 0; i < $listItems.length; i += itemsPerSpan) {
                const $span = $('<span class="d-flex justify-content-between"></span>');
                $listItems.slice(i, i + itemsPerSpan).appendTo($span); // Append items to the new span
                $dropdown.append($span); // Add the span back to the dropdown
            }
        }
    }

    // Attach resize event and run the function once on load
    $(window).on("resize", handleResize);
    handleResize();
});
