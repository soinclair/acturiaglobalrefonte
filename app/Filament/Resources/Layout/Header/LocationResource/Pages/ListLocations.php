<?php

namespace App\Filament\Resources\Layout\Header\LocationResource\Pages;

use App\Filament\Resources\Layout\Header\LocationResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListLocations extends ListRecords
{
    use ListRecords\Concerns\Translatable;
    
    protected static string $resource = LocationResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
