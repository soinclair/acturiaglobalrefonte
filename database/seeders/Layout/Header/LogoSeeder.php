<?php

namespace Database\Seeders\Layout\Header;

use App\Models\Layout\Header\Logo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LogoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Logo::insert([
            [
                'image' => 'images/logo.png',

                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
