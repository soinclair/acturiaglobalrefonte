<?php

namespace Database\Seeders\About;

use App\Models\About\About;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        About::insert([
            [
                'created_at' => now(), 
                'updated_at' => now()
            ],
        ]);

        $this->call([
            BannerSeeder::class,
            VissionSeeder::class
        ]);
    }
}
