<?php

namespace Database\Seeders\Home;

use App\Models\Home\HomeHero;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class HeroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        HomeHero::insert([
            [   
                'created_at' => now(), 
                'updated_at' => now()
            ],
        ]);
    }
}
