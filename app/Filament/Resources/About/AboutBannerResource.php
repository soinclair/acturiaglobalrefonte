<?php

namespace App\Filament\Resources\About;

use App\Filament\Resources\About\AboutBannerResource\Pages;
use App\Filament\Resources\About\AboutBannerResource\RelationManagers;
use App\Models\About\AboutBanner;
use Filament\Forms;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class AboutBannerResource extends Resource
{

    use Translatable;

    protected static ?string $model = AboutBanner::class;

    protected static ?string $navigationIcon = 'heroicon-o-chart-bar';

    protected static ?string $activeNavigationIcon = 'heroicon-s-chart-bar';

    protected static ?string $modelLabel = 'about_banners';

    protected static ?string $navigationGroup = 'abouts';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Banner Section')->schema([
                    TextInput::make('title')
                        ->required()
                        ->maxLength(255)
                        ->label(static::getDynamicLabel("title"))
                        ->translateLabel(),
                    FileUpload::make('image')
                        ->acceptedFileTypes(['image/png', "image/jpeg"])
                        ->disk('assets')
                        ->directory('images/abouts')
                        ->required()
                        ->label(static::getDynamicLabel("image"))
                        ->translateLabel(),
                ])
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('title')
                    ->sortable()
                    ->searchable()
                    ->toggleable()
                    ->label(static::getDynamicLabel("title"))
                    ->translateLabel(),
                ImageColumn::make('image')
                    ->disk('assets')
                    ->square()
                    ->label(static::getDynamicLabel("image"))
                    ->translateLabel(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAboutBanners::route('/'),
            'create' => Pages\CreateAboutBanner::route('/create'),
            'edit' => Pages\EditAboutBanner::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch plural translation without "s"
    }

    public static function getModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch translation based on current locale
    }

    public static function getNavigationGroup(): ?string
    {
        return __("filament/custom/navigation-model-labels." . static::$navigationGroup); // Fetch translation based on current locale
    }

    protected static function getDynamicLabel(string $field): string
    {
        return __("model-properties." . static::$modelLabel . '-' . $field);
    }
}
