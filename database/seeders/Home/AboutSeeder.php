<?php

namespace Database\Seeders\Home;

use App\Models\Home\HomeAbout;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        HomeAbout::insert([
            [
                'title' => json_encode([
                    'en' => "The Alliance of Passion and Precision",
                    'fr' => "L'alliance de la passion et de la précision",
                    'ar' => "تحالف الشغف والدقة",
                ]),
                'keywords' => json_encode([
                    'en' => "Passion,Precision",
                    'fr' => "passion,précision",
                    'ar' => "الدقة",
                ]),
                'paragraph' => json_encode([
                    'en' => "ACTUAIRA GLOBAL is your trusted partner to navigate a constantly evolving world. We combine our proven expertise in actuarial science and risk management with the latest advancements in data science to provide you with comprehensive and innovative solutions.",
                    'fr' => "ACTUAIRA GLOBAL est votre partenaire de confiance pour naviguer dans un monde en constante évolution. Nous combinons notre expertise éprouvée en actuariat et gestion des risques avec les dernières avancées en data science pour vous offrir des solutions complètes et innovantes.",
                    'ar' => "ACTUAIRA GLOBAL هو شريكك الموثوق للتنقل في عالم يتطور باستمرار. نحن نجمع بين خبرتنا المثبتة في علم الاكتوارية وإدارة المخاطر مع أحدث التطورات في علم البيانات لنقدم لك حلولًا شاملة ومبتكرة.",
                ]),
                'italic_paragraph' => json_encode([
                    'en' => "Whether you're looking to maximize profits, manage risks effectively, plan strategically for the future, or optimize business processes through artificial intelligence, we are here to provide you with reliable advice and proven strategies.",
                    'fr' => "Que vous cherchiez à maximiser vos profits, à gérer efficacement vos risques, à planifier stratégiquement pour l'avenir ou à optimiser vos processus métier grâce à l'intelligence artificielle, nous sommes là pour vous fournir des conseils fiables et des stratégies éprouvées.",
                    'ar' => "سواء كنت تبحث عن تعظيم الأرباح، أو إدارة المخاطر بفعالية، أو التخطيط الاستراتيجي للمستقبل، أو تحسين العمليات التجارية من خلال الذكاء الاصطناعي، نحن هنا لتزويدك بنصائح موثوقة واستراتيجيات مثبتة.",
                ]),
                'image' => "images/about/about.jpg",
                'cta_label' => json_encode([
                    'en' => "Learn More",
                    'fr' => "Savoir plus",
                    'ar' => "تعرف على المزيد",
                ]),
                'cta_path' => "about",
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
