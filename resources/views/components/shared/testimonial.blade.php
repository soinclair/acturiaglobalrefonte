<section class="testimonial-section pt-135 rpt-85 pb-150 rpb-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-md-8 col-sm-9">
                <div class="section-title text-center mb-80 wow fadeInUp" data-wow-duration="2s">
                    <h2> {{ $section->headers[0] }} <span>{{ $section->headers[1] }}</span></h2>
                    <p> {{ $section->paragraphs[0]}} </p>
                </div>
            </div>
        </div>
        <div class="testimonial-area">
            <div class="review-buttons">
                @foreach ($reviewButtons as $reviewBtn)
                    <figure>
                        <a href="{{ $reviewBtn->uri }}" data-review="{{ $reviewBtn->id }}" class="review-btn {{ $loop->index === 1 ? "active" : "" }}" >
                            <img src="{{ asset($reviewBtn->image->path) }}" alt="{{ $reviewBtn->image->label }}">
                        </a>
                    </figure>
                @endforeach
            </div>
            <div class="testimony-content">
                @foreach ($reviews as $review)
                    <div class="review-single {{ $loop->index === 1 ? "active" : "" }}">
                        <div class="textimonial-image">
                            <img src="{{ asset($review->image->path) }}" alt="{{ $review->image->label }}">
                        </div>
                        <div class="textimonial-content">
                            <p> {{ $review->paragraph }} </p>
                            <div class="reviewer">
                                <h3> {{ $review->name }} </h3>
                                <span> {{ $review->position }} </span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>