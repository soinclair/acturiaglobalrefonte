<?php

namespace App\Filament\Resources\Layout\Footer\FooterMenuItemResource\Pages;

use App\Filament\Resources\Layout\Footer\FooterMenuItemResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateFooterMenuItem extends CreateRecord
{
    use CreateRecord\Concerns\Translatable;

    protected static string $resource = FooterMenuItemResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            // ...
        ];
    }
}
