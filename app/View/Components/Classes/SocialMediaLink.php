<?php

namespace App\View\Components\Classes;

class SocialMediaLink{

    public function __construct(
        
        public string $label,
        public string $url
        
    ){

    }
}