<?php

namespace App\Filament\Resources\Home\HomeClientResource\Pages;

use App\Filament\Resources\Home\HomeClientResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditHomeClient extends EditRecord
{

    use EditRecord\Concerns\Translatable;

    protected static string $resource = HomeClientResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
