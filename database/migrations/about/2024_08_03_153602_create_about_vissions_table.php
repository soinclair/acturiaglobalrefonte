<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('about_vissions', function (Blueprint $table) {
            $table->id();
            $table->json('tab_title');
            $table->string('tab_image');
            $table->json('title');
            $table->json('keywords');
            $table->json('paragraph');
            $table->foreignId('about_id')->default(1)->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('about_vissions');
    }
};
