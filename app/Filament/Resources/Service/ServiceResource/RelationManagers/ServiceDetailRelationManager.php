<?php

namespace App\Filament\Resources\Service\ServiceResource\RelationManagers;

use Filament\Forms;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\Concerns\Translatable;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Livewire\Attributes\Reactive;

class ServiceDetailRelationManager extends RelationManager
{

    use Translatable;

    protected static string $relationship = 'serviceDetail';

    protected static ?string $modelLabel = "service_details";

    // This will make the table inherit the services resource locale
    // #[Reactive]
    // public ?string $activeLocale = null;

    // TODO: Add tabs
    public function form(Form $form): Form
    {
        return $form
            ->schema([
                // Title Paragraph - Full Row
                Textarea::make('title_paragraph')
                    ->label('Title Paragraph')
                    ->required()
                    ->columnSpan('full')
                    ->label(static::getDynamicLabel("title_paragraph"))
                    ->translateLabel(),

                // Image - Below Title Paragraph
                TextInput::make('image')
                    ->label('Image URL')
                    ->required()
                    ->maxLength(255)
                    ->columnSpan('full')
                    ->label(static::getDynamicLabel("image"))
                    ->translateLabel(),

                // Section for Headers and Paragraphs
                Section::make('Content Section')->schema([
                    Grid::make()->schema([
                        // Column 1
                        Group::make()->schema([
                            TextInput::make('header1')
                                ->label('First Header')
                                ->maxLength(255)
                                ->label(static::getDynamicLabel("header1"))
                                ->translateLabel(),
                            Textarea::make('paragraph1_1')
                                ->label('Premiere paragraph')
                                ->label(static::getDynamicLabel("paragraph1_1"))
                                ->translateLabel(),
                            Textarea::make('paragraph2_1')
                                ->label('Second paragraph')
                                ->label(static::getDynamicLabel("paragraph2_1"))
                                ->translateLabel(),
                        ])->columnSpan(2),

                        // Column 2
                        Group::make()->schema([
                            TextInput::make('header2')
                                ->label('Second Header')
                                ->maxLength(255)
                                ->label(static::getDynamicLabel("header2"))
                                ->translateLabel(),
                            Textarea::make('paragraph1_2')
                                ->label('Premiere paragraph')
                                ->label(static::getDynamicLabel("paragraph1_2"))
                                ->translateLabel(),
                            Textarea::make('paragraph2_2')
                                ->label('Second paragraph')
                                ->label(static::getDynamicLabel("paragraph2_2"))
                                ->translateLabel(),
                        ])->columnSpan(2),

                        // Column 3
                        Group::make()->schema([
                            TextInput::make('header3')
                                ->label('Third Header')
                                ->maxLength(255)
                                ->label(static::getDynamicLabel("header3"))
                                ->translateLabel(),
                            Textarea::make('paragraph1_3')
                                ->label('Paragraph')
                                ->label(static::getDynamicLabel("paragraph1_3"))
                                ->translateLabel(),
                        ])->columnSpan(2),
                    ]),

                ])->columnSpan('full'),

            ]);
    }

    public function table(Table $table): Table
    {
        return $table
        ->modelLabel("sd")
            ->recordTitleAttribute('id')
            ->columns([
                // TextColumn::make('id')
                //     ->label(static::getDynamicLabel("id"))
                //     ->translateLabel(),
                TextColumn::make('title_paragraph')
                    ->label("Title's paragraph")
                    ->label(static::getDynamicLabel("title_paragraph"))
                    ->translateLabel(),

                // TODO: Replace Images with ImageUpload
                TextColumn::make('image')
                    ->label(static::getDynamicLabel("image"))
                    ->translateLabel(),

                TextColumn::make('header1')
                    ->label("First Header")
                    ->label(static::getDynamicLabel("header1"))
                    ->translateLabel(),

                TextColumn::make('header2')
                    ->label("Second Header")
                    ->label(static::getDynamicLabel("header2"))
                    ->translateLabel(),

                TextColumn::make('header3')
                    ->label("Third Header")
                    ->label(static::getDynamicLabel("header3"))
                    ->translateLabel(),

                // TextColumn::make('created_at')->dateTime()
                //     ->label(static::getDynamicLabel("created_at"))
                //     ->translateLabel(),

                // TextColumn::make('updated_at')->dateTime()
                //     ->label(static::getDynamicLabel("updated_at"))
                //     ->translateLabel(),
            ])
            ->filters([
                //
            ])
            ->headerActions([
                Tables\Actions\LocaleSwitcher::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    protected static function getDynamicLabel(string $field): string
    {
        return __("model-properties." . static::$modelLabel . '-' . $field);
    }
}
