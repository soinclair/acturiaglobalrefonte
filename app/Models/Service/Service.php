<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Service extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        'label',
        'uri',
        'icon'
    ];

    public $translatable = [
        'label',
        'uri',
    ];

    public function serviceDetail(){
        return $this->hasOne(ServiceDetail::class);
    }

    protected $casts = [
        'uri' => 'array',
        "label" => "array", // Automatically handle JSON as an array
    ];
}
