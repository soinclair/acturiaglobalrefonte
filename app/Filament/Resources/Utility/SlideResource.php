<?php

namespace App\Filament\Resources\Utility;

use App\Filament\Resources\Utility\SlideResource\Pages;
use App\Filament\Resources\Utility\SlideResource\RelationManagers;
use App\Models\Utility\Slide;
use Filament\Forms;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;



// TODO: Re-order columns for better readability
class SlideResource extends Resource
{

    use Translatable;

    protected static ?string $model = Slide::class;

    protected static ?string $navigationIcon = 'heroicon-o-photo';

    protected static ?string $activeNavigationIcon = 'heroicon-s-photo';

    protected static ?string $modelLabel = 'slides';

    protected static ?string $navigationGroup = 'homes';


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('cta_label')
                    ->required()
                    ->maxLength(255)
                    ->label(static::getDynamicLabel("cta_label"))
                    ->translateLabel(),

                Select::make('cta_path')
                    ->options([
                        'abouts' => 'A propos',
                        'homes' => 'Accueil',
                        'contacts' => 'Contact'
                    ])
                    ->required()
                    ->label(static::getDynamicLabel("cta_path"))
                    ->translateLabel(),

                TextInput::make('title')
                    ->required()
                    ->maxLength(255)
                    ->label(static::getDynamicLabel("title"))
                    ->translateLabel(),

                FileUpload::make('image')
                    ->acceptedFileTypes(['image/jpeg', 'image/png'])
                    ->disk('assets')
                    ->directory('images/hero')
                    ->required()
                    ->preserveFilenames()
                    ->label(static::getDynamicLabel("image"))
                    ->translateLabel(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('cta_label')
                    ->sortable()
                    ->searchable()
                    ->label(static::getDynamicLabel("cta_label"))
                    ->translateLabel(),

                TextColumn::make('cta_path')
                    ->sortable()
                    ->searchable()
                    ->label(static::getDynamicLabel("cta_path"))
                    ->translateLabel(),

                TextColumn::make('title')
                    ->sortable()
                    ->searchable()
                    ->label(static::getDynamicLabel("title"))
                    ->translateLabel(),

                ImageColumn::make('image')
                    ->disk('assets')
                    ->square()
                    ->label(static::getDynamicLabel("image"))
                    ->translateLabel(),

                // TextColumn::make('created_at')
                //     ->dateTime()
                //     ->label(static::getDynamicLabel("created_at"))
                //     ->translateLabel(),

                // TextColumn::make('updated_at')
                //     ->dateTime()
                //     ->label(static::getDynamicLabel("updated_at"))
                //     ->translateLabel(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSlides::route('/'),
            'create' => Pages\CreateSlide::route('/create'),
            'edit' => Pages\EditSlide::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch plural translation without "s"
    }

    public static function getModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch translation based on current locale
    }

    public static function getNavigationGroup(): ?string
    {
        return __("filament/custom/navigation-model-labels." . static::$navigationGroup); // Fetch translation based on current locale
    }

    protected static function getDynamicLabel(string $field): string
    {
        return __("model-properties." . static::$modelLabel . '-' . $field);
    }
}
