<?php

namespace App\View\Components\Pages\Home;

use App\Models\Home\HomeClient;
use App\View\Components\Classes\ClientItem;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Clients extends Component
{
    public string $title;
    public array $clientItems; 

    public function __construct(
        HomeClient $homeClient
    )
    {
        $this->title = $homeClient->title;
        $this->clientItems = $homeClient->clients->map(function($client){
            return new ClientItem($client->name, $client->logo);
        })->toArray();
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.pages.home.clients');
    }
}
