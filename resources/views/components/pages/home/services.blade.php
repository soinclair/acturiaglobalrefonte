<section class="services-section bg-snow pt-140 rpt-90 pb-110 rpb-60">
    <div class="container">
        {{-- --------------- Section Title --------------- --}}
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-8">
                <div class="section-title text-center mb-80 wow fadeInUp" data-wow-duration="2s">
                    <h2> {{ $title }} </h2>
                    <p> {{ $paragraph }} </p>
                </div>
            </div>
        </div>
        {{-- --------------- Section Services --------------- --}}
        <div class="row">

            @foreach ($topServices as $service)
                <div class="col-lg-3 col-md-6">
                    <div class="service-item wow fadeInUp" data-wow-duration="2s">
                        <div class="service-icon d-flex justify-content-between">
                            <img src="{{ asset($service->icon) }}" loading="lazy" alt="Services">
                            <a href="{{ route('services.show', ['uri' => $service->uri]) }}" class="">
                                <i class="fas fa-angle-double-right"></i>
                            </a>
                        </div>
                        <div class="service-content">
                            <h4><a href="{{ route('services.show', ['uri' => $service->uri]) }}">
                                    {{ $service->label }} </a></h4>
                            <p>{{ $service->paragraph }} </p>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="separetor wow fadeInUp" data-wow-duration="2s"></div>

            @foreach ($bottomServices as $service)
                <div class="col-lg-3 col-md-6">
                    <div class="service-item wow fadeInUp" data-wow-duration="2s">
                        <div class="service-icon d-flex justify-content-between">
                            <img src="{{ asset($service->icon) }}" loading="lazy" alt="Services">
                            <a href="{{ route('services.show', ['uri' => $service->uri]) }}" class="">
                                <i class="fas fa-angle-double-right"></i>
                            </a>
                        </div>
                        <div class="service-content">
                            <h4><a href="{{ route('services.show', ['uri' => $service->uri]) }}">
                                    {{ $service->label }}</a></h4>
                            <p>{{ $service->paragraph }} </p>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</section>
