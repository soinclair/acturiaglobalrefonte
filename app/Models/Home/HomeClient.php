<?php

namespace App\Models\Home;

use App\Models\Client\Client;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class HomeClient extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        'title',
        'home_id',
    ];

    public $translatable = [
        "title",
    ];

    public function getClientsAttribute()
    {
        return Client::take(8)->get();
    }
}
