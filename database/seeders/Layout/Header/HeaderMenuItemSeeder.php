<?php

namespace Database\Seeders\Layout\Header;

use App\Models\Layout\Header\HeaderMenuItem;
use App\Models\Service\Service;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class HeaderMenuItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Don't Forget to change the parent_id of Services, if the order of menu items were to be changed
        // If the Service header menu item were to be the 4th to be created in the array
        // then the parent_id should be 4

        $headerMenuItems = [
            [
                'label' => json_encode([
                    'en' => 'Home',
                    'fr' => 'Accueil',
                    'ar' => 'الرئيسية',
                ]),
                'uri' => 'home',
                'parent_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'label' => json_encode([
                    'en' => 'Who We Are?',
                    'fr' => 'Qui Sommes Nous?',
                    'ar' => 'من نحن؟',
                ]),
                'uri' => 'about',
                'parent_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            // [
            //     'label' => json_encode([
            //         'en' => 'Your Sector',
            //         'fr' => 'Votre secteur',
            //         'ar' => 'قطاعك',
            //     ]),
            //     'uri' => 'sector',
            //     'parent_id' => null,
            //     'created_at' => now(),
            //     'updated_at' => now(),
            // ],
            [
                'label' => json_encode([
                    'en' => 'Services',
                    'fr' => 'Services',
                    'ar' => 'الخدمات',
                ]),
                'uri' => 'services',
                'parent_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            // [
            //     'label' => json_encode([
            //         'en' => 'Our Clients',
            //         'fr' => 'Nos clients',
            //         'ar' => 'عملاؤنا',
            //     ]),
            //     'uri' => 'clients',
            //     'parent_id' => null,
            //     'created_at' => now(),
            //     'updated_at' => now(),
            // ],
            // [
            //     'label' => json_encode([
            //         'en' => 'News',
            //         'fr' => 'Actualites',
            //         'ar' => 'الأخبار',
            //     ]),
            //     'uri' => 'news',        
            //     'parent_id' => null,
            //     'created_at' => now(),
            //     'updated_at' => now(),
            // ],
            // Uncomment and translate if needed
            // [
            //     'label' => json_encode([
            //         'en' => "Consultancy Cases",
            //         "fr" => "Cas de conseil",
            //         "ar" => "حالات الاستشارة",
            //     ]),
            //     "uri" => "consultancy-cases",
            //     "parent_id" => null,
            //     "created_at" => now(),
            //     "updated_at" => now(),
            // ],
            [
                'label' => json_encode([
                    'en' => "Contact",
                    "fr" => "Contact",
                    "ar" => "اتصل بنا",
                ]),
                'uri' => "contact",
                "parent_id" => null,
                "created_at" => now(),
                "updated_at" => now(),
            ],
        ];

        $services = Service::all();

        foreach ($services as $service) {
            $headerMenuItems[] = [
                'label' => json_encode($service->getTranslations("label")),
                'uri' => $service->uri,
                'parent_id' => 3,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        HeaderMenuItem::insert($headerMenuItems);
    }
}
