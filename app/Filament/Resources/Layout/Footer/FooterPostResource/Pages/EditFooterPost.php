<?php

namespace App\Filament\Resources\Layout\Footer\FooterPostResource\Pages;

use App\Filament\Resources\Layout\Footer\FooterPostResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditFooterPost extends EditRecord
{
    use EditRecord\Concerns\Translatable;

    protected static string $resource = FooterPostResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
