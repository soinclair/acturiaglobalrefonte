<?php

namespace App\View\Components\Classes;

class Section{

    public function __construct(
        
        public array $headers,
        public array $subHeaders,
        public array $paragraphs
        
    ){

    }
}