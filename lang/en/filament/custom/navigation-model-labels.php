<?php

return [
    /* <---------- Public ----------> */

    /* <----- Home -----> */
    "homes" => "Home Page Sections",
    "home_abouts" => "About Us",
    "home_clients" => "Our Clients",
    "home_heroes" => "Main Section",
    "slides" => "Hero Image Slides",
    "home_services" => "Our Services",
    "home_successes" => "Success Stories",

    /* <----- About -----> */
    "abouts" => "About Page Sections",
    "about_banners" => "Top Banner",
    "about_vissions" => "Our Vision",

    /* <---------- Layout ----------> */

    /* <----- Header -----> */
    "header" => "Layout Header",
    "header_menu_items" => "Navigation Links",
    "locations" => "Our Locations",
    "logos" => "Brand Logo",
    "supports" => "Contact Information",

    /* <----- Footer -----> */
    "footer" => "Layout Footer",
    "footer_menu_items" => "Navigation Links",
    "copyrights" => "Copyright",
    "footer_contacts" => "Contact Information",
    "footer_posts" => "Posts Section",
    "footer_services" => "Services Section",
    "subscribe_forms" => "Newsletter Subscription Form",

    /* <---------- Resources ----------> */
    "resources" => "Resources",
    "services" => "Services",
    "service_details" => "Service Details",
    "subscribe_emails" => "Subscribed Emails",
    "success_items" => "Success Elements",
];
