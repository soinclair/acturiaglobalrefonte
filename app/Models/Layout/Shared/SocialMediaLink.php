<?php

namespace App\Models\Layout\Shared;

use App\Models\Layout\Layout;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialMediaLink extends Model
{
    use HasFactory;

    public function layout(){
        return $this->belongsTo(Layout::class);
    }
}
