@extends('layouts.app')
@section('contact.index')
    <x-shared.breadcrumb-banner
        :title="'Contactez Nous.'"
        :image="asset('assets/images/banner/contact-bg.jpg')"
        {{--
        :breadcrumbs=""
        --}}
    />

    <section class="get-in-touch py-150 rpy-100">
        <div class="container">
            <div class="row">
                {{--@include('contact.partials.locations')--}}
                @include('contact.partials.form')
            </div>
        </div>
    </section>

    @include('contact.partials.map')
    <x-shared.call-to-action
    {{--
        :section=""
        :cta=""
        :theme=""
    --}}
    />
@endsection




