<?php

namespace Database\Seeders\Layout\Footer;

use App\Models\Layout\Footer\Copyright;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CopyrightSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Copyright::insert([
            [
                'paragraph' => json_encode([
                    'en' => '© 2024 ACTUARIA GLOBAL - All rights reserved',
                    'fr' => '© 2024 ACTUARIA GLOBAL - Tous droits réservés',
                    'ar' => '© 2024 ACTUARIA GLOBAL - جميع الحقوق محفوظة',
                ]),
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
