<?php

namespace App\Filament\Resources\Layout\Header\LogoResource\Pages;

use App\Filament\Resources\Layout\Header\LogoResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateLogo extends CreateRecord
{
    protected static string $resource = LogoResource::class;
}
