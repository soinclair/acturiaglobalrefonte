<div class="col-lg-3 col-6">
    <div class="widget menu-widget">
        <h3 class="widget-title"> {{ $footerService->title }} </h3>
        <ul>
            @foreach ($services as $service)
                <li><a href="{{ $service->uri }}"> {{ $service->label }} </a></li>
            @endforeach
        </ul>
    </div>
</div>
