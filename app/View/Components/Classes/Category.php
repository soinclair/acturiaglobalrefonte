<?php

namespace App\View\Components\Classes;

class Category{

    public function __construct(
        
        public int $id,
        public string $label
        
    ){

    }
}