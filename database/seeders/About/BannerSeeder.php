<?php

namespace Database\Seeders\About;

use App\Models\About\AboutBanner;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        AboutBanner::insert([
            [
                'title' => json_encode([
                    'en' => 'About',
                    'fr' => 'À propos',
                    'ar' => 'حول',
                ]),
                'image' => 'about-banner.jpg',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
