<?php
namespace App\Models\Layout\Header;

use App\Models\Layout\Layout;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class HeaderMenuItem extends Model
{
    use HasFactory, HasTranslations;
    
    protected $fillable = [
        'label',
        'uri',
        'parent_id',
    ];

    public $translatable = [
        "label",
    ];

    public function layout(){
        return $this->belongsTo(Layout::class);
    }

    public function parent()
    {
        return $this->belongsTo(HeaderMenuItem::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(HeaderMenuItem::class, 'parent_id');
    }
}
