<div class="our-success pb-30 rpb-0 wow fadeInUp" data-wow-duration="2s">
    <div class="container">
        <div class="success-wrap bg-orange">
            <div class="row">
                @foreach ($successItems as $success )
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <div class="success-item">
                            <div class="success-icon">
                                <i class="fa-{{ $success->iconStyle }} fa-{{ $success->icon }}"></i>
                            </div>
                            <div class="success-content">
                                <span class="count-text @if($success->isPercentage) percentage @endif" data-speed="2500" data-stop="{{ $success->number }}">0</span>
                                <p> {{ $success->label }} </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>