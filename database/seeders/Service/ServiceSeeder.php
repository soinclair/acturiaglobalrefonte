<?php

namespace Database\Seeders\Service;

use App\Models\Sector;
use App\Models\Service\Service;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        Service::insert([

            /* <----------- Actuariat conseil ------------> */
            [
                'label' => json_encode([
                    "fr" => 'Actuariat conseil',
                    "en" => 'Actuarial Consulting',
                    "ar" => 'استشارات الاكتوارية'
                ]),
                'uri' => json_encode([
                    "fr" => Str::slug('Actuariat conseil'),
                    "en" => Str::slug('Actuarial Consulting'),
                    "ar" => Str::slug('استشارات الاكتوارية')
                ]),
                'icon' => 'images/services/icons/' . Str::slug('Actuariat conseil') . '.png',
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- Protection sociale ------------> */
            [
                'label' => json_encode([
                    "fr" => 'Protection sociale',
                    "en" => 'Social Protection',
                    "ar" => 'الحماية الاجتماعية'
                ]),
                'uri' => json_encode([
                    "fr" => Str::slug('Protection sociale'),
                    "en" => Str::slug('Social Protection'),
                    "ar" => Str::slug('الحماية الاجتماعية')
                ]),
                'icon' => 'images/services/icons/' . Str::slug('Protection sociale') . '.png',
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- Remuneration & avantages sociaux ------------> */
            [
                'label' => json_encode([
                    "fr" => 'Remuneration & avantages sociaux',
                    "en" => 'Compensation & Benefits',
                    "ar" => 'التعويضات والمزايا'
                ]),
                'uri' => json_encode([
                    "fr" => Str::slug('Remuneration & avantages sociaux'),
                    "en" => Str::slug('Compensation & Benefits'),
                    "ar" => Str::slug('التعويضات والمزايا')
                ]),
                'icon' => 'images/services/icons/' . Str::slug('Remuneration & avantages sociaux') . '.png',
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- Risk management ------------> */
            [
                'label' => json_encode([
                    "fr" => 'Risk management',
                    "en" => 'Risk Management',
                    "ar" => 'إدارة المخاطر'
                ]),
                'uri' => json_encode([
                    "fr" => Str::slug('Risk management'),
                    "en" => Str::slug('Risk Management'),
                    "ar" => Str::slug('إدارة المخاطر')
                ]),
                'icon' => 'images/services/icons/' . Str::slug('Risk management') . '.png',
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- Conseil en assurance maladie & mutualite ------------> */
            [
                'label' => json_encode([
                    "fr" => 'Conseil en assurance maladie & mutualite',
                    "en" => 'Health Insurance & Mutual Advice',
                    "ar" => 'استشارة التأمين الصحي والتعاونيات'
                ]),
                'uri' => json_encode([
                    "fr" => Str::slug('Conseil en assurance maladie & mutualite'),
                    "en" => Str::slug('Health Insurance & Mutual Advice'),
                    "ar" => Str::slug('استشارة التأمين الصحي والتعاونيات')
                ]),
                'icon' => 'images/services/icons/' . Str::slug('Conseil en assurance maladie & mutualite') . '.png',
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- AI & Data science ------------> */
            [
                'label' => json_encode([
                    "fr" => 'AI & Data science',
                    "en" => 'AI & Data Science',
                    "ar" => 'الذكاء الاصطناعي وعلوم البيانات'
                ]),
                'uri' => json_encode([
                    "fr" => Str::slug('AI & Data science'),
                    "en" => Str::slug('AI & Data Science'),
                    "ar" => Str::slug('الذكاء الاصطناعي وعلوم البيانات')
                ]),
                'icon' => 'images/services/icons/' . Str::slug('AI & Data science') . '.png',
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- Formation ------------> */
            [
                'label' => json_encode([
                    "fr" => 'Formation',
                    "en" => 'Training',
                    "ar" => 'تدريب'
                ]),
                'uri' => json_encode([
                    "fr" => Str::slug('Formation'),
                    "en" => Str::slug('Training'),
                    "ar" => Str::slug('تدريب')
                ]),
                'icon' => 'images/services/icons/' . Str::slug('Formation') . '.png',
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- Enquete de satisfaction client ------------> */
            [
                'label' => json_encode([
                    "fr" => 'Enquete de satisfaction client',
                    "en" => 'Customer Satisfaction Survey',
                    "ar" => 'استطلاع رضا العملاء'
                ]),
                'uri' => json_encode([
                    "fr" => Str::slug('Enquete de satisfaction client'),
                    "en" => Str::slug('Customer Satisfaction Survey'),
                    "ar" => Str::slug('استطلاع رضا العملاء')
                ]),
                'icon' => 'images/services/icons/' . Str::slug('Enquete de satisfaction client') . '.png',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);

        $this->call([
            ServiceDetailSeeder::class
        ]);
    }
}
