<?php

namespace Database\Seeders\Layout;

use App\Models\Layout\Layout;

use Database\Seeders\Layout\Header\LocationSeeder;
use Database\Seeders\Layout\Header\LogoSeeder;
use Database\Seeders\Layout\Header\SupportSeeder;
use Database\Seeders\Layout\Header\HeaderMenuItemSeeder;

use Database\Seeders\Layout\Footer\CopyrightSeeder;
use Database\Seeders\Layout\Footer\FooterContactSeeder;
use Database\Seeders\Layout\Footer\FooterMenuItemSeeder;
use Database\Seeders\Layout\Footer\FooterPostSeeder;
use Database\Seeders\Layout\Footer\FooterServiceSeeder;
use Database\Seeders\Layout\Footer\SubscribeFormSeeder;

use Database\Seeders\Layout\Shared\SocialMediaLinkSeeder;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LayoutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Layout::insert([
            ['created_at' => now(), 'updated_at' => now()],
        ]);
        $this->call([
            HeaderMenuItemSeeder::class,
            LocationSeeder::class,
            LogoSeeder::class,
            SupportSeeder::class,
            SocialMediaLinkSeeder::class,
            CopyrightSeeder::class,
            FooterContactSeeder::class,
            FooterMenuItemSeeder::class,
            FooterServiceSeeder::class,
            FooterPostSeeder::class,
            SubscribeFormSeeder::class
        ]);
    }
}
