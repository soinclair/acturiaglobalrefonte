<?php

namespace Database\Seeders\Layout\Header;

use App\Models\Layout\Header\Support;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SupportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Support::insert([
            [
                'email' => 'contact@actuariaglobal.ma',
                'phone' => '+212 (0) 5 22 86 28 86',
                 
                'created_at' => now(),
                'updated_at' => now(),
            ],      
        ]);
    }
}
