<?php

namespace Database\Seeders\Utility;


use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UtilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->call([
            SlideSeeder::class,
            SuccessItemSeeder::class
        ]);
    }
}
