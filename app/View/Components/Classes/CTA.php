<?php

namespace App\View\Components\Classes;

class CTA{

    public function __construct(
        
        public string $label,
        public string $path
    
    ){

    }

}