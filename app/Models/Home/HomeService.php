<?php

namespace App\Models\Home;

use App\Models\Service\Service;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class HomeService extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        "title",
        "paragraph",
    ];

    public $translatable = [
        "title",
        "paragraph",
    ];

    public function getServicesAttribute()
    {
        return Service::orderBy('created_at', 'desc')->take(8)->get();
    }
}
