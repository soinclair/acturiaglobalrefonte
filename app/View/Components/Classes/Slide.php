<?php

namespace App\View\Components\Classes;

class Slide{

    public function __construct(
        
        public string $title,
        public CTA $cta,
        public string $image 
        
    ){
        
    }
}