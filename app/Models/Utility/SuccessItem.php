<?php

namespace App\Models\Utility;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class SuccessItem extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        'number',
        'icon',
        'icon_style',
        'label',
        'is_percentage',
    ];

    public $translatable = [
        'label',
    ];

    public function successItemable()
    {
        return $this->morphTo();
    }
    
}
