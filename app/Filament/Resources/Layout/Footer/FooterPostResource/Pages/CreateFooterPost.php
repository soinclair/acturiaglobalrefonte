<?php

namespace App\Filament\Resources\Layout\Footer\FooterPostResource\Pages;

use App\Filament\Resources\Layout\Footer\FooterPostResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateFooterPost extends CreateRecord
{
    use CreateRecord\Concerns\Translatable;

    protected static string $resource = FooterPostResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            // ...
        ];
    }
}
