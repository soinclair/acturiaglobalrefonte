<div class="nav-outer clearfix ml-lg-auto mr-lg-auto">
    {{-- ------------ Main Menu ------------ --}}
    <nav class="navbar navbar-expand-lg navbar-light">
        <button
            class="navbar-toggler"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            type="button"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
        >
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                @foreach ($menuItems as $menuItem)
                    @if ($menuItem->children->isNotEmpty())
                        <li class="nav-item dropdown">
                            <a
                                class=" dropdown-toggle"
                                @class([
                            'nav-item',
                            'current-menu-item' => Str::contains(
                                url()->current(),
                                strtolower($menuItem->label)),
                        ])
                                id="navbarDropdown"
                                data-toggle="dropdown"
                                href="{{ route($menuItem->uri . '.index') }}"
                                role="button"
                                aria-haspopup="true"
                                aria-expanded="false"
                            >
                            {{ $menuItem->label }}
                            </a>
                            <div
                                class="dropdown-menu"
                                aria-labelledby="navbarDropdown"
                                style="width:300px"
                            >
                                <div class="container-sm container">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            @foreach ($menuItem->children->take(4) as $subPage)
                                                    <a class="dropdown-item"
                                                        href="{{ route($menuItem->uri . '.show', ['uri' => $subPage->uri]) }}">{{ $subPage->label }}</a>
                                            @endforeach
                                        </div>
                                        <div class="col-sm-6">
                                            @foreach ($menuItem->children->slice(4) as $subPage)
                                                    <a class="dropdown-item"
                                                        href="{{ route($menuItem->uri . '.show', ['uri' => $subPage->uri]) }}">{{ $subPage->label }}</a>
                                            @endforeach
                                            <a class="dropdown-item" href="#">Link 4</a>
                                            <a class="dropdown-item" href="#">Link 5</a>
                                            <a class="dropdown-item" href="#">Link6</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @elseif ($menuItem->parent === null)
                        <li @class([
                            'nav-item',
                            'current-menu-item' => Str::contains(
                                url()->current(),
                                strtolower($menuItem->label)),
                        ])>
                            <a href="{{ route($menuItem->uri . '.index') }}" @class(['nav-link'])>
                                {{ $menuItem->label }} </a>
                        </li>
                    @endif
                @endforeach
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a
                        class="nav-link dropdown-toggle"
                        id="navbarDropdown"
                        data-toggle="dropdown"
                        href="#"
                        role="button"
                        aria-haspopup="true"
                        aria-expanded="false"
                    >
                        Dropdown
                    </a>
                    <div
                        class="dropdown-menu"
                        aria-labelledby="navbarDropdown"
                        style="width:300px"
                    >
                        <div class="container-sm container">
                            <div class="row">
                                <div class="col-sm-4">
                                    <a class="dropdown-item" href="#">Link 1</a>
                                    <a class="dropdown-item" href="#">Link 2</a>
                                    <a class="dropdown-item" href="#">Link 3</a>
                                </div>
                                <div class="col-sm-4">
                                    <a class="dropdown-item" href="#">Link 4</a>
                                    <a class="dropdown-item" href="#">Link 5</a>
                                    <a class="dropdown-item" href="#">Link6</a>
                                </div>
                                <div class="col-sm-4">
                                    <a class="dropdown-item" href="#">Link 7</a>
                                    <a class="dropdown-item" href="#">Link 8</a>
                                    <a class="dropdown-item" href="#">Link 9</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>
            </ul>
        </div>
    </nav>
    {{-- ------------ Main Menu End ------------ --}}
</div>
