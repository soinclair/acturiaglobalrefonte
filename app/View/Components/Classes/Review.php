<?php

namespace App\View\Components\Classes;

class Review{

    public function __construct(
        
        public int $id,
        public string $name,
        public string $position,
        public string $paragraph,
        public Image $image
        
    ){

    }
}