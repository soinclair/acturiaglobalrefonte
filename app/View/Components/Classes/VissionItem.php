<?php

namespace App\View\Components\Classes;

class VissionItem{

    public function __construct(
        
        public string $tabTitle,
        public string $tabImage,
        public string $title,
        public string $paragraph,
        public SuccessItem $success
    ){
        
    }
}