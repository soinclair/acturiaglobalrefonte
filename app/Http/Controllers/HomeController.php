<?php

namespace App\Http\Controllers;

use App\Models\Home\Home;
use App\Models\Service\Service;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        
        $home = Home::first();
        
        return view('home.home', compact('home'));
    }
}
