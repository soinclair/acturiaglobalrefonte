<?php

namespace App\View\Components\Shared;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

// TODO: Consider making these shared components models MorphToMany
class BreadcrumbBanner extends Component
{
    /**
     * Create a new component instance.
     */
    public string $title;
    public string $image;
    public array $breadcrumbs;

    public function __construct(
        $title = "404 Non Trouvé", 
        $image = "fallback-breadcrumb.jpg"
    )
    {
        $this->title=$title;
        $this->image=$image;
        $currentPath = request()->path(); 
        $segments = explode('/', $currentPath);
        $this->breadcrumbs = array_map(fn($segment) => str_replace('-', ' ', $segment), $segments);    
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.shared.breadcrumb-banner');
    }
}
