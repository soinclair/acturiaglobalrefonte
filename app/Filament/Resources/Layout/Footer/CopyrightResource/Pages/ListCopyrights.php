<?php

namespace App\Filament\Resources\Layout\Footer\CopyrightResource\Pages;

use App\Filament\Resources\Layout\Footer\CopyrightResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListCopyrights extends ListRecords
{
    use ListRecords\Concerns\Translatable;

    protected static string $resource = CopyrightResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
