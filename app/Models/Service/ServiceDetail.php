<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class ServiceDetail extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        'title_paragraph',
        'image',
        'header1',
        'paragraph1_1',
        'paragraph2_1',
        'header2',
        'paragraph1_2',
        'paragraph2_2',
        'header3',
        'paragraph1_3',
        'service_id',
    ];

    public $translatable = [
        'title_paragraph',
        'header1',
        'paragraph1_1',
        'paragraph2_1',
        'header2',
        'paragraph1_2',
        'paragraph2_2',
        'header3',
        'paragraph1_3',
    ];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}
