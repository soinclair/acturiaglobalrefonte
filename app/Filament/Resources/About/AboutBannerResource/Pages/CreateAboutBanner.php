<?php

namespace App\Filament\Resources\About\AboutBannerResource\Pages;

use App\Filament\Resources\About\AboutBannerResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateAboutBanner extends CreateRecord
{
    use CreateRecord\Concerns\Translatable;

    protected static string $resource = AboutBannerResource::class;
    
    protected function getHeaderActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            // ...
        ];
    }

}
