<?php

namespace Database\Seeders\Client;

use App\Models\Client\Client;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Client::insert([
            [
                'name' => 'CNSS',
                'logo' => 'assets/images/clients/cnss.png',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Dell',
                'logo' => 'assets/images/clients/dell.png',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Lafarge',
                'logo' => 'assets/images/clients/lafarge.png',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Marsa Maroc',
                'logo' => 'assets/images/clients/marsa-maroc.png',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'OCP',
                'logo' => 'assets/images/clients/ocp.png',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'ONCF',
                'logo' => 'assets/images/clients/oncf.png',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Roca',
                'logo' => 'assets/images/clients/roca.png',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Royal Air Maroc',
                'logo' => 'assets/images/clients/royal-air-maroc.png',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);    
    }
}
