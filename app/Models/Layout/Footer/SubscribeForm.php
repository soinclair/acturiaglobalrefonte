<?php

namespace App\Models\Layout\Footer;

use App\Models\Layout\Layout;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class SubscribeForm extends Model
{
    use HasFactory, HasTranslations;
    
    protected $fillable = [
        'title',
        'paragraph',
        'input_placeholder',
    ];

    public $translatable = [
        'title',
        'paragraph',
        'input_placeholder',
    ];
    
    public function layout(){
        return $this->belongsTo(Layout::class);
    }
    
}
