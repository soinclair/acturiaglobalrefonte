<?php

namespace App\Models\Utility;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Slide extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        'cta_label',
        'cta_path',
        'title',
        'image',
    ];

    public $translatable = [
        'cta_label',
        'title',
    ];
    
}
