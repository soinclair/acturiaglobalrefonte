<section class="about-us pb-150 rpb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="about-image rmb-50">
                    <img class="wow fadeInBottomLeft" data-wow-duration="2s" src="{{ $image }}" alt="About Image">
                    <div class="about-border"></div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about-content">
                    <div class="section-title mb-25 wow fadeInUp" data-wow-duration="2s">
                        <h2> {!! $title !!} </h2>
                    </div>
                    <p class="wow fadeInUp" data-wow-duration="2s"> {!! nl2br( e( $paragraph )) !!} </p>

                    <i class="wow fadeInUp" data-wow-duration="2s"> {{ $italicParagraph }} </i>
            
                    <a href="{{ route( $cta->path . '.index' ) }}" class="theme-btn wow fadeInUp" data-wow-duration="2s"> {{ $cta->label }} <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>