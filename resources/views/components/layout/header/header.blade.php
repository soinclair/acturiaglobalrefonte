<header class="main-header">

    {{-- ---------- Contact & Social Media ---------- --}}
    <div class="header-top bg-orange">
        <div class="container">
            <div class="top-inner">

                @include('components.layout.header.partials.contact.contact-info')
                @include('components.layout.header.partials.contact.social-media-links')
                {{-- @include('components.layout.header.partials.contact.language-switch') --}}

            </div>
        </div>
    </div>


    {{-- ---------- Main Header Content ---------- --}}
    <div class="header-upper">
        <div class="clearfix container">
            <div class="header-inner d-lg-flex align-items-center">
                @include('components.layout.header.partials.main.logo')
                @include('components.layout.header.partials.main.menu')
                @include('components.layout.header.partials.main.support-number')

            </div>
        </div>
    </div>
    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {!! session('success') !!}
            <button
                class="close"
                data-dismiss="alert"
                type="button"
                aria-label="Close"
            >
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

</header>
