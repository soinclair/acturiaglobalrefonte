<?php

namespace App\Filament\Resources\About\AboutBannerResource\Pages;

use App\Filament\Resources\About\AboutBannerResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListAboutBanners extends ListRecords
{
    use ListRecords\Concerns\Translatable;

    protected static string $resource = AboutBannerResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
