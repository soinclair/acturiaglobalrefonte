<?php

namespace App\Filament\Resources\Layout\Footer\SubscribeFormResource\Pages;

use App\Filament\Resources\Layout\Footer\SubscribeFormResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateSubscribeForm extends CreateRecord
{
    use CreateRecord\Concerns\Translatable;

    protected static string $resource = SubscribeFormResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            // ...
        ];
    }
    
}
