<?php

namespace App\View\Components\Classes;

class ReviewButton{

    public function __construct(
        
        public int $id,
        public Image $image,
        public string $uri,
        
    ){

    }
}