<?php

namespace App\View\Components\Pages\Home;

use App\Models\Home\HomeAbout;
use App\Services\TextTransformer\TextTransformer;
use App\View\Components\Classes\CTA;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class AboutUs extends Component
{
    public string $title;
    public string $paragraph;
    public string $italicParagraph;
    public string $image;
    public CTA $cta;

    public function __construct(HomeAbout $homeAbout, TextTransformer $textTransformer){
        $this->title = $textTransformer->transformTextWithKeywords($homeAbout->title, $homeAbout->keywords)->getText();
        $this->paragraph = $homeAbout->paragraph;
        $this->italicParagraph = $homeAbout->italic_paragraph;
        $this->image = 'assets/'. $homeAbout->image;
        $this->cta = new CTA($homeAbout->cta_label, $homeAbout->cta_path);
    }


    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.pages.home.about-us');
    }
}
