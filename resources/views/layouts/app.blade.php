<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="utf-8">
        <meta
            http-equiv="X-UA-Compatible"
            content="IE=edge"
        >
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"
        >
        <title>Bienvenue || ACTUARIA GLOBAL</title>
        {{-- TODO: Navigation isnt Responsive anymore, changing to the static links doesnt fix it --}}
        @vite(['resources/css/app.css', 'resources/js/app.js'])
       {{-- @include('shared.CSSlinks')--}}
        <link
            type="image/x-icon"
            href="{{ asset('assets/images/favicon.png') }}"
            rel="shortcut icon"
        >
    </head>

    <body class="@if (app()->getLocale() == 'ar') rtl @endif">
        <div class="page-wrapper">
            <!-- Preloader -->

            <div class="preloader"></div>

            {{-- -------------- Header -------------- --}}
            <x-layout.header.header
                :supports="$layout->supports"
                :location="$layout->location"
                :social-media-links="$layout->socialMediaLinks"
                :menu-items="$layout->headerMenuItems"
                :logo="$layout->logo"
            />

            {{-- -------------- Home -------------- --}}
            @yield('home.index')

            {{-- -------------- Contact -------------- --}}
            @yield('contact.index')

            {{-- -------------- Services -------------- --}}
            @yield('services.show')

            {{-- -------------- About -------------- --}}
            @yield('about.index')

            {{-- -------------- Error pages -------------- --}}
            @yield('404')
            @yield('coming-soon.index')

            {{-- -------------- Footer -------------- --}}
            <x-layout.footer.footer
                :contacts="$layout->footerContacts"
                :footer-service="$layout->footerService"
                :services="$services"
                :recent-posts="$recentPosts"
                :footer-post="$layout->footerPost"
                :social-media-links="$layout->socialMediaLinks"
                :subscribe-form="$layout->subscribeForm"
                :menu-items="$layout->footerMenuItems"
                :copyright="$layout->copyright"
            />
        </div>
        <!--End pagewrapper-->
        {{--@include('shared.JSscripts')--}}
        @stack('scripts')
    </body>

</html>
