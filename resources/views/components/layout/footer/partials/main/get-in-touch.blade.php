<div class="col-lg-3 col-6">
    <div class="widget contact-info-widget">
        <h3 class="widget-title">{{ $contacts[0]->title }} </h3>
        <ul>
            <li>
                <h5>{{ $contacts[0]->address_title }} </h5>
                <p>{{ $contacts[0]->address }} </p>
            </li>
            <li>
                <h5>{{ $contacts[0]->contact_number_title }} </h5>
                @foreach ($contacts as $contact)

                    <p><a href="callto:{{ $contact->contact_number }}">{{ $contact->contact_number  }} </a></p>
                
                @endforeach
            </li>
        </ul>
    </div>
</div>
