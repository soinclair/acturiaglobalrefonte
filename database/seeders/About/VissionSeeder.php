<?php

namespace Database\Seeders\About;

use App\Models\About\AboutVission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class VissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        AboutVission::insert([
            [
                'tab_title' => json_encode([
                    'en' => 'History',
                    'fr' => 'Histoire',
                    'ar' => 'تاريخ',
                ]),
                'tab_image' => 'image1.png',
                'title' => json_encode([
                    'en' => 'Our Past, Your Secure Future.',
                    'fr' => 'Notre Passe Votre Avenir Securise.',
                    'ar' => 'ماضينا، مستقبلكم الآمن.',
                ]),
                'keywords' => json_encode([
                    'en' => 'Secure',
                    'fr' => 'Securise',
                    'ar' => 'آمن',
                ]),
                'paragraph' => json_encode([
                    'en' => "Founded in 2011 by experts with over 20 years of experience in the fields of actuarial science and risk management, our company has seen impressive growth over the years. Thanks to our commitment to excellence and our personalized approach, we have been able to meet the specific needs of our clients, offering them innovative and effective solutions to face the complex challenges of their industry. With our experience and deep expertise, we continue to evolve and expand our services to stay at the forefront of our industry while maintaining our commitment to client satisfaction and professional integrity.",
                    'fr' => "Fondée en 2011 par des experts cumulant plus de 20 ans d'expérience dans le domaine de l'actuariat et de la gestion des risques, notre entreprise a connu une croissance impressionnante au fil des ans. Grâce à notre engagement envers l'excellence et notre approche personnalisée, nous avons su répondre aux besoins spécifiques de nos clients, leur offrant des solutions innovantes et efficaces pour faire face aux défis complexes de leur secteur d'activité. Forts de notre expérience et de notre expertise approfondie, nous continuons à évoluer et à élargir nos services pour rester à la pointe de notre industrie, tout en maintenant notre engagement envers la satisfaction client et l'intégrité professionnelle.",
                    'ar' => "تأسست في عام 2011 من قبل خبراء يمتلكون أكثر من 20 عامًا من الخبرة في مجالات علم الاكتوارية وإدارة المخاطر، شهدت شركتنا نموًا ملحوظًا على مر السنين. بفضل التزامنا بالتميز ونهجنا المخصص، تمكنا من تلبية الاحتياجات المحددة لعملائنا، مقدمين لهم حلولًا مبتكرة وفعالة لمواجهة التحديات المعقدة في صناعتهم. مع خبرتنا وخبرتنا العميقة، نستمر في التطور وتوسيع خدماتنا للبقاء في طليعة صناعتنا، مع الحفاظ على التزامنا برضا العملاء والنزاهة المهنية.",
                ]),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'tab_title' => json_encode([
                    'en' => 'Our Commitment in Africa',
                    'fr' => 'Notre engagement en Afrique',
                    'ar' => 'التزامنا في أفريقيا',
                ]),
                'tab_image' => 'image2.png',
                'title' => json_encode([
                    'en' => 'Local Solutions for Global Challenges',
                    'fr' => 'Des Solutions Locales pour des Defis Mondiaux',
                    'ar' => 'حلول محلية للتحديات العالمية',
                ]),
                'keywords' => json_encode([
                    'en' => 'Challenges, Global',
                    'fr' => 'Defis, Mondiaux',
                    'ar' => 'تحديات، عالمية',
                ]),
                'paragraph' => json_encode([
                    'en' => "Our missions in Africa reflect our commitment to the continent and its communities. Our achievements in Africa include:
                                • Study on the technical and financial conditions related to extending coverage to other categories of public state agents as well as launching the branch of professional risks by CNSSAP in the Democratic Republic of Congo.
                                • Establishment of a health coverage scheme for civil servants for the benefit of the National Institute of Mandatory Health Insurance (INAMO) in Guinea Conakry.
                                • Actuarial study for the three branches of activity of the National Social Security Fund for State Agents (CNPSAE) in Guinea Conakry.
                                • Conducting actuarial work as part of the project to establish a new health strategy in the Republic of Congo, piloted by the World Bank.
                                • Actuarial and financial evaluation of the universal health insurance scheme in Burkina Faso.
                                • Strategic diagnosis of the National Health Insurance Fund - CANAM (Mali).
                                • Numerous missions for evaluating social commitments according to IFRS standards for companies in DRC, Benin, Guinea, Ivory Coast, and Cameroon.",
                    'fr' => "Nos missions en Afrique reflètent notre engagement envers le continent et ses communautés. Nos réalisations en Afrique incluent :
                                • Étude sur les conditions techniques et financières relatives à l’extension de la couverture aux autres catégories d’agents publics de l’État ainsi qu’au lancement de la branche des risques professionnels par la CNSSAP en République Démocratique du Congo.
                                • La mise en place d'un régime de couverture médicale des fonctionnaires au profit de l'institut national d'assurance maladie obligatoire (INAMO) en Guinée Conakry.
                                • Étude actuarielle pour les trois branches d'activité de la Caisse Nationale de Prévoyance Sociale des Agents de l'État (CNPSAE) en Guinée Conakry.
                                • Réalisation des travaux actuariels dans le cadre du projet de mise en place d'une nouvelle stratégie sanitaire à la République du Congo, projet piloté par la Banque Mondiale.
                                • Évaluation actuarielle et financière du régime d'assurance maladie universelle au Burkina Faso.
                                • Diagnostic stratégique de la Caisse Nationale d’Assurance Maladie - CANAM (Mali).
                                • De nombreuses missions d’évaluations des engagements sociaux selon les normes IFRS pour des entreprises en RDC, Bénin, Guinée, Côte d'Ivoire et Cameroun.",
                    'ar' => "تعكس مهامنا في أفريقيا التزامنا بالقارة ومجتمعاتها. تشمل إنجازاتنا في أفريقيا:
                                • دراسة حول الشروط الفنية والمالية المتعلقة بتمديد التغطية لفئات أخرى من وكلاء الدولة العموميين وكذلك إطلاق فرع المخاطر المهنية من قبل CNSSAP في جمهورية الكونغو الديمقراطية.
                                • إنشاء نظام تغطية صحية للموظفين لصالح المعهد الوطني للتأمين الصحي الإلزامي (INAMO) في غينيا كوناكري.
                                • دراسة اكتوارية للثلاثة فروع من النشاط لصندوق الضمان الاجتماعي الوطني لوكلاء الدولة (CNPSAE) في غينيا كوناكري.
                                • إجراء أعمال اكتوارية كجزء من مشروع إنشاء استراتيجية صحية جديدة في جمهورية الكونغو، تحت إشراف البنك الدولي.
                                • تقييم اكتواري ومالي لنظام التأمين الصحي الشامل في بوركينا فاسو.
                                • تشخيص استراتيجي لصندوق التأمين الصحي الوطني - CANAM (مالي).
                                • العديد من المهام لتقييم الالتزامات الاجتماعية وفقًا لمعايير IFRS لشركات في جمهورية الكونغو الديمقراطية، بنين، غينيا، ساحل العاج، والكاميرون.",
                ]),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'tab_title' => json_encode([
                    'en' => 'Our Vision',
                    'fr' => 'Notre vision',
                    'ar' => 'رؤيتنا',
                ]),
                'tab_image' => 'image3.png',
                'title' => json_encode([
                    'en' => "Predicting Uncertainty Securing What Matters",
                    'fr' => "Prédire l'incertain Securiser l'essentiel",
                    'ar' => "توقع عدم اليقين وتأمين ما يهم",
                ]),
                'keywords' => json_encode([
                    'en' => "What Matters",
                    'fr' => "l'essentiel",
                    'ar' => "ما يهم",
                ]),
                'paragraph' => json_encode([
                    'en' => "In a dynamically evolving world marked by economic, technological, and social challenges, we are determined to provide our clients with the clarity and confidence needed to successfully navigate through the complexities of the future. We firmly believe that every financial decision must be guided by rigorous analysis and anticipation of potential risks. That’s why we are committed to providing innovative and tailored actuarial solutions that enable our clients to make informed decisions and protect what matters most to them. With our expertise and commitment to excellence, we are here to help you build a safe and prosperous future, one step at a time.",
                    'fr' => "Dans un monde en évolution dynamique, marqué par des défis économiques, technologiques et sociaux, nous sommes déterminés à offrir à nos clients la clarté et la confiance nécessaires pour naviguer avec succès à travers les complexités de l'avenir. Nous croyons fermement que chaque décision financière doit être guidée par une analyse rigoureuse et une anticipation des risques potentiels. C'est pourquoi nous nous engageons à fournir des solutions actuarielles innovantes et sur mesure qui permettent à nos clients de prendre des décisions éclairées et de protéger ce qui leur est le plus cher. Avec notre expertise et notre engagement envers l'excellence, nous sommes là pour vous aider à construire un avenir sûr et prospère, un pas à la fois.",
                    'ar' => "في عالم يتطور بشكل ديناميكي، ويتميز بالتحديات الاقتصادية والتكنولوجية والاجتماعية، نحن مصممون على تزويد عملائنا بالوضوح والثقة اللازمة للتنقل بنجاح عبر تعقيدات المستقبل. نحن نؤمن بشدة بأن كل قرار مالي يجب أن يكون موجهًا بواسطة تحليل دقيق وتوقع للمخاطر المحتملة. لهذا السبب نحن ملتزمون بتقديم حلول اكتوارية مبتكرة ومخصصة تمكن عملائنا من اتخاذ قرارات مستنيرة وحماية ما هو أكثر أهمية بالنسبة لهم. مع خبرتنا والتزامنا بالتميز، نحن هنا لمساعدتك في بناء مستقبل آمن ومزدهر، خطوة بخطوة.",
                ]),
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
        
    }
}
