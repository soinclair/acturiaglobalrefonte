<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('footer_contacts', function (Blueprint $table) {
            $table->id();
            $table->json('title');
            $table->json('address_title')->nullable();
            $table->json('address')->nullable();
            $table->json('contact_number_title')->nullable();
            $table->string('contact_number')->nullable();
            $table->foreignId('layout_id')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('footer_contacts');
    }
};
