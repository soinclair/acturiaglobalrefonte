<?php

namespace App\View\Components\Classes;

class ClientItem{

    public function __construct(
        
        public string $name,
        public string $logo
    
    ){

    }

}