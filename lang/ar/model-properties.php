<?php

return [
    /* <-------------------- Layout Models --------------------> */

    /* <----------- Header Models -----------> */

    /* <--- Support ------> */
    "supports-email" => "البريد الإلكتروني",
    "supports-phone" => "رقم الهاتف",

    /* <--- Location ------> */
    "locations-label" => "اسم الموقع",
    "locations-url" => "رابط الخريطة",

    /* <--- HeaderMenuItem ------> */
    "header_menu_items-label" => "اسم العنصر",
    "header_menu_items-uri" => "الرابط",
    "header_menu_items-parent_id" => "القائمة الرئيسية",

    /* <--- Logo ------> */
    "logos-image" => "الشعار",

    /* <----------- Footer Models -----------> */

    /* <--- SubscribeForm ------> */
    "subscribe_forms-title" => "عنوان النموذج",
    "subscribe_forms-paragraph" => "وصف النموذج",
    "subscribe_forms-input_placeholder" => "حقل البريد الإلكتروني",

    /* <--- SubscribeEmail ------> */
    "subscribe_emails-email" => "بريد المشترك",

    /* <--- FooterMenuItem ------> */
    "footer_menu_items-label" => "اسم القائمة",
    "footer_menu_items-uri" => "الرابط",

    /* <--- Copyright ------> */
    "copyrights-paragraph" => "نص حقوق الطبع",

    /* <--- FooterService ------> */
    "footer_services-title" => "عنوان الخدمة",

    /* <--- FooterPost ------> */
    "footer_posts-title" => "عنوان المنشور",

    /* <--- FooterContact ------> */
    "footer_contacts-title" => "عنوان القسم",
    "footer_contacts-address_title" => "اسم العنوان",
    "footer_contacts-address" => "العنوان",
    "footer_contacts-contact_number_title" => "اسم رقم الهاتف",
    "footer_contacts-contact_number" => "رقم الهاتف",

    /* <-------------------- Home Models --------------------> */

    /* <----------- HomeAbout -----------> */
    "home_abouts-title" => "العنوان",
    "home_abouts-keywords" => "الكلمات المفتاحية",
    "home_abouts-paragraph" => "الوصف",
    "home_abouts-italic_paragraph" => "الوصف المميز",
    "home_abouts-image" => "الصورة",
    "home_abouts-cta_label" => "زر الإجراء",
    "home_abouts-cta_path" => "رابط الإجراء",

    /* <----------- HomeClient -----------> */
    "home_clients-title" => "عنوان العملاء",

    /* <----------- HomeServices -----------> */
    "home_services-title" => "عنوان القسم",
    "home_services-paragraph" => "وصف القسم",

    /* <-------------------- About Models --------------------> */

    /* <----------- AboutBanner -----------> */
    "about_banners-title" => "عنوان البانر",
    "about_banners-image" => "صورة البانر",

    /* <----------- AboutVission -----------> */
    "about_vissions-tab_title" => "عنوان التبويب",
    "about_vissions-tab_image" => "صورة التبويب",
    "about_vissions-title" => "العنوان",
    "about_vissions-keywords" => "الكلمات المفتاحية",
    "about_vissions-paragraph" => "الوصف",

    /* <-------------------- Contact Models --------------------> */

    /* <----------- Contact -----------> */
    "contacts-name" => "اسمك",
    "contacts-email" => "بريدك الإلكتروني",
    "contacts-phone_number" => "رقم الهاتف",
    "contacts-subject" => "الموضوع",
    "contacts-message" => "رسالتك",

    /* <-------------------- Service Models --------------------> */

    /* <----------- Service -----------> */
    "services-label" => "الاسم",
    "services-uri" => "الرابط",
    "services-icon" => "الأيقونة",

    /* <----------- ServiceDetail -----------> */
    "service_details-title_paragraph" => "العنوان",
    "service_details-image" => "الصورة",
    "service_details-header1" => "العنوان 1",
    "service_details-paragraph1_1" => "الفقرة 1.1",
    "service_details-paragraph2_1" => "الفقرة 2.1",
    "service_details-header2" => "العنوان 2",
    "service_details-paragraph1_2" => "الفقرة 1.2",
    "service_details-paragraph2_2" => "الفقرة 2.2",
    "service_details-header3" => "العنوان 3",
    "service_details-paragraph1_3" => "الفقرة 1.3",

    /* <-------------------- Post Models --------------------> */

    /* <----------- Post -----------> */
    "posts-title" => "العنوان",
    "posts-uri" => "الرابط",
    "posts-date" => "التاريخ",

    /* <-------------------- Client Models --------------------> */

    /* <----------- Client -----------> */
    "clients-name" => "اسم العميل",
    "clients-logo" => "شعار العميل",

    /* <-------------------- Utility Models --------------------> */

    /* <----------- SuccessItem  -----------> */
    "success_items-label" => "الوصف",
    "success_items-number" => "العدد",
    "success_items-icon" => "الأيقونة",
    "success_items-icon_style" => "نمط الأيقونة",
    "success_items-is_percentage" => "نسبة مئوية؟",

    /* <----------- Slide  -----------> */
    "slides-title" => "العنوان",
    "slides-image" => "الصورة",
    "slides-cta_label" => "زر الإجراء",
    "slides-cta_path" => "رابط الإجراء"
];
