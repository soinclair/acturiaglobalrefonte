<?php

namespace App\Models\Layout\Footer;

use App\Models\Layout\Layout;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class FooterMenuItem extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        'label',
        'uri',
    ];

    public $translatable = [
        'label',
    ];
    
    public function layout(){
        return $this->belongsTo(Layout::class);
    }
}
