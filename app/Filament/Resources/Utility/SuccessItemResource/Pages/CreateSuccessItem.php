<?php

namespace App\Filament\Resources\Utility\SuccessItemResource\Pages;

use App\Filament\Resources\Utility\SuccessItemResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateSuccessItem extends CreateRecord
{

    use CreateRecord\Concerns\Translatable;

    protected static string $resource = SuccessItemResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            // ...
        ];
    }
}
