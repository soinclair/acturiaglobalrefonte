<section class="team-section pt-135 rpt-90 pb-90 rpb-40">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-md-8">
                <div class="section-title text-center mb-80 wow fadeInUp"  data-wow-duration="2s">
                    <h2> {{ $section->headers[0] }} </h2>
                    <p> {{ $section->paragraphs[0] }} </p>
                </div>
            </div>
        </div>
    </div>
        
    <div class="team-wrap">
        @foreach ($members as $index => $member)
            <div class="team-item wow fadeInUp" data-wow-duration="2s" 
                @if ($index > 0) data-wow-delay="{{ 0.3 + $index * 0.2 }}s" @endif >

                <div class="item-image">
                    <img src="{{ asset('assets/images/team/'.$member->image->path) }}" alt="{{ $member->image->label }}">
                    <div class="social-style-two">
                        @foreach ($member->socialMediaLinks as $social)
                            <a href="{{ $social->url }}"><i class="fab fa-{{ $social->label }}"></i></a>
                        @endforeach
                    </div>
                </div>
                <div class="team-desc">
                    <h3>{{ $member->name }}</h3>
                    <p>{{ $member->position }}</p>
                </div>

            </div>
        @endforeach
    </div>
</section>  