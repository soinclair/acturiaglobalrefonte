<?php

namespace App\Filament\Resources\Utility\SuccessItemResource\Pages;

use App\Filament\Resources\Utility\SuccessItemResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditSuccessItem extends EditRecord
{

    use EditRecord\Concerns\Translatable;

    protected static string $resource = SuccessItemResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
