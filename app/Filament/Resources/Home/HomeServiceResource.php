<?php

namespace App\Filament\Resources\Home;

use App\Filament\Resources\Home\HomeServiceResource\Pages;
use App\Filament\Resources\Home\HomeServiceResource\RelationManagers;
use App\Models\Home\HomeService;
use Filament\Forms;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class HomeServiceResource extends Resource
{

    use Translatable;

    protected static ?string $model = HomeService::class;

    protected static ?string $navigationIcon = 'heroicon-o-cog';

    protected static ?string $activeNavigationIcon = 'heroicon-s-cog';

    protected static ?string $modelLabel = 'home_services';

    protected static ?string $navigationGroup = 'homes';


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('title')
                    ->required()
                    ->maxLength(255)
                    ->label(static::getDynamicLabel("title"))
                    ->translateLabel(),
                Textarea::make('paragraph')
                    ->required()
                    ->label(static::getDynamicLabel("paragraph"))
                    ->translateLabel(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('title')
                    ->sortable()
                    ->searchable()
                    ->label(static::getDynamicLabel("title"))
                    ->translateLabel(),

                TextColumn::make('paragraph')
                    ->limit(50) // limits the displayed text to 50 characters
                    ->tooltip(fn($record) => $record->paragraph) // shows full text on hover
                    ->label(static::getDynamicLabel("paragraph"))
                    ->translateLabel(),

                // TextColumn::make('created_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->toggleable(isToggledHiddenByDefault: true)
                //     ->label(static::getDynamicLabel("created_at"))
                //     ->translateLabel(),

                // TextColumn::make('updated_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->toggleable(isToggledHiddenByDefault: true)
                //     ->label(static::getDynamicLabel("updated_at"))
                //     ->translateLabel(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListHomeServices::route('/'),
            'create' => Pages\CreateHomeService::route('/create'),
            'edit' => Pages\EditHomeService::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch plural translation without "s"
    }

    public static function getModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch translation based on current locale
    }

    public static function getNavigationGroup(): ?string
    {
        return __("filament/custom/navigation-model-labels." . static::$navigationGroup); // Fetch translation based on current locale
    }

    protected static function getDynamicLabel(string $field): string
    {
        return __("model-properties." . static::$modelLabel . '-' . $field);
    }
}
