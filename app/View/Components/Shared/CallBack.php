<?php

namespace App\View\Components\Shared;

use App\View\Components\Classes\CTA;
use App\View\Components\Classes\Image;
use App\View\Components\Classes\Placeholder;
use App\View\Components\Classes\Section;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class CallBack extends Component
{
    
    /**
     * Create a new component instance.
     */

    public Image $image;
    public Section $section;
    public Placeholder $placeholder;
    public CTA $cta;

     /**
      * Create a new component instance.
      */
    public function __construct(
    ){
        $this->image = new Image('1920x615', '../images/call-back/call-back-bg.jpg');
        $this->section = new Section(["Demander un rappel."], [], ["Préférez-vous qu'on vous contacte ?", "Remplissez ce formulaire, notre équipe se fera un plaisir de répondre à vos questions."]);
        $this->placeholder = new Placeholder("Nom","Email","Telephone","Sujet","Message");
        $this->cta = new CTA("Envoyer","callback");
    }



    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.shared.call-back');
    }
}
