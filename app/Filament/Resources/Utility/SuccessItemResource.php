<?php

namespace App\Filament\Resources\Utility;

use App\Filament\Resources\Utility\SuccessItemResource\Pages;
use App\Filament\Resources\Utility\SuccessItemResource\RelationManagers;
use App\Models\About\AboutVission;
use App\Models\Home\HomeSuccess;
use App\Models\Utility\SuccessItem;
use Filament\Forms;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Form;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\BadgeColumn;
use Filament\Tables\Columns\CheckboxColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Columns\ToggleColumn;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class SuccessItemResource extends Resource
{

    use Translatable;

    protected static ?string $model = SuccessItem::class;

    protected static ?string $navigationIcon = 'heroicon-o-check-circle';

    protected static ?string $activeNavigationIcon = 'heroicon-s-check-circle';

    protected static ?string $navigationGroup = 'resources';

    protected static bool $shouldRegisterNavigation = true;

    protected static ?string $modelLabel = 'success_items';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('number')
                    ->required()
                    ->numeric()
                    ->minValue(0)
                    ->label(static::getDynamicLabel("number"))
                    ->translateLabel(),
                TextInput::make('icon')
                    ->required()
                    ->label(static::getDynamicLabel("icon"))
                    ->translateLabel(),
                Select::make('icon_style')
                    ->options([
                        'solid' => 'Solid',
                        'regular' => 'Regular',
                        'light' => 'Light',
                        'thin' => 'Thin',
                    ])
                    ->required()
                    ->label(static::getDynamicLabel("icon_style"))
                    ->translateLabel(),
                TextInput::make('label')
                    ->required()
                    ->label(static::getDynamicLabel("label"))
                    ->translateLabel(),
                Toggle::make('is_percentage')
                    ->default(false)
                    ->label(static::getDynamicLabel("is_percentage"))
                    ->translateLabel(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('label')
                    ->label(static::getDynamicLabel("label"))
                    ->translateLabel(),
                TextColumn::make('number')
                    ->label(static::getDynamicLabel("number"))
                    ->translateLabel(),
                TextColumn::make('icon')
                    ->label(static::getDynamicLabel("icon"))
                    ->translateLabel(),
                TextColumn::make('icon_style')
                    ->badge()
                    ->color(fn(string $state): string => match ($state) {
                        'solid' => 'primary',
                        'regular' => 'danger',
                        'light' => 'success',
                        'thin' => 'warning',
                    })
                    ->alignCenter()
                    ->label(static::getDynamicLabel("icon_style"))
                    ->translateLabel(),
                CheckboxColumn::make('is_percentage')
                    ->label('Is Percentage')
                    ->alignCenter()
                    ->label(static::getDynamicLabel("is_percentage"))
                    ->translateLabel(),
            ])
            // TODO: Consider Internationalize these filters
            ->filters([
                Filter::make('home_successes')
                    ->query(fn(Builder $query) => $query->whereHasMorph(
                        'successItemable',
                        [HomeSuccess::class]
                    ))
                    ->label('Home Success Items'),
                Filter::make('about_vissions')
                    ->query(fn(Builder $query) => $query->whereHasMorph(
                        'successItemable',
                        [AboutVission::class]
                    ))
                    ->label('About Vission Items')
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSuccessItems::route('/'),
            'create' => Pages\CreateSuccessItem::route('/create'),
            'edit' => Pages\EditSuccessItem::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch plural translation without "s"
    }

    public static function getModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch translation based on current locale
    }

    public static function getNavigationGroup(): ?string
    {
        return __("filament/custom/navigation-model-labels." . static::$navigationGroup); // Fetch translation based on current locale
    }

    protected static function getDynamicLabel(string $field): string
    {
        return __("model-properties." . static::$modelLabel . '-' . $field);
    }
}
