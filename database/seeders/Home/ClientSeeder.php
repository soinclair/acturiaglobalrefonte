<?php

namespace Database\Seeders\Home;

use App\Models\Home\HomeClient;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        HomeClient::insert([
            [
                'title' => json_encode([
                    'en' => 'Our Clients',
                    'fr' => 'Nos Clients',
                    'ar' => 'عملاؤنا',
                ]),
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
