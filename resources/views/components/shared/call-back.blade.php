<section class="call-back-section text-white py-150 rpt-90 rpb-100" style="--bg-callback-img: url('{{ $image->path }}');">
    <div class="call-back-shap"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="section-title wow fadeInUp"  data-wow-duration="2s">
                <h2> {{$section->headers[0]}} </h2>
                    <p>{{ $section->paragraphs[0] }} <br><br> {{ $section->paragraphs[1]}} </p>
                </div>
            </div>
            <div class="col-lg-6">
                <form id="call-back-form" class="call-back-form" name="call-back-form" action="{{route($cta->path.".store")}}" method="post">
                    @csrf
                    <div class="row clearfix">
                        <div class="col-md-6">        
                            <div class="form-group">
                                <input type="text" name="full-name" class="form-control @error('full-name') is-invalid @enderror"  value="{{ old('full-name') }}" placeholder="{{ $placeholder->name }}" required autocomplete="on">
                                @error('full-name')
                                    <div class="invalid-feedback"> {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" name="email-address" class="form-control @error('email-address') is-invalid @enderror" value="{{ old('email-address') }}" placeholder="{{ $placeholder->email }}" required autocomplete="on">
                                @error('email-address')
                                    <div class="invalid-feedback"> {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="phone-number" class="form-control @error('phone-number') is-invalid @enderror" value="{{ old('phone-number') }}" placeholder="{{ $placeholder->phoneNumber }}" autocomplete="on" > 
                                @error('phone-number')
                                    <div class="invalid-feedback"> {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">        
                            <div class="form-group">
                                <input type="text" name="subject" class="form-control @error('subject') is-invalid @enderror" value="{{ old('subject') }}" placeholder="{{ $placeholder->subject }}" required autocomplete="on" >
                                @error('subject')
                                    <div class="invalid-feedback"> {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-10">        
                            <div class="form-group">
                                <input type="text" name="short-text" class="form-control @error('short-text') is-invalid @enderror" value="{{ old('form-message') }}" placeholder="{{ $placeholder->shortText }}" autocomplete="on" >
                            </div>
                            @error('form-message')
                                <div class="invalid-feedback"> {{ $message }}</div>
                            @enderror
                        </div>                                               
                    </div>
                    <div class="form-group call-submit text-center">
                        <button class="theme-btn" type="submit"> {{ $cta->label }} <i class="fas fa-arrow-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@push('scripts')
    @if($errors->any())
    <script>
        $(document).ready(function() {
            $('html, body').animate({
                scrollTop: $('.call-back-section').offset().top
            }, 'slow');
        });
    </script>
    @endif
@endpush