<?php

namespace Database\Seeders\Home;

use App\Models\Home\Home;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Home::insert([
            [
                'created_at' => now(), 
                'updated_at' => now()
            ],
        ]);

        $this->call([
            AboutSeeder::class,
            SuccessSeeder::class,
            HeroSeeder::class,
            ServiceSeeder::class,
            ClientSeeder::class
        ]);
    }
}
