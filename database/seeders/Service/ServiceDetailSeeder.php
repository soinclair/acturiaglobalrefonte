<?php

namespace Database\Seeders\Service;

use App\Models\Service\Service;
use App\Models\Service\ServiceDetail;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ServiceDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Service Details should be in the same order as services

        $services = Service::all();
        $serviceDetails = [
            /* <----------- Actuariat conseil ------------> */
            [
                'title_paragraph' => json_encode([
                    "fr" => 'Evaluation et gestion des risques financiers pour assurer la stabilite et la rentabilite.',
                    "en" => 'Assessment and management of financial risks to ensure stability and profitability.',
                    "ar" => 'تقييم وإدارة المخاطر المالية لضمان الاستقرار والربحية.'
                ]),
                'image' => 'images/services/' . Str::slug('Actuariat conseil') . '.jpg',
                'header1' => null,
                'paragraph1_1' => null,
                'paragraph2_1' => null,
                'header2' => null,
                'paragraph1_2' => null,
                'paragraph2_2' => null,
                'header3' => null,
                'paragraph1_3' => null,
                'service_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- Protection sociale ------------> */
            [
                'title_paragraph' => json_encode([
                    "fr" => 'Optimisation de vos dispositifs de protection sociale pour le bien-etre de vos employes.',
                    "en" => 'Optimization of your social protection systems for the well-being of your employees.',
                    "ar" => 'تحسين أنظمة الحماية الاجتماعية لرفاهية موظفيك.'
                ]),
                'image' => 'images/services/' . Str::slug('Protection sociale') . '.jpg',
                'header1' => null,
                'paragraph1_1' => null,
                'paragraph2_1' => null,
                'header2' => null,
                'paragraph1_2' => null,
                'paragraph2_2' => null,
                'header3' => null,
                'paragraph1_3' => null,
                'service_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- Remuneration & avantages sociaux ------------> */
            [
                'title_paragraph' => json_encode([
                    "fr" => 'Solutions personnalisees pour optimiser vos politiques de remuneration et d\'avantages sociaux.',
                    "en" => 'Customized solutions to optimize your compensation and benefits policies.',
                    "ar" => 'حلول مخصصة لتحسين سياسات التعويضات والمزايا الخاصة بك.'
                ]),
                'image' => 'images/services/' . Str::slug('Remuneration & avantages sociaux') . '.jpg',
                'header1' => null,
                'paragraph1_1' => null,
                'paragraph2_1' => null,
                'header2' => null,
                'paragraph1_2' => null,
                'paragraph2_2' => null,
                'header3' => null,
                'paragraph1_3' => null,
                'service_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- Risk management ------------> */
            [
                'title_paragraph' => json_encode([
                    "fr" => 'Gestion proactive des risques pour proteger vos actifs et assurer la continuite des activites.',
                    "en" => 'Proactive risk management to protect your assets and ensure business continuity.',
                    "ar" => 'إدارة المخاطر بشكل استباقي لحماية أصولك وضمان استمرارية الأعمال.'
                ]),
                'image' => 'images/services/' . Str::slug('Risk management') . '.jpg',
                'header1'  => null,
                'paragraph1_1' => null,
                'paragraph2_1' => null,
                'header2' => null,
                'paragraph1_2' => null,
                'paragraph2_2' => null,
                'header3' => null,
                'paragraph1_3' => null,
                'service_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- Conseil en assurance maladie & mutualite ------------> */
            [
                'title_paragraph' => json_encode([
                    "fr" => "Accompagnement pour choisir les meilleures couvertures d'assurance sante et mutualite.",
                    "en" => "Support in choosing the best health insurance and mutual coverages.",
                    "ar" => "الدعم في اختيار أفضل تغطيات التأمين الصحي والتعاونيات."
                ]),
                'image' => 'images/services/' . Str::slug('Conseil en assurance maladie & mutualite') . '.jpg',
                'header1' => null,
                'paragraph1_1' => null,
                'paragraph2_1' => null,
                'header2' => null,
                'paragraph1_2' => null,
                'paragraph2_2' => null,
                'header3' => null,
                'paragraph1_3' => null,
                'service_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- AI & Data science ------------> */
            [
                'title_paragraph' => json_encode([
                    "fr" => 'Exploitez vos donnees avec l\'IA et la science des donnees pour des decisions strategiques.',
                    "en" => 'Leverage your data with AI and data science for strategic decisions.',
                    "ar" => 'استغل بياناتك باستخدام الذكاء الاصطناعي وعلوم البيانات لاتخاذ قرارات استراتيجية.'
                ]),
                'image' => 'images/services/' . Str::slug('AI & Data science') . '.jpg',
                'header1' => null,
                'paragraph1_1' => null,
                'paragraph2_1' => null,
                'header2' => null,
                'paragraph1_2' => null,
                'paragraph2_2' => null,
                'header3' => null,
                'paragraph1_3' => null,
                'service_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- Formation ------------> */
            [
                'title_paragraph' => json_encode([
                    "fr" => 'Programmes de formation sur mesure pour developper les competences de vos equipes.',
                    "en" => 'Tailored training programs to develop your teams\' skills.',
                    "ar" => 'برامج تدريب مصممة خصيصًا لتطوير مهارات فرقك.'
                ]),
                'image' => 'images/services/' . Str::slug('Formation') . '.jpg',
                'header1' => null,
                'paragraph1_1' => null,
                'paragraph2_1' => null,
                'header2' => null,
                'paragraph1_2' => null,
                'paragraph2_2' => null,
                'header3' => null,
                'paragraph1_3' => null,
                'service_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],

            /* <----------- Enquete de satisfaction client ------------> */
            [
                'title_paragraph' => json_encode([
                    "fr" => 'Enquetes de satisfaction pour ameliorer l\'experience client et ajuster vos services.',
                    "en" => 'Satisfaction surveys to improve customer experience and adjust your services.',
                    "ar" => 'استطلاعات الرضا لتحسين تجربة العملاء وضبط خدماتك.'
                ]),
                'image' => 'images/services/' . Str::slug('Enquete de satisfaction client') . '.jpg',
                'header1' => null,
                'paragraph1_1' => null,
                'paragraph2_1' => null,
                'header2' => null,
                'paragraph1_2' => null,
                'paragraph2_2' => null,
                'header3' => null,
                'paragraph1_3' => null,
                'service_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ];
        foreach ($services as $index => $service) {
            $serviceDetails[$index]['service_id'] = $service->id;
        }
        ServiceDetail::insert($serviceDetails);
    }
}
