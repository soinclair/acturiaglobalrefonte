<?php

namespace App\Filament\Resources\Layout\Footer;

use App\Filament\Resources\Layout\Footer\FooterContactResource\Pages;
use App\Filament\Resources\Layout\Footer\FooterContactResource\RelationManagers;
use App\Models\Layout\Footer\FooterContact;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class FooterContactResource extends Resource
{
    use Translatable;

    protected static ?string $model = FooterContact::class;

    protected static ?string $navigationIcon = 'heroicon-o-phone';

    protected static ?string $activeNavigationIcon = 'heroicon-s-phone';

    protected static ?string $modelLabel = 'footer_contacts';

    protected static ?string $navigationGroup = 'footer';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('title')
                    ->required()
                    ->label(static::getDynamicLabel("title"))
                    ->translateLabel(),
                Forms\Components\TextInput::make('address_title')
                    ->label(static::getDynamicLabel("address_title"))
                    ->translateLabel(),
                Forms\Components\Textarea::make('address')
                    ->label(static::getDynamicLabel("address"))
                    ->translateLabel(),
                Forms\Components\TextInput::make('contact_number_title')
                    ->label(static::getDynamicLabel("contact_number_title"))
                    ->translateLabel(),
                Forms\Components\TextInput::make('contact_number')
                    ->label(static::getDynamicLabel("cta_lcontact_numberabel"))
                    ->translateLabel(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->searchable()
                    ->sortable()
                    ->label(static::getDynamicLabel("title"))
                    ->translateLabel(),
                Tables\Columns\TextColumn::make('address_title')
                    ->sortable()
                    ->label(static::getDynamicLabel("address_title"))
                    ->translateLabel(),
                Tables\Columns\TextColumn::make('address')
                    ->sortable()
                    ->label(static::getDynamicLabel("address"))
                    ->translateLabel(),
                Tables\Columns\TextColumn::make('contact_number_title')
                    ->sortable()
                    ->label(static::getDynamicLabel("contact_number_title"))
                    ->translateLabel(),
                Tables\Columns\TextColumn::make('contact_number')
                    ->sortable()
                    ->label(static::getDynamicLabel("contact_number"))
                    ->translateLabel(),
                // Tables\Columns\TextColumn::make('created_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->label(static::getDynamicLabel("created_at"))
                //     ->translateLabel(),
                // Tables\Columns\TextColumn::make('updated_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->label(static::getDynamicLabel("updated_at"))
                //     ->translateLabel(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListFooterContacts::route('/'),
            'create' => Pages\CreateFooterContact::route('/create'),
            'edit' => Pages\EditFooterContact::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch plural translation without "s"
    }

    public static function getModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch translation based on current locale
    }

    public static function getNavigationGroup(): ?string
    {
        return __("filament/custom/navigation-model-labels." . static::$navigationGroup); // Fetch translation based on current locale
    }

    protected static function getDynamicLabel(string $field): string
    {
        return __("model-properties." . static::$modelLabel . '-' . $field);
    }
}
