@extends('layouts.app')
@section('services.show')
    <x-shared.breadcrumb-banner 
        :title="'Services'"
        :image="asset('assets/images/banner/services-bg.jpg')"
        {{-- 
        :breadcrumbs=""
        --}}
    />

    <section class="service-details pt-140 pb-150 rpt-90 rpb-100">
        <div class="container">
            <div class="row">
                @include('services.partials.details')
                @include('services.partials.sidebar')            
            </div>
        </div>
    </section>

    {{--
    <x-shared.testimonial 
     
        :section="" 
        :reviews="" 
        :reviewButtons=""  
    
    /> 
    --}}
    
    <x-shared.call-to-action 
    {{-- 
        :section=""
        :cta=""
        :theme=""
    --}}
    />
@endsection
