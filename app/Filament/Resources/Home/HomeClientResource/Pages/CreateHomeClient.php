<?php

namespace App\Filament\Resources\Home\HomeClientResource\Pages;

use App\Filament\Resources\Home\HomeClientResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateHomeClient extends CreateRecord
{

    use CreateRecord\Concerns\Translatable;

    protected static string $resource = HomeClientResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            // ...
        ];
    }
}
