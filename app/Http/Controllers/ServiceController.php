<?php

namespace App\Http\Controllers;

use App\Models\Service\Service;
use Illuminate\Http\Request;
use App\Services\TextTransformer\TextTransformer;

class ServiceController extends Controller
{   

    protected $textTransformer;
    
    public function __construct(TextTransformer $textTransformer)
    {
        $this->textTransformer = $textTransformer;
    }

    public function index(){
        return redirect()->route('services.show',['conseil-en-assurance-maladie-mutualite']);
    }

    public function show($uri){ 
        
        $currentLocale = app()->getLocale();
        
        $service = Service::where("uri->$currentLocale", $uri)->with('serviceDetail')->firstOrFail();

        // Ensure serviceDetail exists
        if ($service->serviceDetail) {
            // Define the list of property names to check
            $properties = [
                'title_paragraph',
                'header1',
                'paragraph1_1',
                'paragraph2_1',
                'header2',
                'paragraph1_2',
                'paragraph2_2',
                'header3',
                'paragraph1_3'
            ];

            foreach ($properties as $property) {
                
                $service->serviceDetail[$property] = $service->serviceDetail[$property]
                        ? $this->textTransformer
                            ->transformTextWithLists($service->serviceDetail[$property])
                            ->removeAccents()
                            ->getText()
                        : null;
            }
        }

        $previousServices = Service::where('id', '<', $service->id)
                                   ->orderBy('id', 'desc')
                                   ->take(2)
                                   ->get()
                                   ->reverse()
                                   ->values();

        if(count($previousServices) < 2){

            $remaining = 2-count($previousServices);

            $remainingServices= Service::orderBy('id', 'desc')
                                       ->take($remaining)
                                       ->get();

            $previousServices = $previousServices->concat($remainingServices)
                                                 ->reverse()
                                                 ->values();    
        }

        $nextServices = Service::where('id', '>', $service->id)
                               ->orderBy('id')
                               ->take(2)
                               ->get();

        if(count($nextServices) < 2){
            $remaining = 2-count($nextServices);
            $remainingServices= Service::orderBy('id', 'asc')
                                       ->take($remaining)
                                       ->get();
            $nextServices = $nextServices->concat($remainingServices);    
        }
        
        $services = $previousServices->push($service)->concat($nextServices);
        $currentService = $service;
        $nextService = $services[3];
        $previousService = $services[1];

        return view('services.services',compact('services','currentService','nextService','previousService'));
    }
    
}
