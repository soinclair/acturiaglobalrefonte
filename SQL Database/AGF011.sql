-- MySQL dump 10.13  Distrib 8.0.41, for Linux (x86_64)
--
-- Host: localhost    Database: agr
-- ------------------------------------------------------
-- Server version	8.0.41-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `about_banners`
--

DROP TABLE IF EXISTS `about_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `about_banners` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` json NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `about_banners_about_id_foreign` (`about_id`),
  CONSTRAINT `about_banners_about_id_foreign` FOREIGN KEY (`about_id`) REFERENCES `abouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about_banners`
--

LOCK TABLES `about_banners` WRITE;
/*!40000 ALTER TABLE `about_banners` DISABLE KEYS */;
INSERT INTO `about_banners` VALUES (1,'{\"ar\": \"حول\", \"en\": \"About\", \"fr\": \"À propos\"}','about-banner.jpg',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `about_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `about_vissions`
--

DROP TABLE IF EXISTS `about_vissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `about_vissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tab_title` json NOT NULL,
  `tab_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` json NOT NULL,
  `keywords` json NOT NULL,
  `paragraph` json NOT NULL,
  `about_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `about_vissions_about_id_foreign` (`about_id`),
  CONSTRAINT `about_vissions_about_id_foreign` FOREIGN KEY (`about_id`) REFERENCES `abouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about_vissions`
--

LOCK TABLES `about_vissions` WRITE;
/*!40000 ALTER TABLE `about_vissions` DISABLE KEYS */;
INSERT INTO `about_vissions` VALUES (1,'{\"ar\": \"تاريخ\", \"en\": \"History\", \"fr\": \"Histoire\"}','image1.png','{\"ar\": \"ماضينا، مستقبلكم الآمن.\", \"en\": \"Our Past, Your Secure Future.\", \"fr\": \"Notre Passe Votre Avenir Securise.\"}','{\"ar\": \"آمن\", \"en\": \"Secure\", \"fr\": \"Securise\"}','{\"ar\": \"تأسست في عام 2011 من قبل خبراء يمتلكون أكثر من 20 عامًا من الخبرة في مجالات علم الاكتوارية وإدارة المخاطر، شهدت شركتنا نموًا ملحوظًا على مر السنين. بفضل التزامنا بالتميز ونهجنا المخصص، تمكنا من تلبية الاحتياجات المحددة لعملائنا، مقدمين لهم حلولًا مبتكرة وفعالة لمواجهة التحديات المعقدة في صناعتهم. مع خبرتنا وخبرتنا العميقة، نستمر في التطور وتوسيع خدماتنا للبقاء في طليعة صناعتنا، مع الحفاظ على التزامنا برضا العملاء والنزاهة المهنية.\", \"en\": \"Founded in 2011 by experts with over 20 years of experience in the fields of actuarial science and risk management, our company has seen impressive growth over the years. Thanks to our commitment to excellence and our personalized approach, we have been able to meet the specific needs of our clients, offering them innovative and effective solutions to face the complex challenges of their industry. With our experience and deep expertise, we continue to evolve and expand our services to stay at the forefront of our industry while maintaining our commitment to client satisfaction and professional integrity.\", \"fr\": \"Fondée en 2011 par des experts cumulant plus de 20 ans d\'expérience dans le domaine de l\'actuariat et de la gestion des risques, notre entreprise a connu une croissance impressionnante au fil des ans. Grâce à notre engagement envers l\'excellence et notre approche personnalisée, nous avons su répondre aux besoins spécifiques de nos clients, leur offrant des solutions innovantes et efficaces pour faire face aux défis complexes de leur secteur d\'activité. Forts de notre expérience et de notre expertise approfondie, nous continuons à évoluer et à élargir nos services pour rester à la pointe de notre industrie, tout en maintenant notre engagement envers la satisfaction client et l\'intégrité professionnelle.\"}',1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(2,'{\"ar\": \"التزامنا في أفريقيا\", \"en\": \"Our Commitment in Africa\", \"fr\": \"Notre engagement en Afrique\"}','image2.png','{\"ar\": \"حلول محلية للتحديات العالمية\", \"en\": \"Local Solutions for Global Challenges\", \"fr\": \"Des Solutions Locales pour des Defis Mondiaux\"}','{\"ar\": \"تحديات، عالمية\", \"en\": \"Challenges, Global\", \"fr\": \"Defis, Mondiaux\"}','{\"ar\": \"تعكس مهامنا في أفريقيا التزامنا بالقارة ومجتمعاتها. تشمل إنجازاتنا في أفريقيا:\\n                                • دراسة حول الشروط الفنية والمالية المتعلقة بتمديد التغطية لفئات أخرى من وكلاء الدولة العموميين وكذلك إطلاق فرع المخاطر المهنية من قبل CNSSAP في جمهورية الكونغو الديمقراطية.\\n                                • إنشاء نظام تغطية صحية للموظفين لصالح المعهد الوطني للتأمين الصحي الإلزامي (INAMO) في غينيا كوناكري.\\n                                • دراسة اكتوارية للثلاثة فروع من النشاط لصندوق الضمان الاجتماعي الوطني لوكلاء الدولة (CNPSAE) في غينيا كوناكري.\\n                                • إجراء أعمال اكتوارية كجزء من مشروع إنشاء استراتيجية صحية جديدة في جمهورية الكونغو، تحت إشراف البنك الدولي.\\n                                • تقييم اكتواري ومالي لنظام التأمين الصحي الشامل في بوركينا فاسو.\\n                                • تشخيص استراتيجي لصندوق التأمين الصحي الوطني - CANAM (مالي).\\n                                • العديد من المهام لتقييم الالتزامات الاجتماعية وفقًا لمعايير IFRS لشركات في جمهورية الكونغو الديمقراطية، بنين، غينيا، ساحل العاج، والكاميرون.\", \"en\": \"Our missions in Africa reflect our commitment to the continent and its communities. Our achievements in Africa include:\\n                                • Study on the technical and financial conditions related to extending coverage to other categories of public state agents as well as launching the branch of professional risks by CNSSAP in the Democratic Republic of Congo.\\n                                • Establishment of a health coverage scheme for civil servants for the benefit of the National Institute of Mandatory Health Insurance (INAMO) in Guinea Conakry.\\n                                • Actuarial study for the three branches of activity of the National Social Security Fund for State Agents (CNPSAE) in Guinea Conakry.\\n                                • Conducting actuarial work as part of the project to establish a new health strategy in the Republic of Congo, piloted by the World Bank.\\n                                • Actuarial and financial evaluation of the universal health insurance scheme in Burkina Faso.\\n                                • Strategic diagnosis of the National Health Insurance Fund - CANAM (Mali).\\n                                • Numerous missions for evaluating social commitments according to IFRS standards for companies in DRC, Benin, Guinea, Ivory Coast, and Cameroon.\", \"fr\": \"Nos missions en Afrique reflètent notre engagement envers le continent et ses communautés. Nos réalisations en Afrique incluent :\\n                                • Étude sur les conditions techniques et financières relatives à l’extension de la couverture aux autres catégories d’agents publics de l’État ainsi qu’au lancement de la branche des risques professionnels par la CNSSAP en République Démocratique du Congo.\\n                                • La mise en place d\'un régime de couverture médicale des fonctionnaires au profit de l\'institut national d\'assurance maladie obligatoire (INAMO) en Guinée Conakry.\\n                                • Étude actuarielle pour les trois branches d\'activité de la Caisse Nationale de Prévoyance Sociale des Agents de l\'État (CNPSAE) en Guinée Conakry.\\n                                • Réalisation des travaux actuariels dans le cadre du projet de mise en place d\'une nouvelle stratégie sanitaire à la République du Congo, projet piloté par la Banque Mondiale.\\n                                • Évaluation actuarielle et financière du régime d\'assurance maladie universelle au Burkina Faso.\\n                                • Diagnostic stratégique de la Caisse Nationale d’Assurance Maladie - CANAM (Mali).\\n                                • De nombreuses missions d’évaluations des engagements sociaux selon les normes IFRS pour des entreprises en RDC, Bénin, Guinée, Côte d\'Ivoire et Cameroun.\"}',1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(3,'{\"ar\": \"رؤيتنا\", \"en\": \"Our Vision\", \"fr\": \"Notre vision\"}','image3.png','{\"ar\": \"توقع عدم اليقين وتأمين ما يهم\", \"en\": \"Predicting Uncertainty Securing What Matters\", \"fr\": \"Prédire l\'incertain Securiser l\'essentiel\"}','{\"ar\": \"ما يهم\", \"en\": \"What Matters\", \"fr\": \"l\'essentiel\"}','{\"ar\": \"في عالم يتطور بشكل ديناميكي، ويتميز بالتحديات الاقتصادية والتكنولوجية والاجتماعية، نحن مصممون على تزويد عملائنا بالوضوح والثقة اللازمة للتنقل بنجاح عبر تعقيدات المستقبل. نحن نؤمن بشدة بأن كل قرار مالي يجب أن يكون موجهًا بواسطة تحليل دقيق وتوقع للمخاطر المحتملة. لهذا السبب نحن ملتزمون بتقديم حلول اكتوارية مبتكرة ومخصصة تمكن عملائنا من اتخاذ قرارات مستنيرة وحماية ما هو أكثر أهمية بالنسبة لهم. مع خبرتنا والتزامنا بالتميز، نحن هنا لمساعدتك في بناء مستقبل آمن ومزدهر، خطوة بخطوة.\", \"en\": \"In a dynamically evolving world marked by economic, technological, and social challenges, we are determined to provide our clients with the clarity and confidence needed to successfully navigate through the complexities of the future. We firmly believe that every financial decision must be guided by rigorous analysis and anticipation of potential risks. That’s why we are committed to providing innovative and tailored actuarial solutions that enable our clients to make informed decisions and protect what matters most to them. With our expertise and commitment to excellence, we are here to help you build a safe and prosperous future, one step at a time.\", \"fr\": \"Dans un monde en évolution dynamique, marqué par des défis économiques, technologiques et sociaux, nous sommes déterminés à offrir à nos clients la clarté et la confiance nécessaires pour naviguer avec succès à travers les complexités de l\'avenir. Nous croyons fermement que chaque décision financière doit être guidée par une analyse rigoureuse et une anticipation des risques potentiels. C\'est pourquoi nous nous engageons à fournir des solutions actuarielles innovantes et sur mesure qui permettent à nos clients de prendre des décisions éclairées et de protéger ce qui leur est le plus cher. Avec notre expertise et notre engagement envers l\'excellence, nous sommes là pour vous aider à construire un avenir sûr et prospère, un pas à la fois.\"}',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `about_vissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `abouts`
--

DROP TABLE IF EXISTS `abouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `abouts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abouts`
--

LOCK TABLES `abouts` WRITE;
/*!40000 ALTER TABLE `abouts` DISABLE KEYS */;
INSERT INTO `abouts` VALUES (1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `abouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache` (
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache`
--

LOCK TABLES `cache` WRITE;
/*!40000 ALTER TABLE `cache` DISABLE KEYS */;
INSERT INTO `cache` VALUES ('356a192b7913b04c54574d18c28d46e6395428ab','i:1;',1737327333),('356a192b7913b04c54574d18c28d46e6395428ab:timer','i:1737327333;',1737327333),('a17961fa74e9275d529f489537f179c05d50c2f3','i:1;',1737327307),('a17961fa74e9275d529f489537f179c05d50c2f3:timer','i:1737327307;',1737327307);
/*!40000 ALTER TABLE `cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_locks`
--

DROP TABLE IF EXISTS `cache_locks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_locks` (
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_locks`
--

LOCK TABLES `cache_locks` WRITE;
/*!40000 ALTER TABLE `cache_locks` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_locks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `call_back_emails`
--

DROP TABLE IF EXISTS `call_back_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `call_back_emails` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `call_back_emails`
--

LOCK TABLES `call_back_emails` WRITE;
/*!40000 ALTER TABLE `call_back_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `call_back_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_categories`
--

DROP TABLE IF EXISTS `case_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `case_categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_categories`
--

LOCK TABLES `case_categories` WRITE;
/*!40000 ALTER TABLE `case_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `case_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_details`
--

DROP TABLE IF EXISTS `case_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `case_details` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_details`
--

LOCK TABLES `case_details` WRITE;
/*!40000 ALTER TABLE `case_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `case_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases`
--

DROP TABLE IF EXISTS `cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cases` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `case_category_id` bigint unsigned NOT NULL,
  `home_case_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases`
--

LOCK TABLES `cases` WRITE;
/*!40000 ALTER TABLE `cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clients` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clients_name_unique` (`name`),
  UNIQUE KEY `clients_logo_unique` (`logo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,'CNSS','assets/images/clients/cnss.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(2,'Dell','assets/images/clients/dell.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(3,'Lafarge','assets/images/clients/lafarge.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(4,'Marsa Maroc','assets/images/clients/marsa-maroc.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(5,'OCP','assets/images/clients/ocp.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(6,'ONCF','assets/images/clients/oncf.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(7,'Roca','assets/images/clients/roca.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(8,'Royal Air Maroc','assets/images/clients/royal-air-maroc.png','2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contacts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `copyrights`
--

DROP TABLE IF EXISTS `copyrights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `copyrights` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `paragraph` json NOT NULL,
  `layout_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `copyrights`
--

LOCK TABLES `copyrights` WRITE;
/*!40000 ALTER TABLE `copyrights` DISABLE KEYS */;
INSERT INTO `copyrights` VALUES (1,'{\"ar\": \"© 2024 ACTUARIA GLOBAL - جميع الحقوق محفوظة\", \"en\": \"© 2024 ACTUARIA GLOBAL - All rights reserved\", \"fr\": \"© 2024 ACTUARIA GLOBAL - Tous droits réservés\"}',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `copyrights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `footer_contacts`
--

DROP TABLE IF EXISTS `footer_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `footer_contacts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` json NOT NULL,
  `address_title` json DEFAULT NULL,
  `address` json DEFAULT NULL,
  `contact_number_title` json DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `layout_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `footer_contacts`
--

LOCK TABLES `footer_contacts` WRITE;
/*!40000 ALTER TABLE `footer_contacts` DISABLE KEYS */;
INSERT INTO `footer_contacts` VALUES (1,'{\"ar\": \"اتصل بنا\", \"en\": \"Contact Us\", \"fr\": \"Contactez Nous\"}','{\"ar\": \"عنواننا\", \"en\": \"Our Address\", \"fr\": \"Notre Address\"}','{\"ar\": \"445 أ، شارع عبد المومن، الدار البيضاء، المغرب.\", \"en\": \"445 A, Bd Abdelmoumen, Casablanca, Morocco.\", \"fr\": \"445 A, Bd Abdelmoumen, Casablanca, Maroc.\"}','{\"ar\": \"رقم الاتصال\", \"en\": \"Contact Number\", \"fr\": \"Numero de contact\"}','+212 (0) 5 22 86 28 88',1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(2,'{\"ar\": \"N/A\", \"en\": \"N/A\", \"fr\": \"N/A\"}','{\"ar\": \"N/A\", \"en\": \"N/A\", \"fr\": \"N/A\"}','{\"ar\": \"N/A\", \"en\": \"N/A\", \"fr\": \"N/A\"}','{\"ar\": \"N/A\", \"en\": \"N/A\", \"fr\": \"N/A\"}','+212 (0) 5 22 86 28 86',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `footer_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `footer_menu_items`
--

DROP TABLE IF EXISTS `footer_menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `footer_menu_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `label` json NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `layout_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `footer_menu_items`
--

LOCK TABLES `footer_menu_items` WRITE;
/*!40000 ALTER TABLE `footer_menu_items` DISABLE KEYS */;
INSERT INTO `footer_menu_items` VALUES (1,'{\"ar\": \"الدعم\", \"en\": \"Support\", \"fr\": \"Support\"}','contact',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `footer_menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `footer_posts`
--

DROP TABLE IF EXISTS `footer_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `footer_posts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` json NOT NULL,
  `layout_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `footer_posts`
--

LOCK TABLES `footer_posts` WRITE;
/*!40000 ALTER TABLE `footer_posts` DISABLE KEYS */;
INSERT INTO `footer_posts` VALUES (1,'{\"ar\": \"مقالات حديثة\", \"en\": \"Recent Articles\", \"fr\": \"Articles récents\"}',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `footer_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `footer_services`
--

DROP TABLE IF EXISTS `footer_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `footer_services` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` json NOT NULL,
  `layout_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `footer_services`
--

LOCK TABLES `footer_services` WRITE;
/*!40000 ALTER TABLE `footer_services` DISABLE KEYS */;
INSERT INTO `footer_services` VALUES (1,'{\"ar\": \"خدماتنا\", \"en\": \"Our Services\", \"fr\": \"Nos Services\"}',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `footer_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `header_menu_items`
--

DROP TABLE IF EXISTS `header_menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `header_menu_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `label` json NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` bigint unsigned DEFAULT NULL,
  `layout_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `header_menu_items_parent_id_foreign` (`parent_id`),
  CONSTRAINT `header_menu_items_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `header_menu_items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `header_menu_items`
--

LOCK TABLES `header_menu_items` WRITE;
/*!40000 ALTER TABLE `header_menu_items` DISABLE KEYS */;
INSERT INTO `header_menu_items` VALUES (1,'{\"ar\": \"الرئيسية\", \"en\": \"Home\", \"fr\": \"Accueil\"}','home',NULL,1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(2,'{\"ar\": \"من نحن؟\", \"en\": \"Who We Are?\", \"fr\": \"Qui Sommes Nous?\"}','about',NULL,1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(3,'{\"ar\": \"الخدمات\", \"en\": \"Services\", \"fr\": \"Services\"}','services',NULL,1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(4,'{\"ar\": \"اتصل بنا\", \"en\": \"Contact\", \"fr\": \"Contact\"}','contact',NULL,1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(5,'{\"ar\": \"استشارات الاكتوارية\", \"en\": \"Actuarial Consulting\", \"fr\": \"Actuariat conseil\"}','actuariat-conseil',3,1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(6,'{\"ar\": \"الحماية الاجتماعية\", \"en\": \"Social Protection\", \"fr\": \"Protection sociale\"}','protection-sociale',3,1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(7,'{\"ar\": \"التعويضات والمزايا\", \"en\": \"Compensation & Benefits\", \"fr\": \"Remuneration & avantages sociaux\"}','remuneration-avantages-sociaux',3,1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(8,'{\"ar\": \"إدارة المخاطر\", \"en\": \"Risk Management\", \"fr\": \"Risk management\"}','risk-management',3,1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(9,'{\"ar\": \"استشارة التأمين الصحي والتعاونيات\", \"en\": \"Health Insurance & Mutual Advice\", \"fr\": \"Conseil en assurance maladie & mutualite\"}','conseil-en-assurance-maladie-mutualite',3,1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(10,'{\"ar\": \"الذكاء الاصطناعي وعلوم البيانات\", \"en\": \"AI & Data Science\", \"fr\": \"AI & Data science\"}','ai-data-science',3,1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(11,'{\"ar\": \"تدريب\", \"en\": \"Training\", \"fr\": \"Formation\"}','formation',3,1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(12,'{\"ar\": \"استطلاع رضا العملاء\", \"en\": \"Customer Satisfaction Survey\", \"fr\": \"Enquete de satisfaction client\"}','enquete-de-satisfaction-client',3,1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `header_menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_abouts`
--

DROP TABLE IF EXISTS `home_abouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `home_abouts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` json NOT NULL,
  `keywords` json NOT NULL,
  `paragraph` json NOT NULL,
  `italic_paragraph` json NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cta_label` json NOT NULL,
  `cta_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `home_abouts_home_id_foreign` (`home_id`),
  CONSTRAINT `home_abouts_home_id_foreign` FOREIGN KEY (`home_id`) REFERENCES `homes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_abouts`
--

LOCK TABLES `home_abouts` WRITE;
/*!40000 ALTER TABLE `home_abouts` DISABLE KEYS */;
INSERT INTO `home_abouts` VALUES (1,'{\"ar\": \"تحالف الشغف والدقة\", \"en\": \"The Alliance of Passion and Precision\", \"fr\": \"L\'alliance de la passion et de la précision\"}','{\"ar\": \"الدقة\", \"en\": \"Passion,Precision\", \"fr\": \"passion,précision\"}','{\"ar\": \"ACTUAIRA GLOBAL هو شريكك الموثوق للتنقل في عالم يتطور باستمرار. نحن نجمع بين خبرتنا المثبتة في علم الاكتوارية وإدارة المخاطر مع أحدث التطورات في علم البيانات لنقدم لك حلولًا شاملة ومبتكرة.\", \"en\": \"ACTUAIRA GLOBAL is your trusted partner to navigate a constantly evolving world. We combine our proven expertise in actuarial science and risk management with the latest advancements in data science to provide you with comprehensive and innovative solutions.\", \"fr\": \"ACTUAIRA GLOBAL est votre partenaire de confiance pour naviguer dans un monde en constante évolution. Nous combinons notre expertise éprouvée en actuariat et gestion des risques avec les dernières avancées en data science pour vous offrir des solutions complètes et innovantes.\"}','{\"ar\": \"سواء كنت تبحث عن تعظيم الأرباح، أو إدارة المخاطر بفعالية، أو التخطيط الاستراتيجي للمستقبل، أو تحسين العمليات التجارية من خلال الذكاء الاصطناعي، نحن هنا لتزويدك بنصائح موثوقة واستراتيجيات مثبتة.\", \"en\": \"Whether you\'re looking to maximize profits, manage risks effectively, plan strategically for the future, or optimize business processes through artificial intelligence, we are here to provide you with reliable advice and proven strategies.\", \"fr\": \"Que vous cherchiez à maximiser vos profits, à gérer efficacement vos risques, à planifier stratégiquement pour l\'avenir ou à optimiser vos processus métier grâce à l\'intelligence artificielle, nous sommes là pour vous fournir des conseils fiables et des stratégies éprouvées.\"}','images/about/about.jpg','{\"ar\": \"تعرف على المزيد\", \"en\": \"Learn More\", \"fr\": \"Savoir plus\"}','about',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `home_abouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_clients`
--

DROP TABLE IF EXISTS `home_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `home_clients` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` json NOT NULL,
  `home_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `home_clients_home_id_foreign` (`home_id`),
  CONSTRAINT `home_clients_home_id_foreign` FOREIGN KEY (`home_id`) REFERENCES `homes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_clients`
--

LOCK TABLES `home_clients` WRITE;
/*!40000 ALTER TABLE `home_clients` DISABLE KEYS */;
INSERT INTO `home_clients` VALUES (1,'{\"ar\": \"عملاؤنا\", \"en\": \"Our Clients\", \"fr\": \"Nos Clients\"}',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `home_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_heroes`
--

DROP TABLE IF EXISTS `home_heroes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `home_heroes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `home_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `home_heroes_home_id_foreign` (`home_id`),
  CONSTRAINT `home_heroes_home_id_foreign` FOREIGN KEY (`home_id`) REFERENCES `homes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_heroes`
--

LOCK TABLES `home_heroes` WRITE;
/*!40000 ALTER TABLE `home_heroes` DISABLE KEYS */;
INSERT INTO `home_heroes` VALUES (1,1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `home_heroes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_services`
--

DROP TABLE IF EXISTS `home_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `home_services` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` json NOT NULL,
  `paragraph` json NOT NULL,
  `home_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `home_services_home_id_foreign` (`home_id`),
  CONSTRAINT `home_services_home_id_foreign` FOREIGN KEY (`home_id`) REFERENCES `homes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_services`
--

LOCK TABLES `home_services` WRITE;
/*!40000 ALTER TABLE `home_services` DISABLE KEYS */;
INSERT INTO `home_services` VALUES (1,'{\"ar\": \"خدماتنا.\", \"en\": \"Our Services\", \"fr\": \"Nos Services.\"}','{\"ar\": \"نحن نقدم مجموعة متنوعة من الخدمات المصممة لتلبية احتياجاتك المحددة. سواء كنت شركة صغيرة تسعى لفهم وإدارة مخاطرها، أو شركة كبيرة تتطلب تحليلاً عميقاً لتحسين استراتيجياتها المالية.\", \"en\": \"We offer a diverse range of services designed to meet your specific needs. Whether you are a small business looking to understand and manage your risks, or a large corporation requiring in-depth analysis to optimize your financial strategies.\", \"fr\": \"Nous offrons une palette diversifiée de services conçus pour répondre à vos besoins spécifiques. Que vous soyez une petite entreprise cherchant à comprendre et à gérer ses risques, ou une grande entreprise nécessitant une analyse approfondie pour optimiser ses stratégies financières.\"}',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `home_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_successes`
--

DROP TABLE IF EXISTS `home_successes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `home_successes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `home_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `home_successes_home_id_foreign` (`home_id`),
  CONSTRAINT `home_successes_home_id_foreign` FOREIGN KEY (`home_id`) REFERENCES `homes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_successes`
--

LOCK TABLES `home_successes` WRITE;
/*!40000 ALTER TABLE `home_successes` DISABLE KEYS */;
INSERT INTO `home_successes` VALUES (1,1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `home_successes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homes`
--

DROP TABLE IF EXISTS `homes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `homes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homes`
--

LOCK TABLES `homes` WRITE;
/*!40000 ALTER TABLE `homes` DISABLE KEYS */;
INSERT INTO `homes` VALUES (1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `homes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_batches`
--

DROP TABLE IF EXISTS `job_batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `job_batches` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_jobs` int NOT NULL,
  `pending_jobs` int NOT NULL,
  `failed_jobs` int NOT NULL,
  `failed_job_ids` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` mediumtext COLLATE utf8mb4_unicode_ci,
  `cancelled_at` int DEFAULT NULL,
  `created_at` int NOT NULL,
  `finished_at` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_batches`
--

LOCK TABLES `job_batches` WRITE;
/*!40000 ALTER TABLE `job_batches` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_batches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint unsigned NOT NULL,
  `reserved_at` int unsigned DEFAULT NULL,
  `available_at` int unsigned NOT NULL,
  `created_at` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `layouts`
--

DROP TABLE IF EXISTS `layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `layouts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `layouts`
--

LOCK TABLES `layouts` WRITE;
/*!40000 ALTER TABLE `layouts` DISABLE KEYS */;
INSERT INTO `layouts` VALUES (1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `locations` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `label` json NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `layout_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'{\"ar\": \"الدار البيضاء، المغرب.\", \"en\": \"Casablanca, Morocco.\", \"fr\": \"Casablanca, Maroc.\"}','https://maps.app.goo.gl/mnUHLwmQWGQqH3q8A',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logos`
--

DROP TABLE IF EXISTS `logos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `logos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `layout_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logos`
--

LOCK TABLES `logos` WRITE;
/*!40000 ALTER TABLE `logos` DISABLE KEYS */;
INSERT INTO `logos` VALUES (1,'images/logo.png',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `logos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'0001_01_01_000000_create_users_table',1),(2,'0001_01_01_000001_create_cache_table',1),(3,'0001_01_01_000002_create_jobs_table',1),(4,'2024_04_17_114643_create_layouts_table',1),(5,'2024_04_18_095514_create_supports_table',1),(6,'2024_04_18_095515_create_locations_table',1),(7,'2024_04_18_095516_create_social_media_links_table',1),(8,'2024_04_18_095517_create_header_menu_items_table',1),(9,'2024_04_19_155341_create_services_table',1),(10,'2024_04_19_181738_create_subscribe_forms_table',1),(11,'2024_04_19_190048_create_subscribe_emails_table',1),(12,'2024_04_19_194518_create_posts_table',1),(13,'2024_04_20_142042_create_footer_menu_items_table',1),(14,'2024_04_20_145346_create_copyrights_table',1),(15,'2024_04_20_153226_create_logos_table',1),(16,'2024_04_22_115310_create_footer_services_table',1),(17,'2024_04_22_115324_create_footer_posts_table',1),(18,'2024_04_22_115847_create_footer_contacts_table',1),(19,'2024_04_22_155737_create_homes_table',1),(20,'2024_04_22_181619_create_cases_table',1),(21,'2024_04_22_182244_create_case_categories_table',1),(22,'2024_04_22_182329_create_case_details_table',1),(23,'2024_04_23_190948_create_contacts_table',1),(24,'2024_04_27_010035_create_service_details_table',1),(25,'2024_04_27_182314_create_call_back_emails_table',1),(26,'2024_07_15_204457_create_home_successes_table',1),(27,'2024_07_15_204504_create_home_heroes_table',1),(28,'2024_07_15_204509_create_home_abouts_table',1),(29,'2024_07_28_161416_create_success_items_table',1),(30,'2024_07_28_163701_create_slides_table',1),(31,'2024_07_28_172408_create_clients_table',1),(32,'2024_07_28_173651_create_home_clients_table',1),(33,'2024_07_28_173702_create_home_services_table',1),(34,'2024_08_03_153139_create_abouts_table',1),(35,'2024_08_03_153539_create_about_banners_table',1),(36,'2024_08_03_153602_create_about_vissions_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reset_tokens`
--

DROP TABLE IF EXISTS `password_reset_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_reset_tokens`
--

LOCK TABLES `password_reset_tokens` WRITE;
/*!40000 ALTER TABLE `password_reset_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_reset_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `posts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'Post 1','post-1','2024-12-01','2024-12-01 17:13:40','2024-12-01 17:13:40'),(2,'Post 2','post-2','2024-12-01','2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_details`
--

DROP TABLE IF EXISTS `service_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_details` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title_paragraph` json NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `header1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paragraph1_1` text COLLATE utf8mb4_unicode_ci,
  `paragraph2_1` text COLLATE utf8mb4_unicode_ci,
  `header2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paragraph1_2` text COLLATE utf8mb4_unicode_ci,
  `paragraph2_2` text COLLATE utf8mb4_unicode_ci,
  `header3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paragraph1_3` text COLLATE utf8mb4_unicode_ci,
  `service_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_details_service_id_foreign` (`service_id`),
  CONSTRAINT `service_details_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_details`
--

LOCK TABLES `service_details` WRITE;
/*!40000 ALTER TABLE `service_details` DISABLE KEYS */;
INSERT INTO `service_details` VALUES (1,'{\"ar\": \"تقييم وإدارة المخاطر المالية لضمان الاستقرار والربحية.\", \"en\": \"Assessment and management of financial risks to ensure stability and profitability.\", \"fr\": \"Evaluation et gestion des risques financiers pour assurer la stabilite et la rentabilite.\"}','images/services/actuariat-conseil.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(2,'{\"ar\": \"تحسين أنظمة الحماية الاجتماعية لرفاهية موظفيك.\", \"en\": \"Optimization of your social protection systems for the well-being of your employees.\", \"fr\": \"Optimisation de vos dispositifs de protection sociale pour le bien-etre de vos employes.\"}','images/services/protection-sociale.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(3,'{\"ar\": \"حلول مخصصة لتحسين سياسات التعويضات والمزايا الخاصة بك.\", \"en\": \"Customized solutions to optimize your compensation and benefits policies.\", \"fr\": \"Solutions personnalisees pour optimiser vos politiques de remuneration et d\'avantages sociaux.\"}','images/services/remuneration-avantages-sociaux.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(4,'{\"ar\": \"إدارة المخاطر بشكل استباقي لحماية أصولك وضمان استمرارية الأعمال.\", \"en\": \"Proactive risk management to protect your assets and ensure business continuity.\", \"fr\": \"Gestion proactive des risques pour proteger vos actifs et assurer la continuite des activites.\"}','images/services/risk-management.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(5,'{\"ar\": \"الدعم في اختيار أفضل تغطيات التأمين الصحي والتعاونيات.\", \"en\": \"Support in choosing the best health insurance and mutual coverages.\", \"fr\": \"Accompagnement pour choisir les meilleures couvertures d\'assurance sante et mutualite.\"}','images/services/conseil-en-assurance-maladie-mutualite.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(6,'{\"ar\": \"استغل بياناتك باستخدام الذكاء الاصطناعي وعلوم البيانات لاتخاذ قرارات استراتيجية.\", \"en\": \"Leverage your data with AI and data science for strategic decisions.\", \"fr\": \"Exploitez vos donnees avec l\'IA et la science des donnees pour des decisions strategiques.\"}','images/services/ai-data-science.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(7,'{\"ar\": \"برامج تدريب مصممة خصيصًا لتطوير مهارات فرقك.\", \"en\": \"Tailored training programs to develop your teams\' skills.\", \"fr\": \"Programmes de formation sur mesure pour developper les competences de vos equipes.\"}','images/services/formation.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(8,'{\"ar\": \"استطلاعات الرضا لتحسين تجربة العملاء وضبط خدماتك.\", \"en\": \"Satisfaction surveys to improve customer experience and adjust your services.\", \"fr\": \"Enquetes de satisfaction pour ameliorer l\'experience client et ajuster vos services.\"}','images/services/enquete-de-satisfaction-client.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,8,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `service_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `services` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `label` json NOT NULL,
  `uri` json NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'{\"ar\": \"استشارات الاكتوارية\", \"en\": \"Actuarial Consulting\", \"fr\": \"Actuariat conseil\"}','{\"ar\": \"astsharat-alaktoary\", \"en\": \"actuarial-consulting\", \"fr\": \"actuariat-conseil\"}','images/services/icons/actuariat-conseil.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(2,'{\"ar\": \"الحماية الاجتماعية\", \"en\": \"Social Protection\", \"fr\": \"Protection sociale\"}','{\"ar\": \"alhmay-alagtmaaay\", \"en\": \"social-protection\", \"fr\": \"protection-sociale\"}','images/services/icons/protection-sociale.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(3,'{\"ar\": \"التعويضات والمزايا\", \"en\": \"Compensation & Benefits\", \"fr\": \"Remuneration & avantages sociaux\"}','{\"ar\": \"altaaoydat-oalmzaya\", \"en\": \"compensation-benefits\", \"fr\": \"remuneration-avantages-sociaux\"}','images/services/icons/remuneration-avantages-sociaux.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(4,'{\"ar\": \"إدارة المخاطر\", \"en\": \"Risk Management\", \"fr\": \"Risk management\"}','{\"ar\": \"adar-almkhatr\", \"en\": \"risk-management\", \"fr\": \"risk-management\"}','images/services/icons/risk-management.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(5,'{\"ar\": \"استشارة التأمين الصحي والتعاونيات\", \"en\": \"Health Insurance & Mutual Advice\", \"fr\": \"Conseil en assurance maladie & mutualite\"}','{\"ar\": \"astshar-altamyn-alshy-oaltaaaonyat\", \"en\": \"health-insurance-mutual-advice\", \"fr\": \"conseil-en-assurance-maladie-mutualite\"}','images/services/icons/conseil-en-assurance-maladie-mutualite.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(6,'{\"ar\": \"الذكاء الاصطناعي وعلوم البيانات\", \"en\": \"AI & Data Science\", \"fr\": \"AI & Data science\"}','{\"ar\": \"althkaaa-alastnaaay-oaalom-albyanat\", \"en\": \"ai-data-science\", \"fr\": \"ai-data-science\"}','images/services/icons/ai-data-science.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(7,'{\"ar\": \"تدريب\", \"en\": \"Training\", \"fr\": \"Formation\"}','{\"ar\": \"tdryb\", \"en\": \"training\", \"fr\": \"formation\"}','images/services/icons/formation.png','2024-12-01 17:13:40','2024-12-01 17:13:40'),(8,'{\"ar\": \"استطلاع رضا العملاء\", \"en\": \"Customer Satisfaction Survey\", \"fr\": \"Enquete de satisfaction client\"}','{\"ar\": \"asttlaaa-rda-alaamlaaa\", \"en\": \"customer-satisfaction-survey\", \"fr\": \"enquete-de-satisfaction-client\"}','images/services/icons/enquete-de-satisfaction-client.png','2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('31wAVWtaDXKvbTEfcPIioTouwhh46oRbRrKapEXG',1,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36 OPR/114.0.0.0','YTo2OntzOjY6Il90b2tlbiI7czo0MDoiWlNGbXc3V09QbHhZcW1vbUpkV2NFY0dZQ09RV0RCR3VESjZNdHFhMiI7czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjU0OiJodHRwOi8vMTI3LjAuMC4xOjgwMDAvYWRtaW4vYWJvdXQvYWJvdXQtYmFubmVycy8xL2VkaXQiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToxO3M6MTc6InBhc3N3b3JkX2hhc2hfd2ViIjtzOjYwOiIkMnkkMTIkbEFIOHF2SG5LLjZVVVp4bmhIeVR6dUZrdmI0aXc5OExFT0h0V00zLnJqbC56MHY4eS9DbmUiO30=',1737327290),('bmUilJHOPtd3t5N89tEoGN93r7in01X4LsyqsmNA',1,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36 OPR/114.0.0.0','YTo3OntzOjY6Il90b2tlbiI7czo0MDoiWWlWN1FrOHJ3OGxtVkt5dDNYaGhFbFptYkZkRm5PajZTVm1MYkR2ZSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mzk6Imh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9hc3NldHMvaW1hZ2UzLnBuZyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6MzoidXJsIjthOjA6e31zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToxO3M6MTc6InBhc3N3b3JkX2hhc2hfd2ViIjtzOjYwOiIkMnkkMTIkbEFIOHF2SG5LLjZVVVp4bmhIeVR6dUZrdmI0aXc5OExFT0h0V00zLnJqbC56MHY4eS9DbmUiO3M6NjoibG9jYWxlIjtzOjI6ImZyIjt9',1737327289),('INTT1qI2z6T8eJhEv4Rr57IGW9pSm6NfOOrvluNP',1,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36 OPR/116.0.0.0','YTozOntzOjY6Il90b2tlbiI7czo0MDoibjFJSEFNSjlmT3B4a2diclBNbnU5VE16WGp6SzFwSDMxQ2tNTkxxRiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mzk6Imh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9hc3NldHMvaW1hZ2UzLnBuZyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=',1739911322),('XTgqXvmzRz4KYKayOXPJzG1YhC6yvp9Wq5Uj1ery',1,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36 OPR/116.0.0.0','YTozOntzOjY6Il90b2tlbiI7czo0MDoiQ1d3VFlLaERQbmg5azJQbjN4b3hzbTYyMDFkUThFeTJTcjdHVzc4ayI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9hc3NldHMvYWJvdXQtYmFubmVyLmpwZyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=',1739831046);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slides`
--

DROP TABLE IF EXISTS `slides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slides` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` json NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cta_label` json NOT NULL,
  `cta_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slides`
--

LOCK TABLES `slides` WRITE;
/*!40000 ALTER TABLE `slides` DISABLE KEYS */;
INSERT INTO `slides` VALUES (1,'{\"ar\": \"استشارات في علم الاكتوارية للشركات، شركات التأمين، الجمعيات، وصناديق التقاعد\", \"en\": \"Consulting in Actuarial Science for Enterprises, Insurers, Mutuals, and Pension Funds\", \"fr\": \"Conseil en Actuariat pour Entreprises, assureurs, mutuelles et caisses de retraite\"}','images/hero/slide1.jpeg','{\"ar\": \"تعرف على المزيد\", \"en\": \"Learn More\", \"fr\": \"SAVOIR PLUS\"}','contact','2024-12-01 17:13:40','2024-12-01 17:13:40'),(2,'{\"ar\": \"الرائد المغربي في استشارات الإدارة والتفاوض على برامج التأمين للشركات\", \"en\": \"Moroccan Leader in Management Consulting and Negotiation of Corporate Insurance Programs\", \"fr\": \"Leader marocain du conseil en gestion et négociation des programmes d\'assurance en entreprises\"}','images/hero/slide2.jpeg','{\"ar\": \"تعرف على المزيد\", \"en\": \"Learn More\", \"fr\": \"SAVOIR PLUS\"}','contact','2024-12-01 17:13:40','2024-12-01 17:13:40'),(3,'{\"ar\": \"الرائد المغربي في استشارات التأمين الصحي والتعاونيات\", \"en\": \"Moroccan Leader in Health Insurance and Mutuality Consulting\", \"fr\": \"Leader marocain du conseil en assurances maladies et mutualité\"}','images/hero/slide3.jpeg','{\"ar\": \"تعرف على المزيد\", \"en\": \"Learn More\", \"fr\": \"SAVOIR PLUS\"}','contact','2024-12-01 17:13:40','2024-12-01 17:13:40'),(4,'{\"ar\": \"خبراء في تحسين مزايا الموظفين (التقاعد والصحة) وتقييم الالتزامات الاجتماعية IAS 19\", \"en\": \"Experts in Optimizing Employee Benefits (Retirement and Health) and Evaluating Social Commitments IAS 19\", \"fr\": \"Experts en optimisation des avantages sociaux (retraite et maladie) et en évaluation des engagements sociaux IAS 19\"}','images/hero/slide4.jpeg','{\"ar\": \"تعرف على المزيد\", \"en\": \"Learn More\", \"fr\": \"SAVOIR PLUS\"}','contact','2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `slides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_media_links`
--

DROP TABLE IF EXISTS `social_media_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `social_media_links` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `layout_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_media_links`
--

LOCK TABLES `social_media_links` WRITE;
/*!40000 ALTER TABLE `social_media_links` DISABLE KEYS */;
INSERT INTO `social_media_links` VALUES (1,'facebook','https://www.facebook.com/ACTUARIA-GLOBAL-882878798456205',1,'2024-12-01 17:13:40','2024-12-01 17:13:40'),(2,'linkedin','https://www.linkedin.com/company/actuaria-global/',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `social_media_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribe_emails`
--

DROP TABLE IF EXISTS `subscribe_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subscribe_emails` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subscribe_emails_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribe_emails`
--

LOCK TABLES `subscribe_emails` WRITE;
/*!40000 ALTER TABLE `subscribe_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscribe_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribe_forms`
--

DROP TABLE IF EXISTS `subscribe_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subscribe_forms` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` json NOT NULL,
  `paragraph` json NOT NULL,
  `input_placeholder` json NOT NULL,
  `layout_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribe_forms`
--

LOCK TABLES `subscribe_forms` WRITE;
/*!40000 ALTER TABLE `subscribe_forms` DISABLE KEYS */;
INSERT INTO `subscribe_forms` VALUES (1,'{\"ar\": \"اشترك الآن!\", \"en\": \"Subscribe Now!\", \"fr\": \"Abonnez-vous maintenant !\"}','{\"ar\": \"اشترك في نشرتنا الإخبارية للحصول على آخر التحديثات.\", \"en\": \"Subscribe to our newsletter for the latest updates.\", \"fr\": \"Abonnez-vous à notre newsletter pour les dernières mises à jour.\"}','{\"ar\": \"عنوان بريدك الإلكتروني\", \"en\": \"Email address\", \"fr\": \"Adresse e-mail\"}',1,'2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `subscribe_forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `success_items`
--

DROP TABLE IF EXISTS `success_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `success_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `label` json NOT NULL,
  `number` int NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon_style` enum('solid','regular','light','thin') COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_percentage` tinyint(1) NOT NULL DEFAULT '0',
  `success_itemable_id` bigint unsigned DEFAULT NULL,
  `success_itemable_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `success_items`
--

LOCK TABLES `success_items` WRITE;
/*!40000 ALTER TABLE `success_items` DISABLE KEYS */;
INSERT INTO `success_items` VALUES (1,'{\"ar\": \"سنوات الخبرة\", \"en\": \"Years of Experience\", \"fr\": \"Années d\'expérience\"}',20,'calendar-days','solid',0,1,'App\\Models\\Home\\HomeSuccess','2024-12-01 17:13:40','2024-12-01 17:13:40'),(2,'{\"ar\": \"عملاء راضون\", \"en\": \"Satisfied Clients\", \"fr\": \"Clients satisfaits\"}',200,'smile','solid',0,1,'App\\Models\\Home\\HomeSuccess','2024-12-01 17:13:40','2024-12-01 17:13:40'),(3,'{\"ar\": \"دول التدخل\", \"en\": \"Countries of Intervention\", \"fr\": \"Pays d\'intervention\"}',10,'globe','solid',0,1,'App\\Models\\Home\\HomeSuccess','2024-12-01 17:13:40','2024-12-01 17:13:40'),(4,'{\"ar\": \"معدل الاحتفاظ\", \"en\": \"Retention Rate\", \"fr\": \"Taux de fidélisation\"}',90,'thumbs-up','solid',1,1,'App\\Models\\Home\\HomeSuccess','2024-12-01 17:13:40','2024-12-01 17:13:40'),(5,'{\"ar\": \"معدل الاحتفاظ\", \"en\": \"Retention Rate\", \"fr\": \"Taux de fidélisation\"}',90,'thumbs-up','solid',1,1,'App\\Models\\About\\AboutVission','2024-12-01 17:13:40','2024-12-01 17:13:40'),(6,'{\"ar\": \"معدل الاحتفاظ\", \"en\": \"Retention Rate\", \"fr\": \"Taux de fidélisation\"}',90,'thumbs-up','solid',1,2,'App\\Models\\About\\AboutVission','2024-12-01 17:13:40','2024-12-01 17:13:40'),(7,'{\"ar\": \"معدل الاحتفاظ\", \"en\": \"Retention Rate\", \"fr\": \"Taux de fidélisation\"}',90,'thumbs-up','solid',1,3,'App\\Models\\About\\AboutVission','2024-12-01 17:13:40','2024-12-01 17:13:40');
/*!40000 ALTER TABLE `success_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supports`
--

DROP TABLE IF EXISTS `supports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `supports` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `layout_id` bigint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supports`
--

LOCK TABLES `supports` WRITE;
/*!40000 ALTER TABLE `supports` DISABLE KEYS */;
INSERT INTO `supports` VALUES (1,'contact@actuariaglobal.ma','+212522862886',1,'2024-12-01 17:13:40','2024-12-01 17:16:31');
/*!40000 ALTER TABLE `supports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin@actuariaglobal.ma',NULL,'$2y$12$lAH8qvHnK.6UUZxnhHyTzuFkvb4iw98LEOHtWM3.rjl.z0v8y/Cne','ytNwqWtAgYgB0dbOody3zHdB3rHpWrPBzBpyQ9pLbWFxZkSIYcMw7LQUG7Da','2024-12-01 17:13:57','2024-12-01 17:13:57');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-18 21:51:41
