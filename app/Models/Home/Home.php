<?php

namespace App\Models\Home;

use App\Models\Client\Client;
use App\Models\Service\Service;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'paragraph',
        'home_id',
    ];

    
    public function client(){
        return $this->hasOne(HomeClient::class);
    }

    public function service(){
        return $this->hasOne(HomeService::class);
    }

    public function about(){
        return $this->hasOne(HomeAbout::class);
    }

    public function hero(){
        return $this->hasOne(HomeHero::class);
    }
    
    public function success(){
        return $this->hasOne(HomeSuccess::class);
    }
    
}
