<?php

namespace App\Filament\Resources\Layout\Header;

use App\Filament\Resources\Layout\Header\HeaderMenuItemResource\Pages;
use App\Filament\Resources\Layout\Header\HeaderMenuItemResource\RelationManagers;
use App\Models\Layout\Header\HeaderMenuItem;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

// TODO: Consider making the Layout Resources into its own cluster

class HeaderMenuItemResource extends Resource
{
    use Translatable;

    protected static ?string $model = HeaderMenuItem::class;

    protected static ?string $navigationIcon = 'heroicon-o-link';

    protected static ?string $activeNavigationIcon = 'heroicon-s-link';

    protected static ?string $modelLabel = 'header_menu_items';

    protected static ?string $navigationGroup = 'header';

    // TODO: Make some resources modify-only
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('label')
                    ->required()
                    ->label(static::getDynamicLabel("label"))
                    ->translateLabel(),
                Forms\Components\TextInput::make('uri')
                    ->required()
                    ->label(static::getDynamicLabel("uri"))
                    ->translateLabel(),
                Forms\Components\Select::make('parent_id')
                    ->relationship(
                        'parent',
                        'label'
                    )
                    ->nullable()
                    ->label(static::getDynamicLabel("parent_id"))
                    ->translateLabel(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('label')
                    ->searchable()
                    ->sortable()
                    ->label(static::getDynamicLabel("label"))
                    ->translateLabel(),

                Tables\Columns\TextColumn::make('uri')
                    ->searchable()
                    ->sortable()
                    ->label(static::getDynamicLabel("uri"))
                    ->translateLabel(),

                Tables\Columns\TextColumn::make('parent.label')
                    ->sortable()
                    ->label(static::getDynamicLabel("parent_id"))
                    ->translateLabel(),

                // Tables\Columns\TextColumn::make('created_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->label(static::getDynamicLabel("created_at"))
                //     ->translateLabel(),
                // Tables\Columns\TextColumn::make('updated_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->label(static::getDynamicLabel("updated_at"))
                //     ->translateLabel(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListHeaderMenuItems::route('/'),
            'create' => Pages\CreateHeaderMenuItem::route('/create'),
            'edit' => Pages\EditHeaderMenuItem::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch plural translation without "s"
    }

    public static function getModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch translation based on current locale
    }

    public static function getNavigationGroup(): ?string
    {
        return __("filament/custom/navigation-model-labels." . static::$navigationGroup); // Fetch translation based on current locale
    }

    protected static function getDynamicLabel(string $field): string
    {
        return __("model-properties." . static::$modelLabel . '-' . $field);
    }
}
