<?php

return [
    /* <-------------------- Layout Models --------------------> */

    /* <----------- Header Models -----------> */

    /* <--- Support ------> */
    "supports-email" => "Email",
    "supports-phone" => "Téléphone",

    /* <--- Location ------> */
    "locations-label" => "Nom de l'emplacement",
    "locations-url" => "Lien de la carte",

    /* <--- HeaderMenuItem ------> */
    "header_menu_items-label" => "Nom du menu",
    "header_menu_items-uri" => "Lien",
    "header_menu_items-parent_id" => "Menu parent",

    /* <--- Logo ------> */
    "logos-image" => "Logo",

    /* <----------- Footer Models -----------> */

    /* <--- SubscribeForm ------> */
    "subscribe_forms-title" => "Titre du formulaire",
    "subscribe_forms-paragraph" => "Description du formulaire",
    "subscribe_forms-input_placeholder" => "Champ email",

    /* <--- SubscribeEmail ------> */
    "subscribe_emails-email" => "Email de l'abonné",

    /* <--- FooterMenuItem ------> */
    "footer_menu_items-label" => "Nom du menu",
    "footer_menu_items-uri" => "Lien",

    /* <--- Copyright ------> */
    "copyrights-paragraph" => "Texte des droits d'auteur",

    /* <--- FooterService ------> */
    "footer_services-title" => "Titre du service",

    /* <--- FooterPost ------> */
    "footer_posts-title" => "Titre de la publication",

    /* <--- FooterContact ------> */
    "footer_contacts-title" => "Titre de la section",
    "footer_contacts-address_title" => "Étiquette d'adresse",
    "footer_contacts-address" => "Adresse",
    "footer_contacts-contact_number_title" => "Étiquette du téléphone",
    "footer_contacts-contact_number" => "Téléphone",

    /* <-------------------- Home Models --------------------> */

    /* <----------- HomeAbout -----------> */
    "home_abouts-title" => "Titre",
    "home_abouts-keywords" => "Mots-clés",
    "home_abouts-paragraph" => "Description",
    "home_abouts-italic_paragraph" => "Texte mis en valeur",
    "home_abouts-image" => "Image",
    "home_abouts-cta_label" => "Bouton d'action",
    "home_abouts-cta_path" => "Lien d'action",

    /* <----------- HomeClient -----------> */
    "home_clients-title" => "Titre des clients",

    /* <----------- HomeServices -----------> */
    "home_services-title" => "Titre de la section",
    "home_services-paragraph" => "Description de la section",

    /* <-------------------- About Models --------------------> */

    /* <----------- AboutBanner -----------> */
    "about_banners-title" => "Titre de la bannière",
    "about_banners-image" => "Image de la bannière",

    /* <----------- AboutVission -----------> */
    "about_vissions-tab_title" => "Titre de l'onglet",
    "about_vissions-tab_image" => "Image de l'onglet",
    "about_vissions-title" => "Titre",
    "about_vissions-keywords" => "Mots-clés",
    "about_vissions-paragraph" => "Description",

    /* <-------------------- Contact Models --------------------> */

    /* <----------- Contact -----------> */
    "contacts-name" => "Nom",
    "contacts-email" => "Email",
    "contacts-phone_number" => "Téléphone",
    "contacts-subject" => "Sujet",
    "contacts-message" => "Message",

    /* <-------------------- Service Models --------------------> */

    /* <----------- Service -----------> */
    "services-label" => "Nom",
    "services-uri" => "Lien",
    "services-icon" => "Icône",

    /* <----------- ServiceDetail -----------> */
    "service_details-title_paragraph" => "Titre",
    "service_details-image" => "Image",
    "service_details-header1" => "En-tête 1",
    "service_details-paragraph1_1" => "Paragraphe 1.1",
    "service_details-paragraph2_1" => "Paragraphe 2.1",
    "service_details-header2" => "En-tête 2",
    "service_details-paragraph1_2" => "Paragraphe 1.2",
    "service_details-paragraph2_2" => "Paragraphe 2.2",
    "service_details-header3" => "En-tête 3",
    "service_details-paragraph1_3" => "Paragraphe 1.3",

    /* <-------------------- Post Models --------------------> */

    /* <----------- Post -----------> */
    "posts-title" => "Titre",
    "posts-uri" => "Lien",
    "posts-date" => "Date",

    /* <-------------------- Client Models --------------------> */

    /* <----------- Client -----------> */
    "clients-name" => "Nom",
    "clients-logo" => "Logo",

    /* <-------------------- Utility Models --------------------> */

    /* <----------- SuccessItem  -----------> */
    "success_items-label" => "Étiquette",
    "success_items-number" => "Nombre",
    "success_items-icon" => "Icône",
    "success_items-icon_style" => "Style de l'icône",
    "success_items-is_percentage" => "Pourcentage ?",

    /* <----------- Slide  -----------> */
    "slides-title" => "Titre",
    "slides-image" => "Image",
    "slides-cta_label" => "Bouton d'action",
    "slides-cta_path" => "Lien d'action"
];
