@extends('layouts.app')
@section('home.index')

    <x-pages.home.hero
        :home-hero="$home->hero"
    />

    <x-pages.home.success
        :home-success="$home->success"
    />

    <x-pages.home.about-us
        :home-about="$home->about"
    />

    <x-pages.home.services

        :home-service="$home->service"

    />

    <x-pages.home.clients
        :home-client="$home->client"
    />

    {{-- <x-shared.cases

        :section=""
        :cases=""

    /> --}}

    <x-shared.call-back
    {{--
        :image=""
        :section=""
        :placeholder=""
        :cta=""
    --}}
    />

    {{--
        <x-shared.team

            :section=""
            :members=""

        />
    --}}

    {{--
        <x-shared.testimonial

            :section=""
            :reviews=""
            :reviewButtons=""

        />
    --}}

    <x-shared.call-to-action
    {{--
        :section=""
        :cta=""

    --}}
        :theme="'snow'"
    />

@endsection

