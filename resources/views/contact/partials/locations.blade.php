<div class="col-lg-4">
    <div class="contact-sidebar">
        <div class="sidebar-widget bg-snow">
            <h3>Location: 01</h3>
            <ul>
                <li>
                    <div class="left-icon">
                        <i class="fas fa-map-marker-alt"></i>
                    </div>
                    <div class="right-content">
                    </div>
                </li>
                <li>
                    <div class="left-icon">
                        <i class="fas fa-phone-alt"></i>
                    </div>
                    <div class="right-content">
                        <a href="callto:+88999666444">+88-999-666-444</a><br>
                        <a href="callto:+88888555777">+88-888-555-777</a>
                    </div>
                </li>
                <li>
                    <div class="left-icon">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="right-content">
                        <a href="mailto:info@domain.com">info@domain.com</a><br>
                        <a href="mailto:support@domain.com">support@domain.com</a>
                    </div>
                </li>
            </ul>
        </div>
        <div class="sidebar-widget bg-black text-white">
            <h3>Location: 02</h3>
            <ul>
                <li>
                    <div class="left-icon">
                        <i class="fas fa-map-marker-alt"></i>
                    </div>
                    <div class="right-content">
                        61 South Big Rock Cove Zurich, Villad 60047
                    </div>
                </li>
                <li>
                    <div class="left-icon">
                        <i class="fas fa-phone-alt"></i>
                    </div>
                    <div class="right-content">
                        <a href="callto:+88999666444">+88-999-666-444</a><br>
                        <a href="callto:+88888555777">+88-888-555-777</a>
                    </div>
                </li>
                <li>
                    <div class="left-icon">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="right-content">
                        <a href="mailto:info@domain.com">info@domain.com</a><br>
                        <a href="mailto:support@domain.com">support@domain.com</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
