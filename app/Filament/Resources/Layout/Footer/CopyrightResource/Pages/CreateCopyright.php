<?php

namespace App\Filament\Resources\Layout\Footer\CopyrightResource\Pages;

use App\Filament\Resources\Layout\Footer\CopyrightResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateCopyright extends CreateRecord
{
    use CreateRecord\Concerns\Translatable;

    protected static string $resource = CopyrightResource::class;
    
    protected function getHeaderActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            // ...
        ];
    }

}
