<?php

namespace App\Filament\Resources\Home;

use App\Filament\Resources\Home\HomeAboutResource\Pages;
use App\Filament\Resources\Home\HomeAboutResource\RelationManagers;
use App\Models\Home\HomeAbout;
use Filament\Forms;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TagsInput;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class HomeAboutResource extends Resource
{

    use Translatable;

    protected static ?string $model = HomeAbout::class;

    protected static ?string $navigationIcon = 'heroicon-o-users';

    protected static ?string $activeNavigationIcon = 'heroicon-s-users';

    protected static ?string $modelLabel = 'home_abouts';

    protected static ?string $navigationGroup = 'homes';



    // TODO: Add Tabs for clarity, consider adding them to other resources
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Title Section')->schema([
                    TextInput::make('title')
                        ->required()
                        ->maxLength(255)
                        ->label(static::getDynamicLabel("title"))
                        ->translateLabel(),
                    TextInput::make('keywords')
                        ->hint(static::getDynamicFieldProp("keywords", "hint"))
                        ->label(static::getDynamicLabel("keywords"))
                        ->translateLabel(),
                ])->columnSpan(2),

                Section::make()->schema([
                    FileUpload::make('image')
                        ->acceptedFileTypes(['image/png', "image/jpeg"])
                        ->disk('assets')
                        ->directory('images/abouts')
                        ->required()
                        ->label(static::getDynamicLabel("image"))
                        ->translateLabel(),

                ])->columnSpan(1),

                Section::make('Call To Action Button Section')->schema([
                    TextInput::make('cta_label')
                        ->label('Button label')
                        ->required()
                        ->maxLength(255)
                        ->label(static::getDynamicLabel("cta_label"))
                        ->translateLabel(),

                    Select::make('cta_path')
                        ->options([
                            'abouts' => 'A propos',
                            'homes' => 'Accueil',
                            'contacts' => 'Contact'
                        ])
                        ->label('Button route')
                        ->required()
                        ->label(static::getDynamicLabel("cta_path"))
                        ->translateLabel(),
                ]),

                Section::make('Paragraphs Section')
                    ->description('For using lists, use • before list')
                    ->schema([
                        Textarea::make('paragraph')
                            ->required()
                            ->label(static::getDynamicLabel("paragraph"))
                            ->translateLabel(),

                        Textarea::make('italic_paragraph')
                            ->required()
                            ->label(static::getDynamicLabel("italic_paragraph"))
                            ->translateLabel(),
                    ])->columnSpan(3)


            ])->columns(3);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                ImageColumn::make('image')
                    ->disk('assets')
                    ->square()
                    ->toggleable()
                    ->label(static::getDynamicLabel("image"))
                    ->translateLabel(),

                TextColumn::make('title')
                    ->sortable()
                    ->searchable()
                    ->toggleable()
                    ->label(static::getDynamicLabel("title"))
                    ->translateLabel(),

                TextColumn::make('keywords')
                    ->sortable()
                    ->searchable()
                    ->toggleable()
                    ->label(static::getDynamicLabel("keywords"))
                    ->translateLabel(),

                TextColumn::make('paragraph')
                    ->limit(50)
                    ->tooltip(fn($record) => $record->paragraph)
                    ->toggleable()
                    ->label(static::getDynamicLabel("paragraph"))
                    ->translateLabel(),

                TextColumn::make('italic_paragraph')
                    ->limit(50)
                    ->tooltip(fn($record) => $record->paragraph)
                    ->label('Italic Paragraph')
                    ->toggleable()
                    ->label(static::getDynamicLabel("italic_paragraph"))
                    ->translateLabel(),


                TextColumn::make('cta_label')
                    ->sortable()
                    ->searchable()
                    ->toggleable()
                    ->label(static::getDynamicLabel("cta_label"))
                    ->translateLabel(),

                TextColumn::make('cta_path')
                    ->sortable()
                    ->searchable()
                    ->toggleable()
                    ->label(static::getDynamicLabel("cta_path"))
                    ->translateLabel(),

                // TextColumn::make('created_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->toggleable()
                //     ->label(static::getDynamicLabel("title"))
                //     ->translateLabel(),

                // TextColumn::make('updated_at')
                //     ->dateTime()
                //     ->sortable()
                //     ->toggleable()
                //     ->label(static::getDynamicLabel("title"))
                //     ->translateLabel(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListHomeAbouts::route('/'),
            'create' => Pages\CreateHomeAbout::route('/create'),
            'edit' => Pages\EditHomeAbout::route('/{record}/edit'),
        ];
    }


    public static function getPluralModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch plural translation without "s"
    }

    public static function getModelLabel(): string
    {
        return __("filament/custom/navigation-model-labels." . static::$modelLabel); // Fetch translation based on current locale
    }

    public static function getNavigationGroup(): ?string
    {
        return __("filament/custom/navigation-model-labels." . static::$navigationGroup); // Fetch translation based on current locale
    }

    protected static function getDynamicLabel(string $field): string
    {
        return __("model-properties." . static::$modelLabel . '-' . $field);
    }

    public static function getModelName(): string
    {
        $fullClassName = static::$model;

        return basename(str_replace('\\', '/', $fullClassName));
    }

    public static function getDynamicFieldProp(string $field, string $prop): string
    {
        $parentDirName = basename(dirname(__FILE__));
        $path = "filament/custom/resources/$parentDirName/" . static::getModelName();
        return __($path . "."  .  $field . "-" . $prop);
    }
}
