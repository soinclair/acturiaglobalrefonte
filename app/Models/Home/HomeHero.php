<?php

namespace App\Models\Home;

use App\Models\Utility\Slide;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeHero extends Model
{
    use HasFactory;

    public function home(){
        return $this->belongsTo(Home::class);
    }

    // TODO: Make the slides also MorphToMany
    public function getSlidesAttribute(){
        return Slide::all();
    }
}
