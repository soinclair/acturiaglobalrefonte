<?php

namespace App\View\Components\Classes;

class Member{

    public function __construct(
        
        public int $id,
        public string $name,
        public string $position,
        public Image $image,
        public array $socialMediaLinks 
        
    ){

    }
}