<?php

namespace App\Filament\Resources\About\AboutVissionResource\Pages;

use App\Filament\Resources\About\AboutVissionResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditAboutVission extends EditRecord
{
    use EditRecord\Concerns\Translatable;

    protected static string $resource = AboutVissionResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
