<?php 

namespace App\Services\TextTransformer;

class TextTransformerResult
{
    protected $text;

    public function __construct($text)
    {
        $this->text = $text;
    }

    public function transformTextWithLists()
    {
        // Perform the list transformation (assuming you already have this logic)
        $this->text = $this->transformTextWithListsLogic($this->text);
        return $this;
    }

    public function transformTextWithKeywords($keywords){
        $this->text = $this->transformTextWithKeywordsLogic($this->text, $keywords);
        return $this;
    }

    public function removeAccents()
    {
        // Remove accents
        $this->text = $this->removeAccentsLogic($this->text);
        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function transformTextWithListsLogic($text){
        // Split the text into lines
        $lines = explode("\n", $text);
        $inList = false;
        $result = '';

        foreach ($lines as $line) {
            // Check if the line starts with a bullet point
            if (strpos(trim($line), '•') === 0) {
                // Start an unordered list if not already in one
                if (!$inList) {
                    $result .= '<ul>';
                    $inList = true;
                }

                // Extract the list item, keeping the bullet point and semicolon
                $listItem = trim(ltrim($line, '• '));
                $result .= '<li> ' . $listItem;

                // Check if the list item ends with a semicolon
                if (substr($listItem, -1) === ';') {
                    $result = rtrim($result, ';') . '</li>';
                } else {
                    $result .= '</li>';
                }
            } else {
                // Close the list if it was open
                if ($inList) {
                    $result .= '</ul>';
                    $inList = false;
                }
                $result .= $line . "\n";
            }
    }

    // Close any open list
    if ($inList) {
        $result .= '</ul>';
    }

    return $result;
}


    public function removeAccentsLogic($text){

        $search = ['À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'Þ', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'þ', 'ÿ'];
        $replace = ['A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'TH', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'th', 'y'];
        return str_ireplace($search, $replace, $text);
    }

    public function transformTextWithKeywordsLogic($text, $keywordsString){
        $keywords = array_filter(array_map('trim', explode(',', $keywordsString)));

        foreach ($keywords as $keyword) {
            $text = preg_replace('/\b' . preg_quote($keyword, '/') . '\b/', '<span>' . $keyword . '</span>', $text);
        }

        return $text;
    }
}
