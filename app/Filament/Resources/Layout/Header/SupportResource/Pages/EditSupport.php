<?php

namespace App\Filament\Resources\Layout\Header\SupportResource\Pages;

use App\Filament\Resources\Layout\Header\SupportResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditSupport extends EditRecord
{
    protected static string $resource = SupportResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
