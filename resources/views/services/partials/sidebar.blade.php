<div class="col-lg-4">
    <div class="service-sidebar">
        <div class="sidebar-widget service-widget">
            <h3>Notre Services</h3>
            <ul>
                @foreach ($services as $service)
                <li>
                    <a href="{{route('services.show',['uri' => $service->uri])}}">
                        {{ $service->label }}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="sidebar-widget contact-widget text-center bg-snow">
            <p>NOUS SOMMES PRÊTS.</p>
            <h3>Comment pouvons-nous vous aider?</h3>
            <a href="{{route('contact.index')}}" class="theme-btn">Contacter Nous<i class="fas fa-arrow-right"></i></a>
        </div>
    </div>
</div>