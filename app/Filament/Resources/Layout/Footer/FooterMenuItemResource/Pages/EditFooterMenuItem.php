<?php

namespace App\Filament\Resources\Layout\Footer\FooterMenuItemResource\Pages;

use App\Filament\Resources\Layout\Footer\FooterMenuItemResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditFooterMenuItem extends EditRecord
{
    use EditRecord\Concerns\Translatable;

    protected static string $resource = FooterMenuItemResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
