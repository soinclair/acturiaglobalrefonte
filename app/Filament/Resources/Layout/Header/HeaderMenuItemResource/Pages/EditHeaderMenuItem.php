<?php

namespace App\Filament\Resources\Layout\Header\HeaderMenuItemResource\Pages;

use App\Filament\Resources\Layout\Header\HeaderMenuItemResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditHeaderMenuItem extends EditRecord
{
    use EditRecord\Concerns\Translatable;

    protected static string $resource = HeaderMenuItemResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),

        ];
    }
}
