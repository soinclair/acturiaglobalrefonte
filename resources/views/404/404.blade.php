@extends('layouts.app')
@section('404')

    <x-shared.breadcrumb-banner 
        :title="'404 Non trouve'"
        {{-- 
        :image=""
        :breadcrumbs=""
        --}}
    />

    @include('404.partials.error')

    <x-shared.call-to-action 
    {{-- 
        :section=""
        :cta=""
        :theme=""
    --}}
    />
@endsection        