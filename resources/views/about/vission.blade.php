<section class="vission-mission py-150 rpy-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <ul class="vission-tabs">
                    <li class="wow fadeInUp" data-tab="tab_1" data-wow-duration="1.5s"><h3>Histoire</h3></li>
                    <li class="active wow fadeInUp" data-tab="tab_2" data-wow-duration="1.5s" data-wow-delay="0.3s"><h3>Notre engagement en Afrique</h3></li>
                    <li class="wow fadeInUp" data-tab="tab_3" data-wow-duration="1.5s" data-wow-delay="0.5s"><h3>Notre vision</h3></li>
                </ul>
            </div>
            <div class="col-lg-6">
                <div class="vission-content-wrap">

                    <div id="tab_1" class="vission-tab-content">
                        <div class="section-title">
                            <h2>Notre Passe<br>Votre Avenir <span>Securise.</span></h2>
                        </div>
                        <p>Fondee en 2011 par des experts cumulant plus de 20 ans d'experience dans le domaine de l'actuariat et de la gestion des risques, notre entreprise a connu une croissance impressionnante au fil des ans.</p>
                        <p>Grace a notre engagement envers l'excellence et notre approche personnalisee, nous avons su repondre aux besoins specifiques de nos clients, leur offrant des solutions innovantes et efficaces pour faire face aux defis complexes de leur secteur d'activite. Forts de notre experience et de notre expertise approfondie, nous continuons a evoluer et a elargir nos services pour rester a la pointe de notre industrie, tout en maintenant notre engagement envers la satisfaction client et l'integrite professionnelle.</p>
                        <div class="success-item bg-snow">
                            <div class="icon-image">
                                <img src="{{ asset("assets/images/services/icons/fallback.png") }}" loading="lazy" alt="Success Icon Image">
                            </div>
                            <div class="success-content">
                                <span class="count-text" data-speed="2500" data-stop="568">0</span>
                                <h5>Complete Projects.</h5>
                            </div>
                        </div>
                    </div>

                    <div id="tab_2" class="vission-tab-content active">
                        <div class="section-title">
                            <h2>Des Solutions<br>Locales pour des<br><span>Defis Mondiaux</span></h2>
                        </div>
                        <p>Nos missions en Afrique refletent notre engagement envers le continent et ses communautes. Nos realisations en Afrique incluent : </p>   
                        <ul>
                            <li><span>Etude sur les conditions techniques et financieres</span> relatives a l'extension de la couverture aux autres categories d'Agents publics de l'Etat ainsi qu'au lancement de la branche des risques professionnels par la CNSSAP en republique democratique du Congo.</li>
                            <li><span>La mise en place d'un regime de couverture medicale des fonctionnaires</span> au profit de l'institut national d'assurance maladie obligatoire (INAMO) en Guinee Conakry.</li>
                            <li><span>Etude actuarielle pour les trois branches d'activite</span> de la Caisse Nationale de Prevoyance Sociale des Agents de l'Etat (CNPSAE) en Guinee Conakry.</li>
                            <li><span>Realisation des travaux actuariels dans le cadre du projet</span> de Mise en place d'une nouvelle strategie sanitaire a la Republique du Congo, Projet pilotee par la Banque Mondiale.</li>
                            <li><span>Evaluation actuarielle et financiere</span> du regime d'assurance maladie universelle Burkina Faso.</li>
                            <li><span>Diagnostic strategique</span> de la Caisse Nationale d'Assurance Maladie - CANAM (Mali).</li>
                            <li><span>De nombreuses missions d'evaluations</span> des engagements sociaux selon les normes IFRS pour des entreprises en RDC, Benin, Guinee, Cote d'ivoire et Cameroun.</li>
                        </ul>   
                        <div class="success-item bg-snow">
                            <div class="icon-image">
                                <img  src="{{ asset("assets/images/services/icons/fallback.png") }}" loading="lazy" alt="Success Icon Image">
                            </div>
                            <div class="success-content">
                                <span class="count-text" data-speed="2500" data-stop="890">0</span>
                                <h5>Happy Customers.</h5>
                            </div>
                        </div>
                    </div>

                    <div id="tab_3" class="vission-tab-content">
                        <div class="section-title">
                            <h2>Predire l'incertain<br>Securiser<br><span>l'Essentiel</span></h2>
                        </div>
                        <p>Dans un monde en evolution dynamique, marque par des defis economiques, technologiques et sociaux, nous sommes determines a offrir a nos clients la clarte et la confiance necessaires pour naviguer avec succes a travers les complexites de l'avenir. Nous croyons fermement que chaque decision financiere doit etre guidee par une analyse rigoureuse et une anticipation des risques potentiels. C'est pourquoi nous nous engageons a fournir des solutions actuarielles innovantes et sur mesure, qui permettent a nos clients de prendre des decisions eclairees et de proteger ce qui leur est le plus cher. Avec notre expertise et notre engagement envers l'excellence, nous sommes la pour vous aider a construire un avenir sur et prospere, un pas a la fois.</p>
                        <div class="success-item bg-snow">
                            <div class="icon-image">
                                <img  src="{{ asset("assets/images/services/icons/fallback.png") }}" loading="lazy" alt="Success Icon Image">
                            </div>
                            <div class="success-content">
                                <span class="count-text" data-speed="2500" data-stop="134">0</span>
                                <h5>Winning Awards.</h5>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>