<?php

namespace App\Filament\Resources\Layout\Footer\SubscribeEmailResource\Pages;

use App\Filament\Resources\Layout\Footer\SubscribeEmailResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateSubscribeEmail extends CreateRecord
{
    protected static string $resource = SubscribeEmailResource::class;
}
