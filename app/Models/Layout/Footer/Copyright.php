<?php

namespace App\Models\Layout\Footer;

use App\Models\Layout\Layout;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Copyright extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = [
        'paragraph',
    ];

    public $translatable = [
        "paragraph",
    ];

    public function layout()
    {
        return $this->belongsTo(Layout::class);
    }
}
